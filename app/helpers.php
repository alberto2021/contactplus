<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

if (!function_exists("user_phone")) {

    function user_phone($email)
    {
        if (empty($email)) {
            return '22998736175';
        } else {
            return DB::table('users')->where('email', $email)->select('phone')
                ->get()
                ->pluck('phone')[0];
        }
    }
}


if (!function_exists("new_message_not_open")) {

    function new_message_not_open()
    {
        return DB::table('messages')
                  ->where('is_active', 1)
                  ->where('date_end', '>=', Carbon::now())
                  ->count();
    }
}

if (!function_exists("country_name")) {

    function country_name($indicatif)
    {
        $nom = '';

        if ($indicatif == "+93") {
            $nom = 'Afghanistan';
        }
        if ($indicatif == "+27") {
            $nom = 'Afrique du Sud';
        }
        if ($indicatif == "+355") {
            $nom = 'Albanie';
        }
        if ($indicatif == "+213") {
            $nom = 'Algérie';
        }
        if ($indicatif == "+49") {
            $nom = 'Allemagne';
        }
        if ($indicatif == "+376") {
            $nom = 'Andorre';
        }
        if ($indicatif == "+244") {
            $nom = 'Angola';
        }
        if ($indicatif == "+1264") {
            $nom = 'Anguilla';
        }
        if ($indicatif == "+1268") {
            $nom = 'Antigua-et-Barbuda';
        }
        if ($indicatif == "+599") {
            $nom = 'Antilles néerlandaises';
        }
        if ($indicatif == "+966") {
            $nom = 'Arabie saoudite';
        }
        if ($indicatif == "+54") {
            $nom = 'Argentine';
        }
        if ($indicatif == "+374") {
            $nom = 'Arménie';
        }
        if ($indicatif == "+297") {
            $nom = 'Aruba';
        }
        if ($indicatif == "+247") {
            $nom = 'Ascension';
        }
        if ($indicatif == "+61") {
            $nom = 'Australie';
        }
        if ($indicatif == "+43") {
            $nom = 'Autriche';
        }
        if ($indicatif == "+994") {
            $nom = 'Azerbaïdjan';
        }
        if ($indicatif == "+1242") {
            $nom = 'Bahamas';
        }
        if ($indicatif == "+973") {
            $nom = 'Bahreïn';
        }
        if ($indicatif == "+880") {
            $nom = 'Bangladesh';
        }
        if ($indicatif == "+1246") {
            $nom = 'Barbade';
        }
        if ($indicatif == "+32") {
            $nom = 'Belgique';
        }
        if ($indicatif == "+501") {
            $nom = 'Belize';
        }
        if ($indicatif == "+229") {
            $nom = 'Bénin';
        }
        if ($indicatif == "+1441") {
            $nom = 'Bermudes';
        }
        if ($indicatif == "+975") {
            $nom = 'Bhoutan';
        }
        if ($indicatif == "+375") {
            $nom = 'Biélorussie';
        }
        if ($indicatif == "+95") {
            $nom = 'Birmanie';
        }
        if ($indicatif == "+591") {
            $nom = 'Bolivie';
        }
        if ($indicatif == "+387") {
            $nom = 'Bosnie-Herzégovine';
        }
        if ($indicatif == "+267") {
            $nom = 'Botswana';
        }
        if ($indicatif == "+55") {
            $nom = 'Brésil';
        }
        if ($indicatif == "+673") {
            $nom = 'Brunei Darussalam';
        }
        if ($indicatif == "+359") {
            $nom = 'Bulgarie';
        }
        if ($indicatif == "+226") {
            $nom = 'Burkina Faso';
        }
        if ($indicatif == "+257") {
            $nom = 'Burundi';
        }
        if ($indicatif == "+855") {
            $nom = 'Cambodge';
        }
        if ($indicatif == "+237") {
            $nom = 'Cameroun';
        }
        if ($indicatif == "+1") {
            $nom = 'Canada';
        }
        if ($indicatif == "+238") {
            $nom = 'Cap-Vert';
        }
        if ($indicatif == "+1345") {
            $nom = 'Îles Caïmans';
        }
        if ($indicatif == "+236") {
            $nom = 'République centrafricaine';
        }
        if ($indicatif == "+56") {
            $nom = 'Chili';
        }
        if ($indicatif == "+86") {
            $nom = 'Chine';
        }
        if ($indicatif == "+357") {
            $nom = 'Chypre';
        }
        if ($indicatif == "+57") {
            $nom = 'Colombie';
        }
        if ($indicatif == "+269") {
            $nom = 'Comores';
        }
        if ($indicatif == "+242") {
            $nom = 'Congo';
        }
        if ($indicatif == "+243") {
            $nom = 'Congo (Rép. Democratique)';
        }
        if ($indicatif == "+682") {
            $nom = 'Cook (Îles)';
        }
        if ($indicatif == "+850") {
            $nom = 'Corée du Nord';
        }
        if ($indicatif == "+82") {
            $nom = 'Corée du Sud';
        }
        if ($indicatif == "+506") {
            $nom = 'Costa Rica';
        }
        if ($indicatif == "+225") {
            $nom = 'Côte d\'Ivoire';
        }
        if ($indicatif == "+385") {
            $nom = 'Croatie';
        }
        if ($indicatif == "+53") {
            $nom = 'Cuba';
        }
        if ($indicatif == "+45") {
            $nom = 'Danemark';
        }
        if ($indicatif == "+246") {
            $nom = 'Diego Garcia';
        }
        if ($indicatif == "+253") {
            $nom = 'Djibouti';
        }
        if ($indicatif == "+1767") {
            $nom = 'Dominique';
        }
        if ($indicatif == "+20") {
            $nom = 'Égypte';
        }
        if ($indicatif == "+971") {
            $nom = 'Émirats arabes unis';
        }
        if ($indicatif == "+593") {
            $nom = 'Équateur';
        }
        if ($indicatif == "+291") {
            $nom = 'Érythrée';
        }
        if ($indicatif == "+34") {
            $nom = 'Espagne';
        }
        if ($indicatif == "+372") {
            $nom = 'Estonie';
        }
        if ($indicatif == "+251") {
            $nom = 'Éthiopie';
        }
        if ($indicatif == "+298") {
            $nom = 'Féroé (Îles)';
        }
        if ($indicatif == "+679") {
            $nom = 'Fidji';
        }
        if ($indicatif == "+358") {
            $nom = 'Finlande';
        }
        if ($indicatif == "+33") {
            $nom = 'France';
        }
        if ($indicatif == "+241") {
            $nom = 'Gabon';
        }
        if ($indicatif == "+220") {
            $nom = 'Gambie';
        }
        if ($indicatif == "+995") {
            $nom = 'Géorgie';
        }
        if ($indicatif == "+233") {
            $nom = 'Ghana';
        }
        if ($indicatif == "+350") {
            $nom = 'Gibraltar';
        }
        if ($indicatif == "+30") {
            $nom = 'Grèce';
        }
        if ($indicatif == "+1473") {
            $nom = 'Grenade';
        }
        if ($indicatif == "+299") {
            $nom = 'Groenland';
        }
        if ($indicatif == "+590") {
            $nom = 'Guadeloupe';
        }
        if ($indicatif == "+1671") {
            $nom = 'Guam';
        }
        if ($indicatif == "+502") {
            $nom = 'Guatemala';
        }
        if ($indicatif == "+224") {
            $nom = 'Guinée';
        }
        if ($indicatif == "+240") {
            $nom = 'Guinée équatoriale';
        }
        if ($indicatif == "+245") {
            $nom = 'Guinée-Bissau';
        }
        if ($indicatif == "+592") {
            $nom = 'Guyana';
        }
        if ($indicatif == "+594") {
            $nom = 'Guyane';
        }
        if ($indicatif == "+509") {
            $nom = 'Haïti';
        }
        if ($indicatif == "+504") {
            $nom = 'Honduras';
        }
        if ($indicatif == "+852") {
            $nom = 'Hong Kong';
        }
        if ($indicatif == "+36") {
            $nom = 'Hongrie';
        }
        if ($indicatif == "+91") {
            $nom = 'Inde';
        }
        if ($indicatif == "+62") {
            $nom = 'Indonésie';
        }
        if ($indicatif == "+964") {
            $nom = 'Irak';
        }
        if ($indicatif == "+98") {
            $nom = 'Iran';
        }
        if ($indicatif == "+353") {
            $nom = 'Irlande';
        }
        if ($indicatif == "+354") {
            $nom = 'Islande';
        }
        if ($indicatif == "+972") {
            $nom = 'Israël';
        }
        if ($indicatif == "+39") {
            $nom = 'Italie';
        }
        if ($indicatif == "+1876") {
            $nom = 'Jamaïque';
        }
        if ($indicatif == "+81") {
            $nom = 'Japon';
        }
        if ($indicatif == "+962") {
            $nom = 'Jordanie';
        }
        if ($indicatif == "+254") {
            $nom = 'Kenya';
        }
        if ($indicatif == "+996") {
            $nom = 'Kirghizistan';
        }
        if ($indicatif == "+686") {
            $nom = 'Kiribati';
        }
        if ($indicatif == "+965") {
            $nom = 'Koweït';
        }
        if ($indicatif == "+856") {
            $nom = 'Laos';
        }
        if ($indicatif == "+266") {
            $nom = 'Lesotho';
        }
        if ($indicatif == "+371") {
            $nom = 'Lettonie';
        }
        if ($indicatif == "+961") {
            $nom = 'Liban';
        }
        if ($indicatif == "+231") {
            $nom = 'Liberia';
        }
        if ($indicatif == "+218") {
            $nom = 'Libye';
        }
        if ($indicatif == "+423") {
            $nom = 'Liechtenstein';
        }
        if ($indicatif == "+370") {
            $nom = 'Lituanie';
        }
        if ($indicatif == "+352") {
            $nom = 'Luxembourg';
        }
        if ($indicatif == "+853") {
            $nom = 'Macao';
        }
        if ($indicatif == "+389") {
            $nom = 'Macédoine';
        }
        if ($indicatif == "+261") {
            $nom = 'Madagascar';
        }
        if ($indicatif == "+60") {
            $nom = 'Malaisie';
        }
        if ($indicatif == "+265") {
            $nom = 'Malawi';
        }
        if ($indicatif == "+960") {
            $nom = 'Maldives';
        }
        if ($indicatif == "+223") {
            $nom = 'Mali';
        }
        if ($indicatif == "+500") {
            $nom = 'Îles Malouines';
        }
        if ($indicatif == "+356") {
            $nom = 'Malte';
        }
        if ($indicatif == "+1670") {
            $nom = 'Mariannes du Nord (Îles)';
        }
        if ($indicatif == "+212") {
            $nom = 'Maroc';
        }
        if ($indicatif == "+692") {
            $nom = 'Marshall';
        }
        if ($indicatif == "+596") {
            $nom = 'Martinique';
        }
        if ($indicatif == "+230") {
            $nom = 'Maurice';
        }
        if ($indicatif == "+222") {
            $nom = 'Mauritanie';
        }
        if ($indicatif == "+262") {
            $nom = 'Mayotte';
        }
        if ($indicatif == "+52") {
            $nom = 'Mexique';
        }
        if ($indicatif == "+691") {
            $nom = 'Micronésie';
        }
        if ($indicatif == "+373") {
            $nom = 'Moldavie';
        }
        if ($indicatif == "+377") {
            $nom = 'Monaco';
        }
        if ($indicatif == "+976") {
            $nom = 'Mongolie';
        }
        if ($indicatif == "+382") {
            $nom = 'Monténégro';
        }
        if ($indicatif == "+1664") {
            $nom = 'Montserrat';
        }
        if ($indicatif == "+258") {
            $nom = 'Mozambique';
        }
        if ($indicatif == "+264") {
            $nom = 'Namibie';
        }
        if ($indicatif == "+674") {
            $nom = 'Nauru';
        }
        if ($indicatif == "+977") {
            $nom = 'Népal';
        }
        if ($indicatif == "+505") {
            $nom = 'Nicaragua';
        }
        if ($indicatif == "+227") {
            $nom = 'Niger';
        }
        if ($indicatif == "+234") {
            $nom = 'Nigeria';
        }
        if ($indicatif == "+683") {
            $nom = 'Niue';
        }
        if ($indicatif == "+47") {
            $nom = 'Norvège';
        }
        if ($indicatif == "+687") {
            $nom = 'Nouvelle-Calédonie';
        }
        if ($indicatif == "+64") {
            $nom = 'Nouvelle-Zélande';
        }
        if ($indicatif == "+968") {
            $nom = 'Oman';
        }
        if ($indicatif == "+256") {
            $nom = 'Ouganda';
        }
        if ($indicatif == "+998") {
            $nom = 'Ouzbékistan';
        }
        if ($indicatif == "+92") {
            $nom = 'Pakistan';
        }
        if ($indicatif == "+680") {
            $nom = 'Palaos';
        }
        if ($indicatif == "+970") {
            $nom = 'Palestine';
        }
        if ($indicatif == "+507") {
            $nom = 'Panama';
        }
        if ($indicatif == "+675") {
            $nom = 'Papouasie-Nouvelle-Guinée';
        }
        if ($indicatif == "+595") {
            $nom = 'Paraguay';
        }
        if ($indicatif == "+31") {
            $nom = 'Pays-Bas';
        }
        if ($indicatif == "+51") {
            $nom = 'Pérou';
        }
        if ($indicatif == "+63") {
            $nom = 'Philippines';
        }
        if ($indicatif == "+48") {
            $nom = 'Pologne';
        }
        if ($indicatif == "+689") {
            $nom = 'Polynésie française';
        }
        if ($indicatif == "+351") {
            $nom = 'Portugal';
        }
        if ($indicatif == "+974") {
            $nom = 'Qatar';
        }
        if ($indicatif == "+262") {
            $nom = 'Réunion';
        }
        if ($indicatif == "+40") {
            $nom = 'Roumanie';
        }
        if ($indicatif == "+44") {
            $nom = 'Royaume-Uni';
        }
        if ($indicatif == "+7") {
            $nom = 'Russie ou Kazakhstan';
        }
        if ($indicatif == "+250") {
            $nom = 'Rwanda';
        }
        if ($indicatif == "+1869") {
            $nom = 'Saint-Christophe-et-Niévès';
        }
        if ($indicatif == "+290") {
            $nom = 'Sainte-Hélène, Ascension';
        }
        if ($indicatif == "+1758") {
            $nom = 'Sainte-Lucie';
        }
        if ($indicatif == "+378") {
            $nom = 'Saint-Marin';
        }
        if ($indicatif == "+1721") {
            $nom = 'Saint-Martin (Sint-Maarten)';
        }
        if ($indicatif == "+590") {
            $nom = 'Saint-Martin';
        }
        if ($indicatif == "+508") {
            $nom = 'Saint-Pierre-et-Miquelon';
        }
        if ($indicatif == "+1784") {
            $nom = 'Saint-Vincent-et-les-Grenadines ';
        }
        if ($indicatif == "+677") {
            $nom = 'Salomon';
        }
        if ($indicatif == "+503") {
            $nom = 'Salvador';
        }
        if ($indicatif == "+685") {
            $nom = 'Samoa';
        }
        if ($indicatif == "+1684") {
            $nom = 'Samoa américaines';
        }
        if ($indicatif == "+239") {
            $nom = 'Sao Tomé-et-Principe';
        }
        if ($indicatif == "+221") {
            $nom = 'Sénégal';
        }
        if ($indicatif == "+381") {
            $nom = 'Serbie';
        }
        if ($indicatif == "+248") {
            $nom = 'Seychelles';
        }
        if ($indicatif == "+232") {
            $nom = 'Sierra Leone';
        }
        if ($indicatif == "+65") {
            $nom = 'Singapour';
        }
        if ($indicatif == "+421") {
            $nom = 'Slovaquie';
        }
        if ($indicatif == "+386") {
            $nom = 'Slovénie';
        }
        if ($indicatif == "+252") {
            $nom = 'Somalie';
        }
        if ($indicatif == "+249") {
            $nom = 'Soudan';
        }
        if ($indicatif == "+211") {
            $nom = 'Soudan du Sud';
        }
        if ($indicatif == "+94") {
            $nom = 'Sri Lanka';
        }
        if ($indicatif == "+46") {
            $nom = 'Suède';
        }
        if ($indicatif == "+41") {
            $nom = 'Suisse';
        }
        if ($indicatif == "+597") {
            $nom = 'Suriname';
        }
        if ($indicatif == "+268") {
            $nom = 'Swaziland';
        }
        if ($indicatif == "+963") {
            $nom = 'Syrie';
        }
        if ($indicatif == "+992") {
            $nom = 'Tadjikistan';
        }
        if ($indicatif == "+255") {
            $nom = 'Tanzanie';
        }
        if ($indicatif == "+886") {
            $nom = 'Taïwan';
        }
        if ($indicatif == "+235") {
            $nom = 'Tchad';
        }
        if ($indicatif == "+420") {
            $nom = 'République tchèque';
        }
        if ($indicatif == "+672") {
            $nom = 'Territoires extérieurs - Australie';
        }
        if ($indicatif == "+66") {
            $nom = 'Thaïlande';
        }
        if ($indicatif == "+670") {
            $nom = 'Timor oriental';
        }
        if ($indicatif == "+228") {
            $nom = 'Togo';
        }
        if ($indicatif == "+690") {
            $nom = 'Tokelau';
        }
        if ($indicatif == "+676") {
            $nom = 'Tonga';
        }
        if ($indicatif == "+1868") {
            $nom = 'Trinité-et-Tobago';
        }
        if ($indicatif == "+216") {
            $nom = 'Tunisie';
        }
        if ($indicatif == "+993") {
            $nom = 'Turkménistan';
        }
        if ($indicatif == "+1649") {
            $nom = 'Turks et Caico (Îles)';
        }
        if ($indicatif == "+90") {
            $nom = 'Turquie';
        }
        if ($indicatif == "+688") {
            $nom = 'Tuvalu';
        }
        if ($indicatif == "+380") {
            $nom = 'Ukraine';
        }
        if ($indicatif == "+598") {
            $nom = 'Uruguay';
        }
        if ($indicatif == "+678") {
            $nom = 'Vanuatu';
        }
        if ($indicatif == "+39") {
            $nom = 'Vatican (Saint-Siège)';
        }
        if ($indicatif == "+58") {
            $nom = 'Venezuela';
        }
        if ($indicatif == "+1340") {
            $nom = 'Vierges Américaines (Îles)';
        }
        if ($indicatif == "+1284") {
            $nom = 'Vierges Britanniques (Îles)';
        }
        if ($indicatif == "+84") {
            $nom = 'Viêt Nam';
        }
        if ($indicatif == "+681") {
            $nom = 'Wallis-et-Futuna';
        }
        if ($indicatif == "+967") {
            $nom = 'Yémen';
        }
        if ($indicatif == "+260") {
            $nom = 'Zambie';
        }
        if ($indicatif == "+263") {
            $nom = 'Zimbabwe';
        }

        return $nom;
    }
}

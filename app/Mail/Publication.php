<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Publication extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct( $data)
    {
        
        $this->data = $data;
        
    }

    public function build()
    {
        $description = $this->data['description'];
        
        /*$nom = $this->data['nom'];
        return $this->subject('subject email')
        ->view('email_template', compact('nom'))
        ->attach($this->data['image']->getRealPath(),[
            'as' =>$this->data['image']->getClientOriginalName()

            ]);*/
        return $this->from("") // L'expéditeur
            ->subject($this->data['nom']) // Le sujet
            ->view('emails.publication', compact('description'))
            ->attach($this->data['image']->getRealPath(),[
                'as' =>$this->data['image']->getClientOriginalName()
    
                ]); // La vue
    }
}

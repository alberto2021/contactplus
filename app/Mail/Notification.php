<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Notification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $module;
    public $user;

    public function __construct($module, $user)
    {
        $this->module = $module;
        $this->user = $user;
    }

    public function build()
    {
        return $this->from("") // L'expéditeur
            ->subject("Activation de module par un utilisateur") // Le sujet
            ->view('emails.notification'); // La vue
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminInsert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $code_conf;

    public function __construct($code_conf)
    {
        $this->code_conf = $code_conf;
    }

    public function build()
    {
        return $this->from("") // L'expéditeur
            ->subject("Code de confirmation pour administration") // Le sujet
            ->view('emails.code_conf_admin'); // La vue
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aimer extends Model
{
    use HasFactory;
    protected $table = 'aimers';

    protected $fillable = [
        'pub_id',
        'user_id',
    ];
}

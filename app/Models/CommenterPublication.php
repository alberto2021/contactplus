<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommenterPublication extends Model
{
    use HasFactory;
    protected $table = 'commenter_publications';

    protected $fillable = [
        'user_id',
        'pub_id',
        'commenter'
    ];
}

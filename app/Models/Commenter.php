<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commenter extends Model
{
    use HasFactory;
    protected $table = 'commenters';

    protected $fillable = [
        'user_id',
        'pub_id',
        'commenter'
    ];
}

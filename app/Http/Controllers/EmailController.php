<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class EmailController extends Controller
{
    public function index()
    {
        return view('email');
    }

    public function send(Request $request)
    {
        
      $data = [
          'nom'=> $request->nom,
          'image'=>$request->file('image')
      ];
    $users= User::all();
    foreach ($users as $user){
       \Mail::to($user->email)->send(new \App\Mail\TestMail($data));   
    }
     
        
    echo 'Email envoyé avec succès';
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aimer;
use App\Models\AimerPublication;
use App\Models\Publication;
use App\Models\Admin;
use App\Models\User;
use App\Models\Publicite;
use Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;


class AimerController extends Controller
{
    public function insert_like(Request $request)
    {
        if (Auth::check() and Auth()->user()->is_customer == True) {
            try {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'id_pub' => 'bail|required',
                    ],
                );
    
                $re =   Aimer::Create([
                    'pub_id' => $request->id_pub,
                    'user_id' => Auth::user()->id
                ]);
    
                $old_value = Publicite::where('id', $request->id_pub)
                    ->get()
                    ->pluck('compteur_like')[0];
    
                Publicite::where('id', $request->id_pub)
                    ->update([
                        'compteur_like' => $old_value + 1,
                    ]);
    
                $publications = Publication::where('is_active', 1)
                    ->where('date_end', '>=', now()->toDateTimeString())
                    ->orderBy('created_at', 'desc')
                    ->get();
    
    
    
                $publicites = Publicite::where('is_active', 1)
                    ->where('date_end', '>=', now()->toDateTimeString())
                    ->orderBy('created_at', 'desc')
                    ->get();
    
                $users = User::all();
                $admin = Admin::where('username', "admins")->first();
    
                // return redirect()->route('customers_dashboard')->with([
                //     'publicites' => $publicites,
                //     'publications' => $publications,
                //     'users' => $users,
                //     'admin' => $admin,
                // ]);
    
                return redirect()->back()->with([
                    'response' =>  'Publication bien aimée avec succes',
                    'publicites' => $publicites,
                    'publications' => $publications,
                    'users' => $users,
                    'admin' => $admin,
                ]);
    
            } catch (QueryException $th) {
                return redirect()->back()->with(['error' =>  'Vous avez déjà aimé cette publication. Merci!']);
    
                //return redirect()->route('customers_dashboard')->withNoenter('Vous avez déjà aimé cette publication. Merci !');
            }
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }


        
    }

    //LIKE PUBLICATION

    public function insert_like_publication(Request $request)
    {
        if (Auth::check() and Auth()->user()->is_customer == True) {

            try {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'id_publication' => 'bail|required',
                    ],
                );
    
                AimerPublication::Create([
                    'pub_id' => $request->id_publication,
                    'user_id' => Auth::user()->id
                ]);
               
    
                $old_value = Publication::where('id', $request->id_publication)
                                        ->get()
                                        ->pluck('compteur_like_publication')[0];

                  
                    Publication::where('id', $request->id_publication)
                                ->update([
                                    'compteur_like_publication' => $old_value + 1,
                                ]);
    
                $publications = Publication::where('is_active', 1)
                                            ->where('date_end', '>=', now()->toDateTimeString())
                                            ->orderBy('created_at', 'desc')
                                            ->get();
    
    
    
                $publicites = Publicite::where('is_active', 1)
                                        ->where('date_end', '>=', now()->toDateTimeString())
                                        ->orderBy('created_at', 'desc')
                                        ->get();
    
                $users = User::all();
                $admin = Admin::where('username', "admins")->first();
        
    
                return redirect()->back()->with([
                    'response' =>  'Publication bien aimée avec succes',
                    'publicites' => $publicites,
                    'publications' => $publications,
                    'users' => $users,
                    'admin' => $admin,
                ]);
    
            } catch (QueryException $th) {
                return redirect()->back()->with(['error' =>  'Vous avez déjà aimé cette publication. Merci!']);
    
            }
        }
        else {
            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');
        }
        
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Commenter;
use App\Models\CommenterPublication;
use App\Models\Publication;
use App\Models\Admin;
use App\Models\User;
use App\Models\Publicite;
use Auth;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

class CommenterController extends Controller
{
    public function insert_commenter(Request $request)
    {
        if (Auth::check() and Auth()->user()->is_customer == True) {
            try {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'commenter_id_pub' => 'bail|required',
                        'commentaire' => 'bail|required',
                    ],
                );
    
                $re =   Commenter::Create([
                    'pub_id' => $request->commenter_id_pub,
                    'commenter' => $request->commentaire,
                    'user_id' => Auth::user()->id
                ]);
    
                $old_value = Publicite::where('id', $request->commenter_id_pub)
                    ->get()
                    ->pluck('compteur_commenter')[0];
    
                Publicite::where('id', $request->commenter_id_pub)
                    ->update([
                        'compteur_commenter' => $old_value + 1,
                    ]);
    
                $publications = Publication::where('is_active', 1)
                    ->where('date_end', '>=', now()->toDateTimeString())
                    ->orderBy('created_at', 'desc')
                    ->get();
    
    
    
                $publicites = Publicite::where('is_active', 1)
                    ->where('date_end', '>=', now()->toDateTimeString())
                    ->orderBy('created_at', 'desc')
                    ->get();
    
                $users = User::all();
                $admin = Admin::where('username', "admins")->first();
    
                // return redirect()->route('customers_dashboard')->with([
                //     'publicites' => $publicites,
                //     'publications' => $publications,
                //     'users' => $users,
                //     'admin' => $admin,
                // ]);
                return redirect()->back()->with([
                    'response' =>  'Commentaire effectue avec succes',
                    'publicites' => $publicites,
                    'publications' => $publications,
                    'users' => $users,
                    'admin' => $admin,
                ]);
    
            } catch (QueryException $th) {
                return redirect()->route('customers_dashboard')->withNoenter('Vous avez déjà aimé cette publication. Merci !');
            }
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');
        }

    }




    public function insert_commenter_publication(Request $request)
    {
        if (Auth::check() and Auth()->user()->is_customer == True) {
            try {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'commenter_id_pub2' => 'bail|required',
                        'commentaire2' => 'bail|required',
                    ],
                );
    
                CommenterPublication::Create([
                    'pub_id' => $request->commenter_id_pub2,
                    'commenter' => $request->commentaire2,
                    'user_id' => Auth::user()->id
                ]);
    
                $old_value = Publication::where('id', $request->commenter_id_pub2)
                                        ->get()
                                        ->pluck('compteur_commenter_publication')[0];
    
                Publication::where('id', $request->commenter_id_pub2)
                            ->update([
                                'compteur_commenter_publication' => $old_value + 1,
                            ]);
    
                $publications = Publication::where('is_active', 1)
                                            ->where('date_end', '>=', now()->toDateTimeString())
                                            ->orderBy('created_at', 'desc')
                                            ->get();
    
    
    
                $publicites = Publicite::where('is_active', 1)
                                        ->where('date_end', '>=', now()->toDateTimeString())
                                        ->orderBy('created_at', 'desc')
                                        ->get();
    
                $users = User::all();
                $admin = Admin::where('username', "admins")->first();
    
               
                return redirect()->back()->with([
                    'response' =>  'Commentaire effectue avec succes',
                    'publicites' => $publicites,
                    'publications' => $publications,
                    'users' => $users,
                    'admin' => $admin,
                ]);
    
            } catch (QueryException $th) {
                return redirect()->route('customers_dashboard')->withNoenter('Vous avez déjà aimé cette publication. Merci !');
            }
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');
        }

    }
}

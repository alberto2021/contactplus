<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\Admin;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminInsert;
use App\Mail\AdminMailForUser;
use Illuminate\Support\Facades\Auth;
use App\Models\Contact;
use App\Models\User;
use App\Models\Publicite;
use App\Models\Publication;
use App\Models\Message;
use App\Models\Event;
use App\Http\Controllers\WhatsappAPI;
use App\Mail\Publication as PublicationMail;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Spatie\SimpleExcel\SimpleExcelReader;
use App\Rules\MatchOldPassword;
use Carbon\Carbon;

class AdminsController extends Controller
{
    
    public function download_contact() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
              
          ) {

            $users = User::all();
            
            $file_name = '_users'.time().'.'.'vcf';

            $path = "uploads/contacts".''.$file_name;
            
            foreach($users as $contact){
                file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                file_put_contents($path, "\nN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom};;;;", FILE_APPEND);
                file_put_contents($path, "\nFN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom}", FILE_APPEND);
                file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
            }

            return response()->download(public_path('uploads'.'/'.'contacts'.''.$file_name));
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    // fonction de gestion de la connection
    public function login_view() {
 
        return view('generals.admins.login');
    }


    public function login(Request $request) {

        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'remember' => '',
        ]);


        if ( (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password, 'is_simple_admin' => 1, 'is_valid' => 1 ]) ) 
            or (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password, 'is_super_admin' => 1, 'is_valid' => 1 ]) )) {

            $request->session()->regenerate();
            return redirect()->route('admins_dashboard');
        }
        else {

            return redirect()->back()->withErrore('Identifiants incorrects. Veuillez vérifier vos informations de connexion');

        }
        
    }


    // fonction de gestion de la connection
    public function join_view() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('generals.admins.join');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }



    public function join(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'username' => 'required|unique:admins',
                'email' => 'required|email|unique:admins',
                'password' => 'required|min:8|max:15',
                'confirm_password' => 'required|min:8|max:15|same:password',
                'wanted' => 'required',
            ]);

            $code_conf = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 10);

            session([
                'admin_email' => $request->email,
                'admin_username' => $request->username,
                'admin_password' => Hash::make($request->password),
                'admin_wanted' => $request->wanted,
                'mita' => Hash::make($code_conf),
                'one' => "oui",
            ]);


            Mail::to("groupeducommercesocial@gmail.com")->queue(new AdminInsert($code_conf));
                


            return redirect()->route('admins_join');

        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
        
    }


    public function join_confirm(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'code_conf' => 'required',
            ]);

            if (Hash::check($request->code_conf, session('mita'))) {

                $admin = new Admin;
                $admin->username = session('admin_username');
                $admin->email = session('admin_email');
                $admin->password = session('admin_password');
                if (session('admin_wanted') == "simple"){
                    $admin->is_simple_admin = True;
                    $admin->is_valid = True;
                }
                else{
                    $admin->is_simple_admin = False;
                    $admin->is_super_admin = True;
                    $admin->is_valid = True;
                }
                $admin->created_by = Auth()->guard('admin')->user()->username;
                $admin->save();

                session::forget('admin_username');
                session::forget('admin_email');
                session::forget('admin_password');
                session::forget('admin_wanted');
                session::forget('mita');
                session::forget('one');
                return redirect()->route('admins_dashboard')->withOkay('Insertion de l\'administrateur réussie');
            }
            else{

                return redirect()->back()->withError('Le code de confirmation renseigné est incorrect.');
            }

        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
        
    }


    public function logout(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {
    
            Auth::guard('admin')->logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route('admins_login')->withDeconnexion('Vous vous êtes déconnecté avec succès. Merci.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function dashboard() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('dashboard.admins.welcome');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function list_of_admins() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $admins = Admin::all();
            return view('dashboard.admins.list_of_admins', [
                'admins' => $admins,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_contact() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
              
          ) {

            return view('dashboard.admins.add_contact');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_contact_with_form() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
              
          ) {

            return view('dashboard.admins.add_contact_form');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_contact_with_form_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
              
          ) {

            $request->validate([
                'nom' => 'required',
                'phone' => 'required|unique:contacts',
                'wanted' => 'required',
            ]);

            $contact = new Contact;
            $contact->fullname = $request->nom;
            $contact->phone = $request->phone;
            $contact->type = $request->wanted;
            $contact->created_by = Auth()->guard('admin')->user()->username;
            $contact->save();

            return redirect()->route('admins_add_contact_with_form')->withContact('Vous aviez ajouté avec succès un contact dans la base de données. Merci!');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_contact_with_excel() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            return view('dashboard.admins.add_contact_excel');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_contact_with_excel_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
             
          ) {

            $request->validate([
                'file' => 'required',
                
            ]);

            $file = $request->file('file');
            $filename =$file->getClientOriginalName();
            $path = 'uploads/contacts';
            $fichier = $file->move($path, $filename);
            $reader = SimpleExcelReader::create($fichier);
            $rows = $reader->getRows();
            $status = Contact::insert($rows->toArray());
            if ($status) {

                // 5. On supprime le fichier uploadé
                $reader->close(); // On ferme le $reader
                unlink($fichier);

                // 6. Retour vers le formulaire avec un message $msg
                return redirect()->route('admins_add_contact_with_excel')->withExcel('Vous aviez ajouté avec succès tous les contacts dans la base de données. Merci!');

            }
            /*$file = $request->file('file');
            if ($file !== null) {
                $filename =$file->getClientOriginalName();
                $path = 'uploads/contacts';
                $file->move($path, $filename);

                $filepath = public_path($path . "/" . $filename);
                $file = fopen($filepath, "r");
                $importData_arr = array();
                $i = 0;
                while ( ($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    if ($i == 0) {
                        $i++;
                        continue;
                    }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);
                $j = 0;
                foreach ($importData_arr as $importData) {
                    $name = $importData[0]; //Get user names
                    $phone = $importData[1]; //Get the user emails
                    
                    $j++;

                    $contact = new Contact;
                    $contact->fullname = $importData[0];
                    $contact->phone = $importData[1];
                    $contact->type = "admin";
                    $contact->created_by = Auth()->guard('admin')->user()->username;
                    $contact->save();
                }

                return redirect()->route('admins_add_contact_with_excel')->withExcel('Vous aviez ajouté avec succès tous les contacts dans la base de données. Merci!');

            }*/
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function send_message() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $users = User::all();
            return view('dashboard.admins.send_message', [
                'users' => $users,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    #------CHANGEMENT DE SON MOT DE PASSE -------#

    public function changer_password()
    {
        return view('dashboard.admins.changer_password');
    }

    public function changer_mot_password(Request $request)
    {
        //dd($request->new_confirm_password);
        $request->validate([
            'ancien_password' => ['required', new MatchOldPassword],
            'nouveau_password' => ['required'],
            'confirme_password' => ['same:nouveau_password'],

        ]);

       // Admin::find(auth()->user()->id)->update(['password'=> Hash::make($request->nouveau_password)]);

          Admin::where('id',Auth()->guard('admin')->user()->id)
               ->update([
                        'password'=> Hash::make($request->nouveau_password)
                        ]);

        return redirect()->route('admins_login');
    }


    public function filter_of_customers()
    {
        if ((Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
            or
            (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
        ) {
            $customers =  User::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                ->orderBy('created_at', 'desc')
                                ->get();

            $number = User::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                                ->count();

              $what = 'Liste des utilisateurs de cette semaine '.' ('.$number.' )';

           

            return view('dashboard.admins.filter_of_customers', [
                'customers' => $customers,
                'what' => $what,
            ]);
        } else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');
        }
    }

    public function filter_of_customers_by_reseach(Request $request)
    {
       
        if ((Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
            or
            (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
        ) {
            
            $request->validate([
                'search' => 'required',
            ]);
            $number = 0;
            
            if ($request->search == 'pays') {
                $req = '';
                
                $req = country_name($request->option_reseach1);

                $customers = User::where('phone', 'LIKE', '%' .$request->option_reseach1.'%')
                    ->orderBy('created_at', 'desc')
                    ->get();
                $number = User::where('phone', 'LIKE', '%' .$request->option_reseach1.'%')
                                ->count();
                $what = 'Liste des utilisateurs qui viennent de  '.$req.' ('.$number.' )';

            } elseif ($request->search == 'profession') {

                $customers = User::where('profession', 'LIKE', '%' .$request->option_reseach.'%')
                    ->orderBy('created_at', 'desc')
                    ->get();

                $number = User::where('profession', 'LIKE', '%' .$request->option_reseach.'%')
                                ->count();
                $what = 'Liste des utilisateurs qui sont  '.$request->option_reseach.' ('.$number.' )';

            } elseif ($request->search == 'ville') {
                $customers = User::where('adresse', 'LIKE', '%' .$request->option_reseach.'%')
                    ->orderBy('created_at', 'desc')
                    ->get();

                $number = User::where('adresse', 'LIKE', '%' .$request->option_reseach.'%')
                                ->count();

                $what = 'Liste des utilisateurs qui viennent de  '.$request->option_reseach.' ('.$number.' )';

            }elseif ($request->search == 'nom') {
                $customers = User::where('nom', 'LIKE', '%' .$request->option_reseach.'%')
                                 ->orWhere('prenom', 'LIKE', '%' .$request->option_reseach.'%')
                                 ->orderBy('created_at', 'desc')
                                 ->get();

                $number = User::where('nom', 'LIKE', '%' .$request->option_reseach.'%')
                                ->orWhere('prenom', 'LIKE', '%' .$request->option_reseach.'%')
                                ->count();

                $what = 'Liste des utilisateurs appelant  '.$request->option_reseach.' ('.$number.' )';

            }elseif ($request->search == 'interval') {
                $customers = User::whereBetween('created_at',[$request->date1.' 01:00:00', $request->date2.' 23:59:59'])
                ->orderBy('created_at', 'desc')
                ->get();

               $number = User::whereBetween('created_at',[$request->date1.' 01:00:00', $request->date2.' 23:59:59'])
                                ->count();

              $what = 'Liste des utilisateurs entre '
              .implode('/',array_reverse(explode('-',$request->date1))).' au '
              .implode('/',array_reverse(explode('-',$request->date2))).  ' ('.$number.' )';

            }

            return view('dashboard.admins.filter_of_customers', [
                'customers' => $customers,
                'what' => $what,
            ]);
        } else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');
        }
    }




    public function do_publication() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('dashboard.admins.do_publication');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function list_of_customers() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $customers = User::all();
            $number = 5;
            return view('dashboard.admins.list_of_customers', [
                'customers' => $customers,
                'number' => $number,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function search_customers(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) { 

            $request->validate([
                'search' => 'required',   
            ]);
            if ($request->search == "all"){
                $number = 5;
                $customers = User::all();
            }
            else{
                $number = 0;
                $customers = User::where('mode', $request->search)
                ->orderBy('created_at', 'desc')
                ->get();
            }
            return view('dashboard.admins.list_of_customers', [
                'customers' => $customers,
                'what' => $request->search,
                'number' => $number,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function do_publication_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'nom' => 'required', 
                'description' => '',
                'duree' => 'required',
                'video' => '',
                'image' => '',
            ]);

            $pub = new Publication;
            $pub->title = $request->nom;
            $pub->description = $request->description;
            $pub->duree = $request->duree;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;
            $pub->ordre = 1;
            

            $file_video = $request->file('video');
            if ($file_video !== null) {
                $filename =$file_video->getClientOriginalName();
                $path = 'uploads/videos';
                $file_video->move($path, $filename);
                $pub->video = $filename; 
                
            }


            $file_image = $request->file('image');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/images';
                $file_image->move($path, $filename);

                $pub->image = $filename;
            }

            $pub->save();
            
            return redirect()->route('admins_do_publication')->withPublication('Votre publication a été publié avec succès .Merci');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function do_publicity() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('dashboard.admins.do_publicity');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function do_publicity_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'title' => 'required', 
                'description' => 'required',
                'duree' => 'required|numeric',
            ]);

            $pub = new Publicite;
            $pub->title = $request->title;
            $pub->contenu = $request->description;
            $pub->duree = $request->duree;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;
            $pub->ordre = 1;
            $pub->save();
            
            return redirect()->route('admins_do_publicity')->withPublicite('Votre publicité a été publié avec succès .Merci');
            
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }

    public function send_publication() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('dashboard.admins.pub_for_email');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }




    public function send_publication_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
          or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'nom' => 'required', 
                'description' => '',
                'video' => '',
                'image' => '',
            ]);

            $pub = new Publication;
            $pub->title = $request->nom;
            $pub->description = $request->description;
            $pub->duree = $request->duree;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;
            $pub->ordre = 1;
            $pub->is_active = 0;
            

            $file_video = $request->file('video');
            if ($file_video !== null) {
                $filename =$file_video->getClientOriginalName();
                $path = 'uploads/videos';
                $file_video->move($path, $filename);
                $pub->video = $filename;
                
            }


            $file_image = $request->file('image');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/images';
                $file_image->move($path, $filename);

                $pub->image = $filename;
            }

            $pub->save();

            $emails= User::all();
            $img = $pub->image;
            $video = $pub->video;
            $description = $request->description;
            $nom = $request->nom;
            
            $data  = [
                'nom' => $request->nom, 
                'description' => $request->description,
                'video' => $request->video,
                'image' => $request->file('image'),
            ];
            
            foreach($emails as $email){
                Mail::to($email->email)->send(new PublicationMail($data));
            }

            return redirect()->route('admins_send_publication')->withPublication('Votre publication a été publié avec succès par Mail .Merci');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function write_message($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            $user = User::find($id);
            return view('dashboard.admins.write_message', [
                'user' => $user,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function write_message_form(Request $request, $id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            $request->validate([
                'objet' => 'required', 
                'message' => 'required',
                
            ]);
            $user = User::find($id);
            
            Mail::to($user->email)->queue(new AdminMailForUser( $request->all() ));

            return redirect()->route('admins_send_message')->withSendit('Votre message a été envoyé avec succès par Mail .Merci');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function send_email_to_all() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            return view('dashboard.admins.send_email_to_all');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }



    public function send_email_to_all_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            $request->validate([
                'objet' => 'required', 
                'message' => 'required',
                
            ]);

            $emails= User::all();
            
            foreach($emails as $email){
                Mail::to($email->email)->queue(new AdminMailForUser( $request->all() ));
            }

            return redirect()->route('admins_send_email_to_all')->withMailok('Votre mail a été envoyé avec succès par Mail à tous les utilisateurs. Merci');

        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function send_message_what($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            $user = User::find($id);
            $tabs = ["+22966403090", "+22961597581", "+22998736175" ];
            $obj = new WhatsappAPI; // create an object of the WhatsappAPI class
            foreach ($tabs as $value) {
                $status = $obj->send($value, "C'est un message venant de DAVI Fréjus."); // NOTE: Phone Number should be with country
            }
            

            $status = json_decode($status);
            return view('dashboard.admins.write_message', [
                'user' => $user,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }



    public function add_visitors() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {

            $users = User::all();
            $admin = Admin::where('username', "admins")->first();

            return view('dashboard.admins.add_visitors', [
                'users' => $users,
                'admin' => $admin,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_visitors_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
              
          ) {

            $users = User::all();
            $request->validate([
                'visitor' => 'required',                 
            ]);
            
            $admin = Admin::where('username', 'admins')->update([
                'paid' => $request->visitor,               
            ]);

            return redirect()->route('admins_add_visitors')->withVisiteur('Vous aviez mis à jour avec succès le nombre de visiteurs.');
            
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


     public function send_message_to_users() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            return view('dashboard.admins.send_message_to_users');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function send_message_to_users_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {

            $request->validate([
                'description' => 'required',
                'duree' => 'required',                 
            ]);

            $message = new Message;
            $message->message = $request->description;
            $message->duree = $request->duree;
            $message->created_by = Auth()->guard('admin')->user()->username;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $message->date_end = $date_end;
            $message->save();
            return redirect()->route('admins_send_message_to_users')->withMessage('Votre message est envoyé avec succès.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }



    public function list_of_publicities() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {   

            $publicities = Publicite::where('is_active', 1)
                                    ->orderBy('ordre', 'desc')
                                    ->get();
            return view('dashboard.admins.list_of_publicities', [
                'publicities' => $publicities,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function delete_publicities($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
          ) {   

            $publicities = Publicite::find($id);
            $publicities->delete();
            return redirect()->route('admins_list_of_publicities')->withMessage('Publicité supprimé avec succès de la base de données.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }



    public function list_of_publications() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {   

            $publicities = Publication::where('is_active', 1)
            ->orderBy('created_at', 'desc')
            ->get();
            return view('dashboard.admins.list_of_publications', [
                'publicities' => $publicities,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function delete_publications($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
          ) {   

            $publicities = Publication::find($id);
            $publicities->delete();
            return redirect()->route('admins_list_of_publications')->withMessage('Publication supprimé avec succès de la base de données.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function pub_to_home_page() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            
            return view('dashboard.admins.pub_to_home_page');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function pub_to_home_page_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            $request->validate([
                'nom' => 'required', 
                'description' => 'required',
                'image' => 'required',
            ]);

            $pub = new Publication;
            $pub->title = $request->nom;
            $pub->description = $request->description;
            $pub->duree = 0;
            $pub->is_active = False;
            $pub->ordre = 1;

            $file_image = $request->file('image');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/images';
                $file_image->move($path, $filename);

                $pub->image = $filename;
            }

            $pub->save();

            return redirect()->route('admins_pub_to_home_page')->withPublication('Publication publiée avec succès sur la page d\'accueil.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function announce_event() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            
            return view('dashboard.admins.announce_event');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function announce_event_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            $request->validate([
                'nom' => 'required', 
                'description' => 'required',
                'image' => '',
                'duree' => 'required',
            ]);

            $pub = new Event;
            $pub->title = $request->nom;
            $pub->contenu = $request->description;
            $pub->duree = $request->duree;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;

            $file_image = $request->file('image');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/events';
                $file_image->move($path, $filename);

                $pub->image = $filename;
            }

            $pub->save();

            return redirect()->route('admins_announce_event')->withPublication('Votre annonce d\'évènement est publiée avec succès.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function add_video() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            
            return view('dashboard.admins.add_video');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }

    public function add_video_form(Request $request) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid)
              
          ) {   

            $request->validate([
                'nom' => 'required', 
                'video' => 'required',
                 'duree' => 'required', 
            ]);

            $pub = new Publication;
            $pub->title = $request->nom;
            $pub->description = "video";
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;
            $pub->duree = $request->duree;
            $pub->is_active = False;
            $pub->ordre = 1;

            $file_image = $request->file('video');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/videos';
                $file_image->move($path, $filename);

                $pub->video = $filename;
            }

            $pub->save();
            
            return redirect()->route('admins_add_video')->withPublication('Succès');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function delete_user($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
          ) {   

            $user = User::find($id);
            $user->delete();
            return redirect()->route('admins_list_of_customers')->withMessage('Utilisateur supprimé avec succès.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function list_of_events() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {   

            $events = Event::all();
            return view('dashboard.admins.list_of_events', [
                'events' => $events,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function delete_event($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
          ) {   

            $user = Event::find($id);
            $user->delete();
            return redirect()->route('admins_list_of_events')->withMessage('Evènement supprimé avec succès.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function list_of_pub_videos() {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) or 
              (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_simple_admin  and Auth()->guard('admin')->user()->is_valid)
          ) {   

            $videos = Publication::where('description', "video")->orderBy('ordre', 'desc')->get();
            return view('dashboard.admins.list_of_pub_videos', [
                'videos' => $videos,
            ]);
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function delete_video($id) {

        if ( (Auth::guard('admin')->check() and Auth()->guard('admin')->user()->is_super_admin  and Auth()->guard('admin')->user()->is_valid) 
          ) {   

            $user = Publication::find($id);
            $user->delete();
            return redirect()->route('admins_list_of_pub_videos')->withMessage('Vidéo supprimé avec succès.');
        }
        else {

            return redirect()->route('admins_login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
}

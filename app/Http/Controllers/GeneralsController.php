<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use App\Models\User;
use App\Mail\ResetPassword;
use App\Models\Publication;

class GeneralsController extends Controller
{
     // fonction de gestion de la page d'accueil
    public function welcome() {

        $users = User::all();
        $affiche = Publication::where('is_active', 0)
        ->where('duree', 0)
        ->where('description', '!=', "video")
        ->latest('created_at')->first();
        $admin = Admin::where('username', "admins")->first();
        return view('generals.welcome', [
                'admin' => $admin,
                'users' => $users,
                'affiche' => $affiche,
            ]);
    }


    // fonction de gestion de la connection
    public function login_view() {

        return view('generals.login');
    }


    // fonction de gestion de la inscription
    public function join_view() {

        if (session('changed')) {

            session([
            'etape' => session('etape'),            
            ]);
        }
        else {
            session([
            'etape' => "first step",            
            ]);
        }
        
        return view('generals.join');
    }


    public function watch_video() {
            

            $videos = Publication::where('is_active', 0)
            ->where('date_end', '>=', now()->toDateTimeString())
            ->where('description', "video")
            ->orderBy('created_at', 'desc')
            ->get();
       

        return view('generals.watch_video', [
            'videos' => $videos,
        ]);
    }
    
    public function active_pack() {
        if(Auth::check() and Auth()->user()->is_customer){
             return redirect()->route('customers_dashboard');
        }
        else{
             return redirect()->route('login');
        }
        
    
    }
    
    
    
    //modification online
    
    public function reset_step_one() {

        return view('generals.reset-password');
    }
    
    
    
    public function reset_step_one_form(Request $request) {
        
        $request->validate([
            'email_phone' => 'required',
        ]);
        
        
        $compte = User::where('email', $request->email_phone)->first();
        
        if ($compte) {
            
            $code_conf = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 10);

            session([
                'pass_reset' => Hash::make($code_conf),
                'email' => $request->email_phone
            ]);

            Mail::to($request->email_phone)->queue(new ResetPassword($code_conf));
            
            return redirect()->route('reset_step_two')->withConfirmation('Un code de confirmation est envoyé à l\'adresse E-mail renseignée. Veuillez soumettre ce code pour pouvoir réinitialiser votre mot de passe.');
        }
        
        else{
            return redirect()->back()->withErrormail('Aucun compte ne correspond à cet adresse E-mail!!');
        }

    }
    
    
    public function reset_step_two() {

        return view('generals.reset-password-two');
    }
    
    
    
    public function reset_step_two_form(Request $request) {
        
        $request->validate([
            'code' => 'required',
        ]);
        
        if (Hash::check($request->code, session('pass_reset'))) {

            return redirect()->route('reset_step_final')->withOkay('Correct! Vous pouvez modifier votre mot de passe.');
        }
        
        else{
            return redirect()->back()->withErrormail('Code de confirmation incorrect. Veuillez contrôler vos emails ou vos spams!!');
        }

    }
    
    
    public function reset_step_final() {

        return view('generals.reset-password-final');
    }
    
    
    
    public function reset_step_final_form(Request $request) {
        
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
        
        
        User::where('email', session('email'))->update([
            'password' => Hash::make($request->password),
    
        ]);
        
        session::forget('email');
        session::forget('pass_reset');
        
        return redirect()->route('login')->withOkay('Mot de passe réinitialisé avec succès. Connectez-vous à présent!');
        
    }
}

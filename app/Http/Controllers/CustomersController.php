<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use Spatie\SimpleExcel\SimpleExcelWriter;
use Spatie\SimpleExcel\SimpleExcelReader;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Publicite;
use App\Models\Abonnement;
use App\Models\Publication;
use App\Models\Contact;
use App\Models\Message;
use App\Models\ContactFile;
use App\Models\Event;
use App\Models\Admin;
use App\Mail\Notification;
use Illuminate\Support\Facades\DB;
use App\Models\Commenter;
use App\Models\CommenterPublication;
use Illuminate\Support\Facades\Crypt;

class CustomersController extends Controller
{
    public function give_contact() {

        if ( Auth()->user()->date_next <= now()->toDateTimeString()) {

            $date_previous = Auth()->user()->date_next;
            $date_next = date("Y-m-d H:i:s", strtotime("+7 day", strtotime(Auth()->user()->date_next)));
            $file_name = "contacts".Auth()->user()->nom.'_'.$date_previous.'.'.'vcf';
            $number = Auth()->user()->number_of_contacts;
            if(!( ContactFile::where('filename', $file_name)->exists() ) ) {
                User::where('id', Auth()->user()->id)->update([
                    'updated_at' => Auth()->user()->date_next,
                    'date_next' => $date_next,
                    'number_of_contacts' => Auth()->user()->number_of_contacts + 25,
                ]);

                $contacts = DB::table('contacts')->skip($number)->take(25)->get();                    

                $file_name = Auth()->user()->nom.'_'.$date_previous.'.'.'vcf';

                $path = "uploads/contacts".''.$file_name;

                foreach($contacts as $contact){
                    file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                    file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                    file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                    file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                    file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                    file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                    file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                    file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                }

                $file = new ContactFile;
                $file->number_of_contacts = 25;
                $file->user_id = Auth()->user()->id;
                $file->filename = "contacts".''.$file_name;
                $file->save();
            }

        }
    }
    
    // fonction de vue de la page de connexion de 
    public function login(Request $request) {

        $request->validate([
            'email_phone' => 'required',
            'password' => 'required',
            'remember' => '',
        ]);


        if ( (Auth::attempt(['email' => $request->email_phone, 'password' => $request->password, 'is_customer' => 1 ], $remember = $request->remember) ) 
            or (Auth::attempt(['phone' => $request->email_phone, 'password' => $request->password, 'is_customer' => 1 ] , $remember = $request->remember) )) {

            $request->session()->regenerate();
            return redirect()->route('customers_dashboard');
        }
        else {

            return redirect()->back()->withError('Identifiants incorrects. Veuillez vérifier vos informations de connexion');

        }
        
    }


    public function join(Request $request) {

        $request->validate([
            'nom' => 'required|min:3|max:255',
            'prenom' => 'required|min:3|max:255',
            'phone' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'adresse' => 'required',
            'indicatif' => 'required|max:4|min:1',
            'profession' => 'required',
            'wanted' => '',
            'read' => '',
            'description' => '',
            'password' => 'required|min:6|max:20',
            'confirm_password' => 'required|min:6|max:20|same:password',
        ]);

        if ( preg_match('/(\+)((\d{1})|(\d{2})|(\d{3})|(\d{4}))/i', $request->indicatif) ){
            return redirect()->back()->withError('Indicatif incorrect');
        }

        $user = new User;
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->adresse = $request->adresse;
        $user->phone = "+".$request->indicatif.''.$request->phone;
        $user->email = $request->email;
        $user->profession = $request->profession;
        $user->password = Hash::make($request->password);
        if ($request->wanted == "oui"){
            $user->wanted_to_join_ccs == True;
        }
        else {
            $user->wanted_to_join_ccs == False;
        }

        if ($request->read == "oui"){
            $user->has_read_goal_of_ccs == True;
        }
        else {
            $user->has_read_goal_of_ccs == False;
        }

        $user->date_next = date("Y-m-d H:i:s", strtotime("+7 day", strtotime(now()->toDateTimeString())));

        $user->description_of_you = $request->description;
        $user->is_customer = True;
        $user->is_valid = True;
        $user->save();

        $cont = new Contact;
        $cont->fullname = $user->nom.' '.$user->prenom;
        $cont->phone = $user->phone;
        $cont->created_by = "inscription";
        $cont->save();

        $file_name = $user->nom.'_'.'contacts'.time().'.'.'vcf';

        $path = "uploads/contacts".''.$file_name;

        $number = 25;

        User::where('id', $user->id)->update([
            'number_of_contacts' => $number,
        ]);
        
        $contacts = DB::table('contacts')->take(25)->get();
        
        foreach($contacts as $contact){
            file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
            file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
            file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
            file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
            file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
            file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
            file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
            file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
        }

        $file = new ContactFile;
        $file->number_of_contacts = $number ;
        $file->user_id = $user->id;
        $file->filename = "contacts".''.$file_name;
        $file->save();

        
        

        return redirect()->route('join_success');
            
    }
    

    // fonction de vue de la page de connexion de 
    public function join_success() {

        return view('generals.join_success');
    }



    public function dashboard() {

        if ( Auth::check() and Auth()->user()->is_customer == True) {

            if ( Auth()->user()->date_end == now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'mode' => "gratuit",
                    'date_start' => null,
                    'date_end' => null,
                ]);

            }
            
            
            $this->give_contact();

            Publicite::where('date_end', '<', now()->toDateTimeString())
                ->update(['is_active' => 0,
            ]);
    
            $publications = Publication::where('is_active', 1)
                                        ->where('date_end', '>=', now()->toDateTimeString())
                                        ->orderBy('ordre', 'desc')
                                        ->get();

            $publicites = Publicite::where('is_active', 1)
                                    ->where('date_end', '>=', now()->toDateTimeString())
                                    ->orderBy('ordre', 'desc')
                                    ->get();
                                    
            $users = User::all();
            $admin = Admin::where('username', "admins")->first();
            
            return view('dashboard.customers.welcome', [
                'publicites' => $publicites,
                'publications' => $publications,
                'users' => $users,
                'admin' => $admin,
            ]);
        }
        else {
            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }

    public function dashboard2($id) {
        //dd(Crypt::decryptString($id));
        if ( Auth::check() and Auth()->user()->is_customer == True) {

            if ( Auth()->user()->date_end == now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'mode' => "gratuit",
                    'date_start' => null,
                    'date_end' => null,
                ]);

            }
            
            
            // $this->give_contact();

            // Publicite::where('date_end', '<', now()->toDateTimeString())
            //     ->update(['is_active' => 0,
            // ]);
    
            $publications = Publication::where('is_active', 1)
                                        ->where('date_end', '>=', now()->toDateTimeString())
                                        ->where('id', '=',$id)
                                        ->orderBy('ordre', 'desc')
                                        ->get();

            $publicites = Publicite::where('is_active', 1)
                                    ->where('date_end', '>=', now()->toDateTimeString())
                                    ->where('id', '=',$id)
                                    ->orderBy('ordre', 'desc')
                                    ->get();
                                    
            $users = User::all();
            $admin = Admin::where('username', "admins")->first();
            
            return view('dashboard.customers.publie_network', [
                'publicites' => $publicites,
                'publications' => $publications,
                'users' => $users,
               'admin' => $admin,
            ]);
        }
        else {
            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    


    public function logout(Request $request) {
    
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('welcome')->withDeconnexion('Vous vous êtes déconnecté avec succès. Nous serons heureux de vous revoir bientôt. Merci.');
    }


    public function active_pack() {

        if ( Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            if (session('type')) {
                session::forget('type');
            }

            return view('dashboard.customers.pack');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }

    public function active_pack_specific($id) {

        if ( Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            if ($id == 1) {
                session([
                    'type' => "basic",
                ]);
                $prix = 5000;
                return view('dashboard.customers.pack', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 2) {
                session([
                    'type' => "standard",
                ]);
                $prix = 10000;
                return view('dashboard.customers.pack', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 3) {
                session([
                    'type' => "premium",
                ]);
                $prix = 15000;
                return view('dashboard.customers.pack', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 4) {
                session([
                    'type' => "pro",
                ]);
                $prix = 25000;
                return view('dashboard.customers.pack', [
                    'prix' => $prix,
                ]);
            }
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuiilez vous connecter d\'abord!!!');

        }
    }





    public function transaction() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $public_key = $_ENV["KKIAPAY_PUBLIC_KEY"];
            $private_key = $_ENV["KKIAPAY_PRIVATE_KEY"];
            $secret = $_ENV["KKIAPAY_SECRET_KEY"];

            $kkiapay = new \Kkiapay\Kkiapay($public_key,
                                            $private_key, 
                                            $secret, );                              
            $transaction_id = $_GET['transaction_id'];
            $transaction = $kkiapay->verifyTransaction($transaction_id);

            if ($transaction->status == 'SUCCESS') {

                $new_transaction = new Transaction;

                $new_transaction->type_of_pack = session('type');
                $new_transaction->statut = "success";
                $new_transaction->montant_paye = $transaction->amount;
                $new_transaction->moyen_de_paiement = $transaction->source;
                $new_transaction->transaction_id = $transaction->transactionId;
                $new_transaction->pays = $transaction->country;
                $new_transaction->username = Auth()->user()->email;
                $new_transaction->save();

                if (session('type') == "basic") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+3 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['mode' => "basic",
                        'date_start' => $date_start,
                        'date_end' => $date_end,
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 2000,
                       
                    ]);
                    $contacts = DB::table('contacts')->skip($number)->take(2000)->get();
                                      

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 2000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();

                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                }

                elseif (session('type') == "standard") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+5 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['mode' => "standard",
                        'date_start' => $date_start,
                        'date_end' => $date_end,
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 3000,
                       
                    ]);

                    $contacts = DB::table('contacts')->skip($number)->take(3000)->get();                    

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 3000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();

                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                }


                elseif (session('type') == "premium") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+7 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['mode' => "premium",
                        'date_start' => $date_start,
                        'date_end' => $date_end,
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 3500,
                       
                    ]);

                    $contacts = DB::table('contacts')->skip($number)->take(3500)->get();                    

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 3500;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();


                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                }
                elseif (session('type') == "pro") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+10 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['mode' => "pro",
                        'date_start' => $date_start,
                        'date_end' => $date_end,
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 4000,
                       
                    ]);

                    $contacts = DB::table('contacts')->skip($number)->take(4000)->get();                     

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 4000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();


                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                }

                session::forget('type');
                
                return redirect()->route('customers_dashboard')->withAbonnementsuccess('Vous avez activé avec succès ce pack. Nous vous souhaitons une très belle aventure. Merci pour la confiance!!');

            }
        }

        else {

            return redirect()->route('login');

        }

    }


    public function profil() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            return view('dashboard.customers.profil');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function contact() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $contacts = ContactFile::where('user_id', Auth()->user()->id)->get();

            return view('dashboard.customers.contact', [
                'contacts' => $contacts,
            ]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function view_of_abonns() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $abonns = Abonnement::where('user_id', Auth()->user()->id)->get();
            return view('dashboard.customers.my_abonns', [
                'abonns' => $abonns,
            ]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function download_contacts($id) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            $url = ContactFile::find($id);

            return response()->download(public_path('uploads'.'/'.$url->filename));
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function inbox() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            Message::where('date_end', '<', now()->toDateTimeString())
                ->update(['is_active' => 0,
            ]);

            $messages = Message::where('is_active', 1)
            ->where('date_end', '>=', now()->toDateTimeString())
            ->get();

            //GET COMMENTS DATA

            $comments = Commenter::join('publicites','publicites.id','=','commenters.pub_id')
                                    ->join('users','users.id','=','commenters.user_id')
                                    ->where('publicites.from_user',Auth()->user()->email)
                                    ->select('publicites.title','users.phone','commenters.commenter','commenters.created_at')
                                    ->get();

            $commentsPub = CommenterPublication::join('publications','publications.id','=','commenter_publications.pub_id')
                                    ->join('users','users.id','=','commenter_publications.user_id')
                                    ->where('publications.from_user',Auth()->user()->email)
                                    ->select('publications.title','users.phone','commenter_publications.commenter','commenter_publications.created_at')
                                    ->get();
           
            return view('dashboard.customers.inbox', [
                'messages' => $messages,
                'comments' => $comments,
                'commentsPub' => $commentsPub,
            ]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }


    public function events() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            Event::where('date_end', '<', now()->toDateTimeString())
                ->update(['is_active' => 0,
            ]);

            $events = Event::where('is_active', 1)
            ->where('date_end', '>=', now()->toDateTimeString())
            ->get();
            return view('dashboard.customers.events', [
                'events' => $events,
            ]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function video() {

        if (Auth::check() and Auth()->user()->is_customer == True) {

         $this->give_contact();
            

            $publications= Publication::where('is_active',0 )
                                        ->where('date_end', '>=', now()->toDateTimeString())
                                        ->where('description',  "video")
                                        ->orderBy('ordre', 'desc')
                                        ->paginate(3);

            return view('dashboard.customers.watch_video', [
                'publications' => $publications,
            ]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    public function modify_profil() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            return view('dashboard.customers.modify_profil');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function modify_profil_form(Request $request) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $request->validate([
                'nom' => '',
                'prenom' => '',
                'phone' => 'unique:users',
                'email' => 'unique:users',
                'adresse' => '',
                'profession' => '',
                'password' => '',
                'confirm_password' => 'same:password',
            ]);
        
            if($request->nom != ""){
                $nom = $request->nom;
            }
            else{
                $nom = Auth()->user()->nom;
            }
            
            if($request->prenom != ""){
                $prenom = $request->prenom;
            }
            else{
                $prenom = Auth()->user()->prenom;
            }
            
            if($request->phone != ""){
                $phone = $request->phone;
            }
            else{
                $phone = Auth()->user()->phone;
            }
            
            
            if($request->email != ""){
                $email = $request->email;
            }
            else{
                $email = Auth()->user()->email;
            }
            
            if($request->adresse != ""){
                $adresse = $request->adresse;
            }
            else{
                $adresse = Auth()->user()->adresse;
            }
            
            if($request->profession != ""){
                $profession = $request->profession;
            }
            else{
                $profession = Auth()->user()->profession;
            }
            
            if($request->password != ""){
                $password = Hash::make($request->password);
            }
            else{
                $password = Auth()->user()->password;
            }
            
            User::where('id', Auth()->user()->id)->update([
                'nom' => $nom,
                'prenom' => $prenom,
                'phone' => $phone,
                'email' => $email,
                'adresse' => $adresse,
                'profession' => $profession,
                'password' => $password,
               
            ]);
            
            return redirect()->route('customers_modify_profil')->withSuccess('Vos informations ont été modifié avec succès');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    
    // modification online
    
    public function activate_module() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            return view('dashboard.customers.module');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function activate_module_specific($id) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            
            if ($id == 1) {
                session([
                    'type' => "promotion",
                ]);
                $prix = 7000;
                return view('dashboard.customers.module_specific', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 2) {
                session([
                    'type' => "communication",
                ]);
                $prix = 12000;
                return view('dashboard.customers.module_specific', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 3) {
                session([
                    'type' => "communicationplus",
                ]);
                $prix = 25000;
                return view('dashboard.customers.module_specific', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 4) {
                session([
                    'type' => "entreprise",
                ]);
                $prix = 80000;
                return view('dashboard.customers.module_specific', [
                    'prix' => $prix,
                ]);
            }
            
            
            elseif ($id == 5) {
                session([
                    'type' => "partenariat",
                ]);
                $prix = 110000;
                return view('dashboard.customers.module_specific', [
                    'prix' => $prix,
                ]);
            }
            
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    
    
    public function activate_module_transaction() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $public_key = $_ENV["KKIAPAY_PUBLIC_KEY"];
            $private_key = $_ENV["KKIAPAY_PRIVATE_KEY"];
            $secret = $_ENV["KKIAPAY_SECRET_KEY"];

            $kkiapay = new \Kkiapay\Kkiapay($public_key,
                                            $private_key, 
                                            $secret, );                              
            $transaction_id = $_GET['transaction_id'];
            $transaction = $kkiapay->verifyTransaction($transaction_id);

            if ($transaction->status == 'SUCCESS') {

                $new_transaction = new Transaction;

                $new_transaction->type_of_pack = session('type');
                $new_transaction->statut = "success";
                $new_transaction->montant_paye = $transaction->amount;
                $new_transaction->moyen_de_paiement = $transaction->source;
                $new_transaction->transaction_id = $transaction->transactionId;
                $new_transaction->pays = $transaction->country;
                $new_transaction->username = Auth()->user()->email;
                $new_transaction->save();

                if (session('type') == "promotion") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+5 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['has_module' => "promotion",
                        'date_module_start' => $date_start,
                        'date_module_end' => $date_end,
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'module_days' => 5,
                        'has_active_publication' => "yes",
                       
                    ]);
                    
                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                    
                    Mail::to("groupeducommercesocial@gmail.com")->queue(new Notification($module = session('type'), $user));
                }

                elseif (session('type') == "communication") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+10 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['has_module' => "communication",
                        'date_module_start' => $date_start,
                        'date_module_end' => $date_end,
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'module_days' => 10,
                        'has_active_publication' => "yes",
                       
                    ]);

                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                    
                    Mail::to("groupeducommercesocial@gmail.com")->queue(new Notification($module = session('type'), $user));
                }


                elseif (session('type') == "communicationplus") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+15 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['has_module' => "communication+",
                        'date_module_start' => $date_start,
                        'date_module_end' => $date_end,
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'module_days' => 15,
                        'has_active_publication' => "yes",
                       
                    ]);

                    
                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                    
                    Mail::to("groupeducommercesocial@gmail.com")->queue(new Notification($module = session('type'), $user));
                }
                elseif (session('type') == "entreprise") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+31 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['has_module' => "entreprise",
                        'date_module_start' => $date_start,
                        'date_module_end' => $date_end,
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'module_days' => 31,
                        'has_active_publication' => "yes",
                       
                    ]);

                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                    
                    Mail::to("groupeducommercesocial@gmail.com")->queue(new Notification($module = session('type'), $user));
                }
                
                
                
                elseif (session('type') == "partenariat") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+90 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update(['has_module' => "partenariat",
                        'date_module_start' => $date_start,
                        'date_module_end' => $date_end,
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'module_days' => 90,
                        'has_active_publication' => "yes",
                       
                    ]);

                    $new_transaction = new Abonnement;

                    $new_transaction->type = session('type');
                    $new_transaction->date_start = $date_start;
                    $new_transaction->date_end = $date_end;
                    $new_transaction->user_id = Auth()->user()->id;
                    $new_transaction->save();
                    
                    Mail::to("groupeducommercesocial@gmail.com")->queue(new Notification($module = session('type'), $user));
                }

                session::forget('type');
                
                return redirect()->route('customers_dashboard')->withAbonnementsuccess('Vous avez activé avec succès un module. Nous vous souhaitons une très belle aventure. Merci pour la confiance!!');

            }
        }

        else {

            return redirect()->route('login');

        }

    }
    
    
    
    public function do_publication() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            if ( Auth()->user()->date_module_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_module' => null,
                    'date_module_start' => null,
                    'date_module_end' => null,
                    'module_days' => 0,
                ]);

            }
            
            
            if ( Auth()->user()->date_pub_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_active_publication' => "no",
                    'date_pub_start' => null,
                    'date_pub_end' => null,
                ]);

            }
            
            $interval = (strtotime(auth()->user()->date_pub_end) - strtotime(now()->toDateTimeString()) );
            
            $maxi = intdiv($interval, 86400);
            
            return view('dashboard.customers.do-publication', ['maxi' => $maxi]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function do_affiche() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            if ( Auth()->user()->date_module_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_module' => null,
                    'date_module_start' => null,
                    'date_module_end' => null,
                    'module_days' => 0,
                ]);

            }
            
            
            if ( Auth()->user()->date_pub_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_active_publication' => "no",
                    'date_pub_start' => null,
                    'date_pub_end' => null,
                ]);

            }
            
            $interval = (strtotime(auth()->user()->date_pub_end) - strtotime(now()->toDateTimeString()) );
            
            $maxi = intdiv($interval, 86400);
            
            return view('dashboard.customers.do-affiche',['maxi' => $maxi]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    
    public function do_publication_form(Request $request) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            if ( Auth()->user()->date_module_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_module' => null,
                    'date_module_start' => null,
                    'date_module_end' => null,
                    'module_days' => 0,
                ]);

            }
            
            
            if ( Auth()->user()->date_pub_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_active_publication' => "no",
                    'date_pub_start' => null,
                    'date_pub_end' => null,
                ]);

            }
            $interval = (strtotime(auth()->user()->date_pub_end) - strtotime(now()->toDateTimeString()) );
            
            $maxi = intdiv($interval, 86400);
            
            $request->validate([
                'nom' => 'required', 
                'description' => '',
                'duree' => 'required',
                'image' => '',
            ]);

            $file_image = $request->file('image');
            if ($file_image !== null) {
                $filename =$file_image->getClientOriginalName();
                $path = 'uploads/images';
                $file_image->move($path, $filename);

                $pub = new Publication;
                $pub->title = $request->nom;
                $pub->description = $request->description;
                $pub->duree = $request->duree;
                $date_start = now()->toDateTimeString();
                $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
                $pub->date_end = $date_end;
                $pub->is_active = True;
                $pub->ordre = 1;
                $pub->from_user = auth()->user()->email;
                $pub->image = $filename;
                $pub->save();
            }
            else {
                $pub = new Publicite;
                $pub->title = $request->nom;
                $pub->contenu = $request->description;
                $pub->duree = $request->duree;
                $date_start = now()->toDateTimeString();
                $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
                $pub->date_end = $date_end;
                $pub->is_active = True;
                $pub->ordre = 1;
                $pub->from_user = auth()->user()->email;
                $pub->save();
            }
            
            return redirect()->route('customers_do_publication')->withPublication('Votre affiche ou annonce a été publié avec succès!! Merci');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    
    
    public function post_video() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            if ( Auth()->user()->date_module_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_module' => null,
                    'date_module_start' => null,
                    'date_module_end' => null,
                    'module_days' => 0,
                ]);

            }
            
            
            if ( Auth()->user()->date_pub_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_active_publication' => "no",
                    'date_pub_start' => null,
                    'date_pub_end' => null,
                ]);

            }
            
            $interval = (strtotime(auth()->user()->date_pub_end) - strtotime(now()->toDateTimeString()) );
            $maxi = intdiv($interval, 86400);
            return view('dashboard.customers.post-video', ['maxi' => $maxi]);
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function post_video_form(Request $request) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            
            if ( Auth()->user()->date_module_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_module' => null,
                    'date_module_start' => null,
                    'date_module_end' => null,
                    'module_days' => 0,
                ]);

            }
            
            
            if ( Auth()->user()->date_pub_end <= now()->toDateTimeString()) {
                
                User::where('id', Auth()->user()->id)->update([
                    'has_active_publication' => "no",
                    'date_pub_start' => null,
                    'date_pub_end' => null,
                ]);

            }
            
            $request->validate([
                'nom' => 'required', 
                'description' => '',
                'duree' => 'required',
                'video' => '',
            ]);

            $pub = new Publication;
            $pub->title = $request->nom;
            $pub->is_active = False;
            $pub->from_user = auth()->user()->email;
            $pub->description = "video";
            $pub->duree = $request->duree;
            $date_start = now()->toDateTimeString();
            $date_end = date("Y-m-d H:i:s", strtotime("+{$request->duree} day", strtotime($date_start)));
            $pub->date_end = $date_end;
            

            $file_video = $request->file('video');
            if ($file_video !== null) {
                $filename =$file_video->getClientOriginalName();
                $path = 'uploads/videos';
                $file_video->move($path, $filename);
                $pub->video = $filename; 
                
            }
            $pub->save();
            
            return redirect()->route('customers_post_video')->withPublication('Votre affiche ou annonce ou video a été publié avec succès!! Merci');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function activate_service() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            return view('dashboard.customers.service');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function activate_service_publication() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            return view('dashboard.customers.service_publication');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function activate_service_publication_option($id) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            
            
            if ($id == 1) {
                session([
                    'option' => "5days",
                ]);
                $prix = 500;
                return view('dashboard.customers.service_publication_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 2) {
                session([
                    'option' => "10days",
                ]);
                $prix = 800;
                return view('dashboard.customers.service_publication_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 3) {
                session([
                    'option' => "15days",
                ]);
                $prix = 1350;
                return view('dashboard.customers.service_publication_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 4) {
                session([
                    'option' => "1month",
                ]);
                $prix = 2500;
                return view('dashboard.customers.service_publication_option', [
                    'prix' => $prix,
                ]);
            }
            
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    
    public function activate_service_publication_transaction() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $public_key = $_ENV["KKIAPAY_PUBLIC_KEY"];
            $private_key = $_ENV["KKIAPAY_PRIVATE_KEY"];
            $secret = $_ENV["KKIAPAY_SECRET_KEY"];

            $kkiapay = new \Kkiapay\Kkiapay($public_key,
                                            $private_key, 
                                            $secret, );                              
            $transaction_id = $_GET['transaction_id'];
            $transaction = $kkiapay->verifyTransaction($transaction_id);

            if ($transaction->status == 'SUCCESS') {

                $new_transaction = new Transaction;

                $new_transaction->type_of_pack = session('option');
                $new_transaction->statut = "success";
                $new_transaction->montant_paye = $transaction->amount;
                $new_transaction->moyen_de_paiement = $transaction->source;
                $new_transaction->transaction_id = $transaction->transactionId;
                $new_transaction->pays = $transaction->country;
                $new_transaction->username = Auth()->user()->email;
                $new_transaction->save();

                if (session('option') == "5days") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+5 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update([
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'has_active_publication' => "yes",
                       
                    ]);
                    
                }

                elseif (session('option') == "10days") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+10 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update([
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'has_active_publication' => "yes",
                       
                    ]);

                    
                }


                elseif (session('option') == "15days") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+15 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update([
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'has_active_publication' => "yes",
                       
                    ]);

                    
                }
                elseif (session('option') == "1month") {

                    $user = User::find(Auth()->user()->id);
                    $date_start = now()->toDateTimeString();
                    $date_end = date("Y-m-d H:i:s", strtotime("+31 day", strtotime($date_start)));
                    User::where('id', Auth()->user()->id)->update([
                        'date_pub_start' => $date_start,
                        'date_pub_end' => $date_end,
                        'has_active_publication' => "yes",
                       
                    ]);

                    
                }

                session::forget('option');
                
                return redirect()->route('customers_dashboard')->withAbonnementsuccess('Vous avez activé avec succès ce service. Nous vous souhaitons une très belle aventure. Merci pour la confiance!!');

            }
        }

        else {

            return redirect()->route('login');

        }

    }
    
    
    
    public function telecharger_contacts() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            return view('dashboard.customers.download_contacts');
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function telecharger_contacts_option($id) {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            
            if ($id == 1) {
                session([
                    'option' => "2000",
                ]);
                $prix = 2000;
                return view('dashboard.customers.download_contacts_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 2) {
                session([
                    'option' => "3000",
                ]);
                $prix = 3000;
                return view('dashboard.customers.download_contacts_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 3) {
                session([
                    'option' => "3500",
                ]);
                $prix = 3500;
                return view('dashboard.customers.download_contacts_option', [
                    'prix' => $prix,
                ]);
            }
            elseif ($id == 4) {
                session([
                    'option' => "4000",
                ]);
                $prix = 4000;
                return view('dashboard.customers.download_contacts_option', [
                    'prix' => $prix,
                ]);
            }
            
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    
    public function transaction_download_contacts() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $public_key = $_ENV["KKIAPAY_PUBLIC_KEY"];
            $private_key = $_ENV["KKIAPAY_PRIVATE_KEY"];
            $secret = $_ENV["KKIAPAY_SECRET_KEY"];

            $kkiapay = new \Kkiapay\Kkiapay($public_key,
                                            $private_key, 
                                            $secret, );                              
            $transaction_id = $_GET['transaction_id'];
            $transaction = $kkiapay->verifyTransaction($transaction_id);

            if ($transaction->status == 'SUCCESS') {

                $new_transaction = new Transaction;

                $new_transaction->type_of_pack = session('option');
                $new_transaction->statut = "success";
                $new_transaction->montant_paye = $transaction->amount;
                $new_transaction->moyen_de_paiement = $transaction->source;
                $new_transaction->transaction_id = $transaction->transactionId;
                $new_transaction->pays = $transaction->country;
                $new_transaction->username = Auth()->user()->email;
                $new_transaction->save();

                if (session('option') == "2000") {
                    
                    
                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    User::where('id', Auth()->user()->id)->update([
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 2000,
                       
                    ]);
                    $contacts = DB::table('contacts')->take(2000)->get();
                                      

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 2000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();
                    
                    
                    $url = ContactFile::find($file->id);

                    return response()->download(public_path('uploads'.'/'.$url->filename));
                    
                }

                elseif (session('option') == "3000") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    User::where('id', Auth()->user()->id)->update([
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 3000,
                       
                    ]);
                    $contacts = DB::table('contacts')->take(3000)->get();
                                      

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 3000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();
                    
                    
                    $url = ContactFile::find($file->id);

                    return response()->download(public_path('uploads'.'/'.$url->filename));

                    
                }


                elseif (session('option') == "3500") {

                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    User::where('id', Auth()->user()->id)->update([
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 3500,
                       
                    ]);
                    $contacts = DB::table('contacts')->take(3500)->get();
                                      

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 3500;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();
                    
                    
                    $url = ContactFile::find($file->id);

                    return response()->download(public_path('uploads'.'/'.$url->filename));

                    
                }
                elseif (session('option') == "4000") {
                    
                    $number = Auth()->user()->number_of_contacts;
                    $user = User::find(Auth()->user()->id);
                    User::where('id', Auth()->user()->id)->update([
                        'number_of_contacts' => Auth()->user()->number_of_contacts + 4000,
                       
                    ]);
                    $contacts = DB::table('contacts')->take(4000)->get();
                                      

                    $file_name = Auth()->user()->nom.'_'.'contacts'.time().'.'.'vcf';

                    $path = "uploads/contacts".''.$file_name;

                    foreach($contacts as $contact){
                        file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                        file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                        file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                        file_put_contents($path, "\nN;CHARSET=utf-8:CCS+ {$contact->fullname};;;;", FILE_APPEND);
                        file_put_contents($path, "\nFN;CHARSET=utf-8:CCS+ {$contact->fullname}", FILE_APPEND);
                        file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                        file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                        file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                    }

                    $file = new ContactFile;
                    $file->number_of_contacts = 4000;
                    $file->user_id = Auth()->user()->id;
                    $file->filename = "contacts".''.$file_name;
                    $file->save();
                    
                    
                    $url = ContactFile::find($file->id);

                    return response()->download(public_path('uploads'.'/'.$url->filename));

                    
                }

                session::forget('option');
                
                return redirect()->route('customers_dashboard')->withAbonnementsuccess('Contacts téléchargés avec succès.. Nous vous souhaitons une très belle aventure. Merci pour la confiance!!');

            }
        }

        else {

            return redirect()->route('login');

        }

    }
    
    
    
    public function download_contacts_users() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            
            if (Auth()->user()->has_download == "yes"){
                $users = User::all();
                
                $file_name = '_users'.time().'.'.'vcf';
    
                $path = "uploads/contacts".''.$file_name;
                
                foreach($users as $contact){
                    file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                    file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                    file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                    file_put_contents($path, "\nN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom};;;;", FILE_APPEND);
                    file_put_contents($path, "\nFN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom}", FILE_APPEND);
                    file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                    file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                    file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                }
    
                return response()->download(public_path('uploads'.'/'.'contacts'.''.$file_name));
            }
            else{
            
                return view('dashboard.customers.users_contacts');
            }
            
            
        }
        else {

            return redirect()->route('login')->withNoenter('Vous n\'êtes pas autorisé à accéder à cette page. Veuillez vous connecter d\'abord!!!');

        }
    }
    
    
    public function paiement_users_contacts_transaction() {

        if (Auth::check() and Auth()->user()->is_customer == True) {
            $this->give_contact();
            $public_key = $_ENV["KKIAPAY_PUBLIC_KEY"];
            $private_key = $_ENV["KKIAPAY_PRIVATE_KEY"];
            $secret = $_ENV["KKIAPAY_SECRET_KEY"];

            $kkiapay = new \Kkiapay\Kkiapay($public_key,
                                            $private_key, 
                                            $secret, );                              
            $transaction_id = $_GET['transaction_id'];
            $transaction = $kkiapay->verifyTransaction($transaction_id);

            if ($transaction->status == 'SUCCESS') {

                $new_transaction = new Transaction;

                $new_transaction->type_of_pack = "contacts_users_downloads";
                $new_transaction->statut = "success";
                $new_transaction->montant_paye = $transaction->amount;
                $new_transaction->moyen_de_paiement = $transaction->source;
                $new_transaction->transaction_id = $transaction->transactionId;
                $new_transaction->pays = $transaction->country;
                $new_transaction->username = Auth()->user()->email;
                $new_transaction->save();

                    
                
                $user = User::find(Auth()->user()->id);
                User::where('id', Auth()->user()->id)->update([
                    'has_download' => "yes",
                   
                ]);
                $users = User::all();
                
                $file_name = '_users'.time().'.'.'vcf';
    
                $path = "uploads/contacts".''.$file_name;
                
                foreach($users as $contact){
                    file_put_contents($path, "\nBEGIN:VCARD", FILE_APPEND);
                    file_put_contents($path, "\nVERSION:3.0", FILE_APPEND);
                    file_put_contents($path, "\nREV:2022-02-28T17:50:22Z", FILE_APPEND);
                    file_put_contents($path, "\nN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom};;;;", FILE_APPEND);
                    file_put_contents($path, "\nFN;CHARSET=utf-8:CCS Contact+ {$contact->nom} {$contact->prenom}", FILE_APPEND);
                    file_put_contents($path, "\nTEL:{$contact->phone}", FILE_APPEND);
                    file_put_contents($path, "\nADR;WORK;POSTAL;CHARSET=utf-8:;;Bénin;;;;", FILE_APPEND);
                    file_put_contents($path, "\nEND:VCARD", FILE_APPEND);
                }
    
                return response()->download(public_path('uploads'.'/'.'contacts'.''.$file_name));

            }
        }

        else {

            return redirect()->route('login');

        }

    }
}

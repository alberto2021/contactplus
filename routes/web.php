<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralsController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\AdminsController;
use App\Http\Controllers\AimerController;
use App\Http\Controllers\CommenterController;
use App\Http\Controllers\EmailController;
use Illuminate\Support\Facades\Crypt;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/email', [EmailController::class, 'index'])->name('email');
Route::post('/email/send', [EmailController::class, 'send'])->name('email/send');

/*  Route pour la page d'accueil du site Web  */
Route::get('/', [GeneralsController::class, 'welcome'])
    ->name('welcome');

/*  Route pour activer un pack  */
Route::get('/activate_pack', [GeneralsController::class, 'active_pack'])
    ->name('active_pack');


/*  Route pour la page de connection des clients  */
Route::get('/login', [GeneralsController::class, 'login_view'])
    ->name('login');

/*  Route pour la page de connection des clients  */
Route::post('/login', [CustomersController::class, 'login'])
    ->name('login_form');


/*  Route pour la page de inscription des clients  */
Route::get('/join', [GeneralsController::class, 'join_view'])
    ->name('join');

/*  Route pour la page de inscription des clients  */
Route::post('/join', [CustomersController::class, 'join'])
    ->name('join_form');

/*  Route pour la page de inscription des clients  */
Route::get('/join/success', [CustomersController::class, 'join_success'])
    ->name('join_success');

Route::get('/watch_video', [GeneralsController::class, 'watch_video'])
    ->name('watch_video');
    
    
Route::get('/reset-password/step_one', [GeneralsController::class, 'reset_step_one'])
    ->name('reset_step_one');
    
    
Route::post('/reset-password/step_one', [GeneralsController::class, 'reset_step_one_form'])
    ->name('reset_step_one_form');
    
Route::get('/reset-password/step_two', [GeneralsController::class, 'reset_step_two'])
    ->name('reset_step_two');
    
Route::post('/reset-password/step_two', [GeneralsController::class, 'reset_step_two_form'])
    ->name('reset_step_two_form');
    
    
    
Route::get('/reset-password/step_final', [GeneralsController::class, 'reset_step_final'])
    ->name('reset_step_final');
    
Route::post('/reset-password/step_final', [GeneralsController::class, 'reset_step_final_form'])
    ->name('reset_step_final_form');
    


    #---------AIMER UNE PUBLICITE-----#
    Route::post('/aimer', [AimerController::class, 'insert_like'])->name('LikePub');
    Route::post('/commenter', [CommenterController::class, 'insert_commenter'])->name('addCommentaire');
#---------AIMER UNE PUBLICATION-----#
Route::post('/aimer-pub', [AimerController::class, 'insert_like_publication'])->name('LikePublication');
Route::post('/commenter-pub', [CommenterController::class, 'insert_commenter_publication'])->name('addCommentairePub');


    /* Routage pour le proprietaire */
Route::prefix('customers')->group(function () {

    Route::get('/logout', [CustomersController::class, 'logout'])
        ->name('customers_logout');

       

    Route::middleware('auth')->group(function () {

        /*  Route pour la page de inscription des clients  */
        Route::get('/dashboard', [CustomersController::class, 'dashboard'])
            ->name('customers_dashboard')
            ;

        Route::get('publie/{id}', [CustomersController::class, 'dashboard2'])
                ->name('customers_dashboard2')
                ;


       

        // Route::get('/{locale}/posts', function () {
        //         //
        //     })->name('post.index');

        Route::get('/activate_pack', [CustomersController::class, 'active_pack'])
            ->name('customers_active_pack')
            ;


        Route::get('/activate_pack/{id}', [CustomersController::class, 'active_pack_specific'])
            ->name('customers_active_pack_specific')
            ;

        Route::get('/transaction_to_active_pack', [CustomersController::class, 'transaction'])
            ->name('customers_transaction')
            ;

        Route::get('/my_profile', [CustomersController::class, 'profil'])
            ->name('customers_profil')
            ;

        Route::get('/my_contacts', [CustomersController::class, 'contact'])
            ->name('customers_contact')
            ;

        Route::get('/my_abonnements', [CustomersController::class, 'view_of_abonns'])
            ->name('customers_abonns')
            ;

        Route::get('/contacts/download/{id}', [CustomersController::class, 'download_contacts'])
            ->name('customers_download_contacts')
            ;

        Route::get('/inbox', [CustomersController::class, 'inbox'])
            ->name('customers_inbox')
            ;

        Route::get('/events', [CustomersController::class, 'events'])
            ->name('customers_events')
            ;
            
        Route::get('/video', [CustomersController::class, 'video'])
            ->name('customers_video')
            ;
            
        Route::get('/profil/modify', [CustomersController::class, 'modify_profil'])
            ->name('customers_modify_profil')
            ;
            
            
        Route::post('/profil/modify', [CustomersController::class, 'modify_profil_form'])
            ->name('customers_modify_profil_form')
            ;
            
            
            
        Route::get('/activate-module', [CustomersController::class, 'activate_module'])
            ->name('customers_activate_module')
            ;
            
            
            
        Route::get('/activate-module/{id}', [CustomersController::class, 'activate_module_specific'])
            ->name('customers_activate_module_specific')
            ;
            
        
        
        Route::get('/transaction_paiement_module', [CustomersController::class, 'activate_module_transaction'])
            ->name('customers_activate_module_transaction')
            ;
            
            
        Route::get('/do-publication', [CustomersController::class, 'do_publication'])
            ->name('customers_do_publication');
            
        Route::post('/do-publication', [CustomersController::class, 'do_publication_form'])
            ->name('customers_do_publication_form');
            
        
        Route::get('/do-affiche', [CustomersController::class, 'do_affiche'])
            ->name('customers_do_affiche');
            
        Route::post('/do-affiche', [CustomersController::class, 'do_affiche_form'])
            ->name('customers_do_affiche_form');
            
            
            
        Route::get('/post-video', [CustomersController::class, 'post_video'])
            ->name('customers_post_video');
            
        Route::post('/post-video', [CustomersController::class, 'post_video_form'])
            ->name('customers_post_video_form');
            
            
        
        Route::get('/activate-service', [CustomersController::class, 'activate_service'])
            ->name('customers_activate_service');
            
            
        Route::get('/activate-service-publication', [CustomersController::class, 'activate_service_publication'])
            ->name('customers_activate_service_publication');
            
            
        Route::get('/activate-service-publication/{id}', [CustomersController::class, 'activate_service_publication_option'])
            ->name('customers_activate_service_publication_option');
            
            
        Route::get('/transaction_paiement_service_publication', [CustomersController::class, 'activate_service_publication_transaction'])
            ->name('customers_activate_service_publication_transaction');
            
        Route::get('/telecharger_contacts', [CustomersController::class, 'telecharger_contacts'])
            ->name('customers_telecharger_contacts');
            
            
        Route::get('/telecharger_contacts/{id}', [CustomersController::class, 'telecharger_contacts_option'])
            ->name('customers_telecharger_contacts_option');
            
        Route::get('/transaction_paiement_service_contacts', [CustomersController::class, 'transaction_download_contacts'])
            ->name('customers_transaction_download_contacts');
            
        Route::get('/downloads_contacts', [CustomersController::class, 'download_contacts_users'])
            ->name('customers_download_contacts_users');
            
            
        Route::get('/transaction_paiement_users_contacts', [CustomersController::class, 'paiement_users_contacts_transaction'])
            ->name('customers_paiement_users_contacts_transaction');

    });

}); 





Route::prefix('admin')->group(function () {

    /*Route::get('/logout', [CustomersController::class, 'logout'])
        ->name('customers_logout');*/

    Route::get('/login', [AdminsController::class, 'login_view'])
        ->name('admins_login');

    Route::post('/login', [AdminsController::class, 'login'])
        ->name('admins_login_form');

    /*Route::get('/join', [AdminsController::class, 'join_view'])
        ->name('admins_join');

    Route::post('/join', [AdminsController::class, 'join'])
        ->name('admins_join_form');

    Route::post('/join_confirmation', [AdminsController::class, 'join_confirm'])
        ->name('admins_join_form_confirm');*/


    Route::middleware('auth.admin')->group(function () {
        
         Route::get('/changement', [AdminsController::class, 'changer_password'])
        ->name('changerPassword');

        Route::post('change-password', [AdminsController::class, 'changer_mot_password'])
        ->name('changer_Password');
        
        

        //  Route pour la page de inscription des clients  
        Route::get('/dashboard', [AdminsController::class, 'dashboard'])
            ->name('admins_dashboard');

        Route::get('/list_of_admins', [AdminsController::class, 'list_of_admins'])
            ->name('admins_list_of_admins');


        Route::get('/join', [AdminsController::class, 'join_view'])
            ->name('admins_join');

        Route::post('/join', [AdminsController::class, 'join'])
            ->name('admins_join_form');

        Route::post('/join_confirmation', [AdminsController::class, 'join_confirm'])
            ->name('admins_join_form_confirm');

        Route::get('/logout', [AdminsController::class, 'logout'])
            ->name('admins_logout');


        Route::get('/add_contact', [AdminsController::class, 'add_contact'])
            ->name('admins_add_contact');


        Route::get('/add_contact_with_form', [AdminsController::class, 'add_contact_with_form'])
            ->name('admins_add_contact_with_form');


        Route::post('/add_contact_with_form', [AdminsController::class, 'add_contact_with_form_form'])
            ->name('admins_add_contact_with_form_form');

        Route::get('/add_contact_with_excel', [AdminsController::class, 'add_contact_with_excel'])
            ->name('admins_add_contact_with_excel');

        Route::post('/add_contact_with_excel', [AdminsController::class, 'add_contact_with_excel_form'])
            ->name('admins_add_contact_with_excel_form');

        Route::get('/send_message', [AdminsController::class, 'send_message'])
            ->name('admins_send_message');

        Route::get('/do_publication', [AdminsController::class, 'do_publication'])
            ->name('admins_do_publication');

        Route::post('/do_publication', [AdminsController::class, 'do_publication_form'])
            ->name('admins_do_publication_form');

        Route::get('/list_of_customers', [AdminsController::class, 'list_of_customers'])
            ->name('admins_list_of_customers');
            
            Route::get('/filter-of-customers', [AdminsController::class, 'filter_of_customers'])
            ->name('filter_of_customers');

        Route::post('/filter', [AdminsController::class, 'filter_of_customers_by_reseach'])
          ->name('filter_by_input');
        

        Route::post('/search', [AdminsController::class, 'search_customers'])
            ->name('admins_search_customers');

        Route::get('/do_publicity', [AdminsController::class, 'do_publicity'])
            ->name('admins_do_publicity');

        Route::post('/do_publicity', [AdminsController::class, 'do_publicity_form'])
            ->name('admins_do_publicity_form');


        Route::get('/send_publication', [AdminsController::class, 'send_publication'])
            ->name('admins_send_publication');

        Route::post('/send_publication', [AdminsController::class, 'send_publication_form'])
            ->name('admins_send_publication_form');


        Route::get('/write_message/{id}', [AdminsController::class, 'write_message'])
            ->name('admins_write_message');

        Route::post('/write_message/{id}', [AdminsController::class, 'write_message_form'])
            ->name('admins_write_message_form');

        Route::get('/send_email_to_all', [AdminsController::class, 'send_email_to_all'])
            ->name('admins_send_email_to_all');

        Route::post('/send_email_to_all', [AdminsController::class, 'send_email_to_all_form'])
            ->name('admins_send_email_to_all_form');

        Route::get('/send_message_what/{id}', [AdminsController::class, 'send_message_what'])
            ->name('admins_send_message_what');


        Route::get('/add_visitors', [AdminsController::class, 'add_visitors'])
            ->name('admins_add_visitors');

        Route::post('/add_visitors', [AdminsController::class, 'add_visitors_form'])
            ->name('admins_add_visitors_form');


        Route::get('/send_message_to_users', [AdminsController::class, 'send_message_to_users'])
            ->name('admins_send_message_to_users');


        Route::post('/send_message_to_users', [AdminsController::class, 'send_message_to_users_form'])
            ->name('admins_send_message_to_users_form');


        Route::get('/list_of_publicities', [AdminsController::class, 'list_of_publicities'])
            ->name('admins_list_of_publicities');

        Route::get('/list_of_publicities/delete/{id}', [AdminsController::class, 'delete_publicities'])
            ->name('admins_delete_publicities');

        Route::get('/list_of_publications', [AdminsController::class, 'list_of_publications'])
            ->name('admins_list_of_publications');


        Route::get('/list_of_publications/delete/{id}', [AdminsController::class, 'delete_publications'])
            ->name('admins_delete_publications');

        Route::get('/pub_to_home_page', [AdminsController::class, 'pub_to_home_page'])
            ->name('admins_pub_to_home_page');

        Route::post('/pub_to_home_page', [AdminsController::class, 'pub_to_home_page_form'])
            ->name('admins_pub_to_home_page_form');

        Route::get('/announce_event', [AdminsController::class, 'announce_event'])
            ->name('admins_announce_event');

        Route::post('/announce_event', [AdminsController::class, 'announce_event_form'])
            ->name('admins_announce_event_form');

        Route::get('/add_video', [AdminsController::class, 'add_video'])
            ->name('admins_add_video');

        Route::post('/add_video', [AdminsController::class, 'add_video_form'])
            ->name('admins_add_video_form');
            
        Route::get('/delete_user/{id}', [AdminsController::class, 'delete_user'])
            ->name('admins_delete_user');
            
        Route::get('/list_of_events', [AdminsController::class, 'list_of_events'])
            ->name('admins_list_of_events');
            
        Route::get('/delete_event/{id}', [AdminsController::class, 'delete_event'])
            ->name('admins_delete_event');
            
        Route::get('/list_of_pub_videos', [AdminsController::class, 'list_of_pub_videos'])
            ->name('admins_list_of_pub_videos');
            
        Route::get('/delete_video/{id}', [AdminsController::class, 'delete_video'])
            ->name('admins_delete_video');
            
        Route::get('/contacts/download', [AdminsController::class, 'download_contact'])
            ->name('admins_download_contact')
            ;

    });

});
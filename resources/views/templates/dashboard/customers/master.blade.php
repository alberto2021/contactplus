<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title') | {{ env('APP_NAME') }}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <script src="https://cdn.tailwindcss.com"></script>

<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link async rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2/dist/semantic.min.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
@yield('css')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      

        <li class="nav-item ">
            <a href="{{ route('customers_video') }}" class="nav-link">
              <span class="badge badge-info  pt-2 pb-2 pr-2" >Voir une vidéo publicitaire</span>
            </a>
        </li>&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  

      <li class="nav-item ">
        
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>




      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-user"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('customers_dashboard') }}" class="brand-link text-bold text-center">
      
      <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex ">
        
        <div class="info ">
          <a href="#" class="d-block text-center">{{ auth()->user()->nom }} {{ auth()->user()->prenom }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               
          @if (auth()->user()->has_module == "gratuit")
          @endif
         
          
         
          <li class="nav-item mb-4">
            <a href="{{ route('customers_activate_module') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Activer un module
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>
          
          
          <li class="nav-item mb-4">
            <a href="{{ route('customers_activate_service') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Activer un service
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>
          
           
          <li class="nav-item mb-4">
            <a href="{{ route('customers_do_publication') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Poster une annonce
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>
          
          
          <li class="nav-item mb-4">
            <a href="{{ route('customers_do_affiche') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Poster une affiche
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>
 
          <li class="nav-item mb-4">
            <a href="{{ route('customers_post_video') }}" class="nav-link active" style="background-color: #9e825a;">
              <i class="fas fa-bullhorn"></i>&nbsp;
              <p>
                Poster une vidéo
                <span class="right badge badge-danger">Nouveau</span>
              </p>
            </a>
          </li>
          

          
          <li class="nav-item mb-4 " >
            <a href="{{ route('customers_download_contacts_users') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-id-card-alt"></i> &nbsp;
              <p>
                Télécharger les contacts des nouveaux inscrits
              </p>
            </a>
          </li>

          <li class="nav-item mb-4" >
            <a href="{{ route('customers_contact') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-id-card-alt"></i> &nbsp;
              <p>
                 Télécharger les contacts des anciens inscrits
              </p>
            </a>
          </li>

          <li class="nav-item mb-4">
            <a href="{{ route('customers_events') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Mes Evènements 
                
              </p>
            </a>
          </li>

          <li class="nav-item ">
            <a href="#" class="nav-link active" style="background-color: #9e825a; display: none;">
              <i class="fas fa-bullhorn"></i>&nbsp;
              <p>
                Publications
              </p>
            </a>
          </li>

          <li class="nav-item  mb-4" >
            <a href="{{ route('customers_inbox') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="far fa-envelope"></i>&nbsp;
              <p>
                Boîte de messagerie
                <span class="right badge badge-danger">{{ new_message_not_open() }}</span>
              </p>
            </a>
          </li>
          
          
          
          
          
          
          
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  @yield('content')
  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item mb-4">
            <a href="{{ route('customers_profil') }}" class="nav-link active" style="background-color: #229ddc;">
              
              <p>
                Mon profil
              </p>
            </a>
          </li>

          <li class="nav-item mb-4">
            <a href="{{ route('customers_abonns') }}" class="nav-link active" style="background-color: #229ddc;">
              
              <p>
                Mes abonnements
              </p>
            </a>
          </li>

          <li class="nav-item ">
            <a href="{{ route('customers_logout') }}" class="nav-link active" style="background-color: #229ddc;">
              
              <p>
                Se déconnecter
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Cercle du Commerce Social
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2022 <a href="">CCS CONTACT+</a>.</strong> Tous droits réservés.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@yield('js')
<!-- jQuery -->

<!-- Bootstrap 4 -->
<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
</body>
</html>
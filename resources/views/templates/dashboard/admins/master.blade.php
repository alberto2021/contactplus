<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title') | {{ env('APP_NAME') }}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->

  <link async rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2/dist/semantic.min.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>


  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <script src="https://cdn.tailwindcss.com"></script>

<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

<link href="{{ asset('dist/select2/dist/css/select2.min.css') }}" rel="stylesheet">

  
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      

      
      <li class="nav-item ">
        @if (auth()->guard('admin')->user()->is_simple_admin)
          <a class="nav-link " href="#">
            <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-2 pl-2 pr-2" >Administrateur</span>
          </a>
        @else
          <a class="nav-link " href="#">
            <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-2 pl-2 pr-2" >Super administrateur</span>
          </a>
        @endif
        
      </li>




      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-user"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admins_dashboard') }}" class="brand-link text-bold text-center">
      
      <span class="brand-text font-weight-light">Administration - {{ env('APP_NAME') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex ">
        
        <div class="info ">
          <a href="#" class="d-block text-center">{{ auth()->guard('admin')->user()->username }} | 
            @if (auth()->guard('admin')->user()->is_simple_admin)
              Administrateur
            @else
              Super administrateur
            @endif
          </a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        @if (auth()->guard('admin')->user()->is_super_admin) 
          <li class="nav-item mb-4 mt-3" >
            <a href="{{ route('admins_download_contact') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-id-card-alt"></i> &nbsp;
              <p>
                Télécharger les contacts des utilisateurs
              </p>
            </a>
          </li>
          <li class="nav-item mb-4 mt-3" >
            <a href="{{ route('admins_add_contact') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-id-card-alt"></i> &nbsp;
              <p>
                Ajouter des contacts
              </p>
            </a>
          </li>
        
          <li class="nav-item mb-4">
            <a href="{{ route('email') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Envoyer une publicité-affiche par E-mail
              </p>
            </a>
          </li>
            
          <li class="nav-item mb-4">
            <a href="{{ route('admins_pub_to_home_page') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Publier une affiche sur la page d'accueil
              </p>
            </a>
          </li>
        
          <li class="nav-item mb-4">
            <a href="{{ route('admins_add_video') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Mettre une vidéo/Accueil
              </p>
            </a>
          </li>
        
          <li class="nav-item mb-4">
            <a href="{{ route('admins_announce_event') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Annoncer un évènement
              </p>
            </a>
          </li>
        @endif

          <li class="nav-item mb-4">
            <a href="{{ route('admins_do_publication') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Ajouter une publicité-affiche
              </p>
            </a>
          </li>

          <li class="nav-item mb-4">
            <a href="{{ route('admins_do_publicity') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-calendar-day"></i>&nbsp;
              <p>
                Ajouter une publicité écrite
              </p>
            </a>
          </li>
        
          <li class="nav-item mb-4">
            <a href="{{ route('admins_send_message') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-bullhorn"></i>&nbsp;
              <p>
                Envoyer des messages
              </p>
            </a>
          </li>

          <li class="nav-item mb-4">
            <a href="{{ route('admins_send_message_to_users') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="fas fa-bullhorn"></i>&nbsp;
              <p>
                Envoyer un message aux utilisateurs
              </p>
            </a>
          </li>
        
          <li class="nav-item mb-4">
            <a href="{{ route('admins_list_of_customers') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="far fa-envelope"></i>&nbsp;
              <p>
                Liste des clients
              </p>
            </a>
          </li>
          
          <li class="nav-item mb-4">
              <a href="{{ route('filter_of_customers') }}" class="nav-link active" style="background-color: #9e825a">
                <i class="far fa-envelope"></i>&nbsp;
                <p>
                  Rechercher les clients par option
                </p>
              </a>
            </li>
            
        @if (auth()->guard('admin')->user()->is_super_admin)
          <li class="nav-item mb-4">
            <a href="{{ route('admins_add_visitors') }}" class="nav-link active" style="background-color: #9e825a">
              
              <p>
                Ajouter le nombre de visiteurs
              </p>
            </a>
          </li>
        @endif
          <li class="nav-item mb-4">
            <a href="{{ route('admins_list_of_publicities') }}" class="nav-link active" style="background-color: #9e825a">
              
              <p>
                Liste des publicités Textes
              </p>
            </a>
          </li>


          <li class="nav-item mb-4">
            <a href="{{ route('admins_list_of_publications') }}" class="nav-link active" style="background-color: #9e825a">
              
              <p>
                Liste des publications affiches
              </p>
            </a>
          </li>
          
          <li class="nav-item mb-4">
            <a href="{{ route('admins_list_of_events') }}" class="nav-link active" style="background-color: #9e825a">
              
              <p>
                Liste des évènements
              </p>
            </a>
          </li>
          
          
          <li class="nav-item mb-4">
            <a href="{{ route('admins_list_of_pub_videos') }}" class="nav-link active" style="background-color: #9e825a">
              
              <p>
                Liste des vidéos publicitaires
              </p>
            </a>
          </li>

          @if (auth()->guard('admin')->user()->is_super_admin) 

          <li class="nav-item mb-4">
            <a href="{{ route('admins_join') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="far fa-envelope"></i>&nbsp;
              <p>
                Ajouter un administrateur
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{ route('admins_list_of_admins') }}" class="nav-link active" style="background-color: #9e825a">
              <i class="far fa-envelope"></i>&nbsp;
              <p>
                Liste des administrateurs
              </p>
            </a>
          </li>
          @endif

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  @yield('content')
  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="{{ route('admins_logout') }}" class="nav-link active" style="background-color: #229ddc;">
              
              <p>
                Se déconnecter
              </p>
            </a>
          </li>
          
           <li class="nav-item ">
            <a href="{{ route('changerPassword') }}" class="nav-link active" style="background-color: #229ddc;">
              
              <p>
                Changer son mot de passe
              </p>
            </a>
          </li>

        </ul>
      </nav>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Administration - Cercle Du Commerce Social
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2022 <a href="">CCS+</a>.</strong> Tous droits réservés.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
@yield('js')


<!-- Bootstrap 4 -->
<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
      (function() {
        $('.select2').select2({})
      })();
    })
  </script>

</body>
</html>
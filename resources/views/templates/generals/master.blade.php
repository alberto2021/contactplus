<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>@yield('title') - {{ env('APP_NAME') }}</title>
		
		@include('templates.components.css')
		@yield('stylesheets')
	</head>

	<body>
		@include('templates.components.header')

		@yield('content')

		@include('templates.components.footer')

		@include('templates.components.js')
		@yield('javascripts')
	</body>

</html>
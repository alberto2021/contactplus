<footer class=" text-center text-lg-start pt-10 pb-4" style="background-color: #9e8359; color: #FFFFFF;">

    <div class="container pl-12 pr-12 pb-12">

      <div class="row ">

        <div class="col-md-2 pb-8">

          <h1 class="" style="font-size:30px;font-weight:bold">CCS</h1>
          <ul >

            <li>Cercle du Commerce Social</li>
            

          </ul>
          
        </div>

        


        <div class="col-md-4 mb-5 pb-8 ">

          <h1 class="" style="font-size:25px;font-weight:bold">Adresses</h1>

          
              <ul >

            <li>Gbodjè von Saint Thomas non loin de l'école Sèdjro</li>
             <li>Numéro téléphonique : +229 98 73 61 75</li>
             <li>Numéro Whatsapp : +229 98 73 61 75</li>
             <li>contact@cercle-cs.com</li>
             <li>groupeducommercesocial@gmail.com</li>
            

          </ul>

          
          
        </div>
        
        <div class="col-md-3 mb-5 pb-8">

          <h1 class="" style="font-size:25px;font-weight:bold">Heures de travail</h1>

         <ul >

            <li>Lundi-Vendredi: 08h à 18h</li>
             <li>Samedi :Uniquement sur Rendez-vous</li>
             
            

          </ul>
          
        </div>

        <div class="col-md-3 ">

          <h1 class="pb-2" style="font-size:25px;font-weight:bold">Suivez-nous</h1>

          <div class="" >

            <p style="text-align:center;" class="pt-8">

            <a class="btn-floating  text-center" style="background-color: white; 
            color: #2ba9e1;" 
            type="button" role="button" target="_blank" href="https://www.facebook.com/groupeccs/"><i class="fab fa-facebook-f"></i></a>

            <a class="btn-floating  text-center ml-2" style="background-color: white; 
            color:#2ba9e1" 
            type="button" role="button" target="_blank" href="https://www.instagram.com/ccs_commercesocial/"><i class="fab fa-instagram"></i></a>

            <a class="btn-floating  text-center ml-2" style="background-color: white; 
            color: #2ba9e1;" 
            type="button" role="button" target="_blank" href="https://wa.me/22998736175"><i class="fab fa-whatsapp"></i></a>


          </p>

          </div>
         
          </p>
          
        </div>
        
        
        

      </div>

    </div>
    

    <div class="container p-3 " style="background-color: #1a2649; color: #FFFFFF;opacity: 0.5; font-size: 12px;">
      <hr>
      
        <div class="row pt-3">
            <div class="col-lg-6">
                <p class="text-left">&copy;  
                <a class="text-white text-reset " href="{{ route('welcome') }}">{{ env('APP_NAME') }}</a>
                  </p>
            </div>

            <div class="col-lg-6">
                <p class="text-right"><b>La solution idéale pour vos ventes et vos achats</b></p>
            </div>
        </div>

    </div>
    
</footer>
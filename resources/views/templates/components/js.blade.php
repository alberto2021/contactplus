<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.2/mdb.min.js"
></script>

<script type="text/javascript">
	new WOW().init();
</script>




<!-- jQuery -->
        <script src="/js/jquery-3.0.0.min.js"></script>
        <script src="/js/jquery-migrate-3.0.0.min.js"></script>

        <!-- popper.min -->
        <script src="/js/popper.min.js"></script>

        <!-- bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- scrollIt -->
        <script src="/js/scrollIt.min.js"></script>

        <!-- jquery.waypoints.min -->
        <script src="/js/jquery.waypoints.min.js"></script>

        <!-- owl carousel -->
        <script src="/js/owl.carousel.min.js"></script>

        <!-- jquery.magnific-popup js -->
        <script src="/js/jquery.magnific-popup.min.js"></script>

        <!-- stellar js -->
        <script src="/js/jquery.stellar.min.js"></script>

        <!-- isotope.pkgd.min js -->
        <script src="/js/isotope.pkgd.min.js"></script>

        <!-- YouTubePopUp.jquery -->
        <script src="/js/YouTubePopUp.jquery.js"></script>

        <!-- validator js -->
        <script src="/js/validator.js"></script>

        <!-- custom scripts -->
        <script src="/js/scripts.js"></script>



<style type="text/css">
  @media only screen and (max-width: 1000px) {
    .adevoi2{
      display: block;
    }

  }

  @media only screen and (max-width: 800px) {
    .adevoi {
      display: block;
    }

  }
</style>
    <nav class="navbar navbar-expand-lg">
      <div class="container">

            <!-- Logo -->
            <a class="" href="{{ route('welcome') }}">
               <h1><span style="color: #9e8359">CCS</span> <span style="color: #2ba9e1;font-weight: bold">CONTACT+</span></h1>     
            </a>

            <a class="nav-link btn btn-outline-success adevoi" href="#" data-scroll-nav="6" style="display: none;">Postez une annonce</a>
        <button class="navbar-toggler adevoi2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"><i class="fas fa-bars" style="color: black;"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent"">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link btn btn-primary" href="{{route('watch_video')}}" style="color:white">Voir une vidéo publicitaire</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn btn-primary" href="https://wa.me/22998736175" target="_blank"  style="color:white">Postez une annonce</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

      <!-- End Navbar ====
      ======================================= -->


      <!-- =====================================
      ==== Start Header -->

      <header class="header pos-re slider-fade" data-scroll-index="0">

        <div class="owl-carousel owl-theme">
                <div class="item bg-img" data-overlay-dark="4" data-background="/assets/img/banane_igname.jpg">
              <div class="text-center v-middle caption mt-30">
                        <h1 style="font-size: 35px;">Boostez vos activités</h1>
                        <div class="row">
                            <div class="offset-md-1 col-md-10 offset-lg-2 col-lg-8">
                                <p style="font-size: 1.5em; text-align: justify,padding:7px">Le CCS est le cercle du commerce social regroupant plus de 6000 personnes (acheteurs, vendeurs, consommateurs, chef d'entreprises, etc.) pour faciliter les échanges commerciaux entre les membres. <br> CCS+ est un service qui vous permet de faire voir vos produits aux membres de ce cercle en vous ajoutant au cercle. </p>
                            </div>
                        </div>
                        <a href="{{route('join')}}" class="butn butn-light mt-30" style="background:#2ba9e1;color:white" >
                            <span style="color:white;font-weight:bold">Inscrivez-vous  </span>
                        </a>

                        <a href="{{route('login')}}" class="butn butn-bg" style="background:#9e8359" >
                            <span style="color:white;font-weight:bold">Se connecter</span>
                        </a>
                    </div>
                </div>
                <div class="item bg-img" data-overlay-dark="5" data-background="/assets/img/maman.jpg">
                    <div class="text-center v-middle caption mt-30">
                        <h1 style="font-size: 40px;">Boostez vos activités</h1>
                        <div class="row">
                            <div class="offset-md-1 col-md-10 offset-lg-2 col-lg-8">
                                <p style="font-size: 1.5em; text-align: justify,padding:7px">Le CCS est le cercle du commerce social regroupant plus de 6000 personnes (acheteurs, vendeurs, consommateurs, chef d'entreprises, etc.) pour faciliter les échanges commerciaux entre les membres. <br> CCS+ est un service qui vous permet de faire voir vos produits aux membres de ce cercle en vous ajoutant au cercle. </p>
                            </div>
                        </div>
                        <a href="{{route('join')}}" class="butn butn-light mt-30" style="background:#2ba9e1;color:white">
                            <span style="color:white">Inscrivez-vous  </span>
                        </a>

                        <a href="{{route('login')}}" class="butn butn-bg" style="background:#9e8359">
                            <span>Se connecter</span>
                        </a>
                    </div>
                </div>
                
        </div>
      </header>
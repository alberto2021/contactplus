@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#description').summernote({
      placeholder: 'Saisissez le contenu de la publication ici',
        tabsize: 2,
        height: 150,
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', ]],
          ['view', ['fullscreen', 'codeview', 'help']],
          ['fontname', ['fontname']],
        ],
        popover: {
          image: [
            ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
            ['float', ['floatLeft', 'floatRight', 'floatNone']],
            ['remove', ['removeMedia']]
          ],
          link: [
            ['link', ['linkDialogShow', 'unlink']]
          ],
          table: [
            ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
            ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
          ],
          air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']],
            ['para', ['ul', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture']]
          ]
        }


    });
  });
</script>
@stop

@section('content')
<div class="content-wrapper">
  	@if(session()->has('contact'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('contact') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-8">
            <h1 class="m-0">Faire des publications, annonces ou des affiches.</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire de publication</h2>
                @if(session()->has('publication'))
                  <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
                      {{ session()->get('publication') }}
                  </div>
                @endif
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-2 offset-lg-2 col-lg-8 col-md-8 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('admins_do_publication_form') }}" method="post" enctype="multipart/form-data">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="nom">Saisissez le titre de la publication</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="nom" name="nom" class="form-control @error('nom') is-invalid @enderror" value="{{ old('nom') }}" />

                                @error('nom')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        <div class="row mb-4">

                          <div class="col" style="text-align: left;">
                              
                              <label class="form-label" for="description">Saisissez le contenu de la publication</label>

                              <div style="margin-bottom: 10px;"></div>

                              <textarea name="description" id="description"  style="" rows="6" cols="30" class="form-control"></textarea>

                              @error('description')

                                  <div class="" style="color: red;">{{ $message }}</div>

                              @enderror
                              
                          </div>
  
                        </div>

                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="video">Uploader une vidéo (facultatif)</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="file" id="video" name="video" class="form-control @error('video') is-invalid @enderror" value="{{ old('video') }}"  />

                                @error('video')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="image">Uploader une image (facultatif)</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') }}"  />

                                @error('image')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="duree">Durée de la publicité (en jours)</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="number" id="duree" name="duree" class="form-control @error('duree') is-invalid @enderror" value="{{ old('duree') }}" min="1" />

                                @error('duree')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Publier</button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
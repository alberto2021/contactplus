@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('js')

@stop

@section('content')
<div class="content-wrapper">
  	@if(session()->has('contact'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('contact') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-8">
            <h1 class="m-0">Ajouter le nombre de visiteurs</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          @if(session()->has('visiteur'))
                  <div class="ui message success">
                      {{ session()->get('visiteur') }}
                  </div>
                @endif
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title"><div class="ui message green">Nombre de visiteurs actuel sur le site: {{ $users->count() }}</div>  <div class="ui message info">Nombre de visiteurs ajouté récemment sur le site: {{ $admin->paid ?? '' }}</div> <div class="ui message info">Nombre de visiteurs total sur le site: {{ $admin->paid + $users->count() }}</div></h2>
                
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-2 offset-lg-2 col-lg-8 col-md-8 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('admins_add_visitors_form') }}" method="post">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="visitor">Nombre de visiteurs</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="number" id="visitor" name="visitor" class="form-control @error('visitor') is-invalid @enderror" value="{{ old('visitor') }}" min="1" />

                                @error('visitor')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Enregistrer</button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
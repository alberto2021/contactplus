@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ajouter des contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Choisissez une option</h2>
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="col-lg-3 col-md-3 ">
                    
                  </div>

                  <div class="col-lg-3 col-md-3 mb-6">
                    <a href="{{ route('admins_add_contact_with_form') }}" class="btn btn-success">Remplir un formulaire d'insertion de contact</a>
                  </div>

                  ou

                  <div class="col-lg-3 col-md-3 ">
                    <a href="{{ route('admins_add_contact_with_excel') }}" class="btn btn-info">Importez depuis un fichier Excel</a>
                    
                  </div>

                  <div class="col-lg-3 col-md-3 ">
                    
                  </div>
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
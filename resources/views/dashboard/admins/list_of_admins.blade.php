@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Liste de tous les administrateurs</h1>
          </div><!-- /.col -->
          <div class="col-sm-6" >
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <hr>

    <!-- Main content -->
    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Identifiant
                                        </th>

                                        <th>
                                            Nom d'utilisateur
                                        </th>

                                        <th>
                                            Email
                                        </th>                               

                                        <th>
                                            Date de création du compte 
                                        </th>                                  
                                        
                                        <th>
                                            Compte créé par 
                                        </th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $admins as $proprio )
                                      @if ($proprio->is_super_admin)
                                        <tr class="alert alert-info">
                                            <td>
                                                 {{ $proprio->id ?? '' }}
                                            </td>
                                            <td>
                                                {{ $proprio->username ?? ''}}  
                                                
                                            </td>
                                            <td>
                                                {{ $proprio->email ?? ''}}
                                            </td>
                                            <td>
                                                {{ $proprio->created_at ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->created_by ?? ''}}
                                            </td>                                 
                                        </tr>
                                      @else
                                        <tr >
                                            <td>
                                                 {{ $proprio->id ?? '' }}
                                            </td>
                                            <td>
                                                {{ $proprio->username ?? ''}}  
                                                
                                            </td>
                                            <td>
                                                {{ $proprio->email ?? ''}}
                                            </td>
                                            <td>
                                                {{ $proprio->created_at ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->created_by ?? ''}}
                                            </td>                                 
                                        </tr>
                                      @endif
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@stop
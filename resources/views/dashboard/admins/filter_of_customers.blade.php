@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

<link href="{{ asset('dist/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

 


@section('content')
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">
                    <h1 class="m-0">Rechercher utilisateur par paramétre</h1>
                </div><!-- /.col -->
                <form action="{{ route('filter_by_input') }}" method="post">
                    @csrf

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <select  class="custom-select  @error('search') is-invalid @enderror"
                             id="search" name="search" onchange="displayRaison()" required>
                                <option selected disabled value="">--Veuillez choisir une option--</option>
                                <option value="pays">Par pays</option>
                                <option value="profession">Par profession</option>
                                <option value="ville">Par ville</option>
                                <option value="nom">Par nom</option>
                                <option value="interval">Par interval de date</option>
                            </select>
                            @error('search')
                            <div class="" style="color: red;">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="input-group-prepend" style="display: none;" id="searchCountryDiv">
                        <select id="searchCountry" class="custom-select select2  @error('option_reseach') is-invalid @enderror" 
                         name="option_reseach1">
                            <option selected disabled value="">--Veuillez choisir un pays--</option>
                            <option value="+93">Afghanistan</option>
                            <option value="+27">Afrique du Sud</option>
                            <option value="+355">Albanie</option>
                            <option value="+213">Algérie</option>
                            <option value="+49">Allemagne</option>
                            <option value="+376">Andorre</option>
                            <option value="+244">Angola</option>
                            <option value="+1264">Anguilla</option>
                            <option value="+1268">Antigua-et-Barbuda</option>
                            <option value="+599">Antilles néerlandaises</option>
                            <option value="+966">Arabie saoudite</option>
                            <option value="+54">Argentine</option>
                            <option value="+374">Arménie</option>
                            <option value="+297">Aruba</option>
                            <option value="+247">Ascension</option>
                            <option value="+61">Australie</option>
                            <option value="+43">Autriche</option>
                            <option value="+994">Azerbaïdjan</option>
                            <option value="+1242">Bahamas</option>
                            <option value="+973">Bahreïn</option>
                            <option value="+880">Bangladesh</option>
                            <option value="+1246">Barbade</option>
                            <option value="+32">Belgique</option>
                            <option value="+501">Belize</option>
                            <option value="+229">Bénin</option>
                            <option value="+1441">Bermudes</option>
                            <option value="+975">Bhoutan</option>
                            <option value="+375">Biélorussie</option>
                            <option value="+95">Birmanie</option>
                            <option value="+591">Bolivie</option>
                            <option value="+387">Bosnie-Herzégovine</option>
                            <option value="+267">Botswana</option>
                            <option value="+55">Brésil</option>
                            <option value="+673">Brunei Darussalam</option>
                            <option value="+359">Bulgarie</option>
                            <option value="+226">Burkina Faso</option>
                            <option value="+257">Burundi</option>
                            <option value="+855">Cambodge</option>
                            <option value="+237">Cameroun</option>
                            <option value="+1">Canada</option>
                            <option value="+238">Cap-Vert</option>
                            <option value="+1345">Îles Caïmans</option>
                            <option value="+236">République centrafricaine</option>
                            <option value="+56">Chili</option>
                            <option value="+86">Chine</option>
                            <option value="+357">Chypre</option>
                            <option value="+57">Colombie</option>
                            <option value="+269">Comores</option>
                            <option value="+242">Congo</option>
                            <option value="+243">Congo (Rép. Democratique)</option>
                            <option value="+682">Cook (Îles)</option>
                            <option value="+850">Corée du Nord</option>
                            <option value="+82">Corée du Sud</option>
                            <option value="+506">Costa Rica</option>
                            <option value="+225">Côte d'Ivoire</option>
                            <option value="+385">Croatie</option>
                            <option value="+53">Cuba</option>
                            <option value="+45">Danemark</option>
                            <option value="+246">Diego Garcia</option>
                            <option value="+253">Djibouti</option>
                            <option value="+1767">Dominique</option>
                            <option value="+20">Égypte</option>
                            <option value="+971">Émirats arabes unis</option>
                            <option value="+593">Équateur</option>
                            <option value="+291">Érythrée</option>
                            <option value="+34">Espagne</option>
                            <option value="+372">Estonie</option>
                            <option value="+251">Éthiopie</option>
                            <option value="+298">Féroé (Îles)</option>
                            <option value="+679">Fidji</option>
                            <option value="+358">Finlande</option>
                            <option value="+33">France</option>
                            <option value="+241">Gabon</option>
                            <option value="+220">Gambie</option>
                            <option value="+995">Géorgie</option>
                            <option value="+233">Ghana</option>
                            <option value="+350">Gibraltar</option>
                            <option value="+30">Grèce</option>
                            <option value="+1473">Grenade</option>
                            <option value="+299">Groenland</option>
                            <option value="+590">Guadeloupe</option>
                            <option value="+1671">Guam</option>
                            <option value="+502">Guatemala</option>
                            <option value="+224">Guinée</option>
                            <option value="+240">Guinée équatoriale</option>
                            <option value="+245">Guinée-Bissau</option>
                            <option value="+592">Guyana</option>
                            <option value="+594">Guyane</option>
                            <option value="+509">Haïti</option>
                            <option value="+504">Honduras</option>
                            <option value="+852">Hong Kong</option>
                            <option value="+36">Hongrie</option>
                            <option value="+91">Inde</option>
                            <option value="+62">Indonésie</option>
                            <option value="+964">Irak</option>
                            <option value="+98">Iran</option>
                            <option value="+353">Irlande</option>
                            <option value="+354">Islande</option>
                            <option value="+972">Israël</option>
                            <option value="+39">Italie</option>
                            <option value="+1876">Jamaïque</option>
                            <option value="+81">Japon</option>
                            <option value="+962">Jordanie</option>
                            <option value="+7">Kazakhstan</option>
                            <option value="+254">Kenya</option>
                            <option value="+996">Kirghizistan</option>
                            <option value="+686">Kiribati</option>
                            <option value="+965">Koweït</option>
                            <option value="+856">Laos</option>
                            <option value="+266">Lesotho</option>
                            <option value="+371">Lettonie</option>
                            <option value="+961">Liban</option>
                            <option value="+231">Liberia</option>
                            <option value="+218">Libye</option>
                            <option value="+423">Liechtenstein</option>
                            <option value="+370">Lituanie</option>
                            <option value="+352">Luxembourg</option>
                            <option value="+853">Macao</option>
                            <option value="+389">Macédoine</option>
                            <option value="+261">Madagascar</option>
                            <option value="+60">Malaisie</option>
                            <option value="+265">Malawi</option>
                            <option value="+960">Maldives</option>
                            <option value="+223">Mali</option>
                            <option value="+500">Îles Malouines</option>
                            <option value="+356">Malte</option>
                            <option value="+1670">Mariannes du Nord (Îles)</option>
                            <option value="+212">Maroc</option>
                            <option value="+692">Marshall</option>
                            <option value="+596">Martinique</option>
                            <option value="+230">Maurice</option>
                            <option value="+222">Mauritanie</option>
                            <option value="+262">Mayotte</option>
                            <option value="+52">Mexique</option>
                            <option value="+691">Micronésie</option>
                            <option value="+373">Moldavie</option>
                            <option value="+377">Monaco</option>
                            <option value="+976">Mongolie</option>
                            <option value="+382">Monténégro</option>
                            <option value="+1664">Montserrat</option>
                            <option value="+258">Mozambique</option>
                            <option value="+264">Namibie</option>
                            <option value="+674">Nauru</option>
                            <option value="+977">Népal</option>
                            <option value="+505">Nicaragua</option>
                            <option value="+227">Niger</option>
                            <option value="+234">Nigeria</option>
                            <option value="+683">Niue</option>
                            <option value="+47">Norvège</option>
                            <option value="+687">Nouvelle-Calédonie</option>
                            <option value="+64">Nouvelle-Zélande</option>
                            <option value="+968">Oman</option>
                            <option value="+256">Ouganda</option>
                            <option value="+998">Ouzbékistan</option>
                            <option value="+92">Pakistan</option>
                            <option value="+680">Palaos</option>
                            <option value="+970">Palestine</option>
                            <option value="+507">Panama</option>
                            <option value="+675">Papouasie-Nouvelle-Guinée</option>
                            <option value="+595">Paraguay</option>
                            <option value="+31">Pays-Bas</option>
                            <option value="+51">Pérou</option>
                            <option value="+63">Philippines</option>
                            <option value="+48">Pologne</option>
                            <option value="+689">Polynésie française</option>
                            <option value="+351">Portugal</option>
                            <option value="+974">Qatar</option>
                            <option value="+262">Réunion</option>
                            <option value="+40">Roumanie</option>
                            <option value="+44">Royaume-Uni</option>
                            <option value="+7">Russie</option>
                            <option value="+250">Rwanda</option>
                            <option value="+1869">Saint-Christophe-et-Niévès</option>
                            <option value="+290">Sainte-Hélène, Ascension</option>
                            <option value="+1758">Sainte-Lucie</option>
                            <option value="+378">Saint-Marin</option>
                            <option value="+1721">Saint-Martin (Sint-Maarten)</option>
                            <option value="+590">Saint-Martin</option>
                            <option value="+508">Saint-Pierre-et-Miquelon</option>
                            <option value="+1784">Saint-Vincent-et-les-Grenadines</option>
                            <option value="+677">Salomon</option>
                            <option value="+503">Salvador</option>
                            <option value="+685">Samoa</option>
                            <option value="+1684">Samoa américaines</option>
                            <option value="+239">Sao Tomé-et-Principe</option>
                            <option value="+221">Sénégal</option>
                            <option value="+381">Serbie</option>
                            <option value="+248">Seychelles</option>
                            <option value="+232">Sierra Leone</option>
                            <option value="+65">Singapour</option>
                            <option value="+421">Slovaquie</option>
                            <option value="+386">Slovénie</option>
                            <option value="+252">Somalie</option>
                            <option value="+249">Soudan</option>
                            <option value="+211">Soudan du Sud</option>
                            <option value="+94">Sri Lanka</option>
                            <option value="+46">Suède</option>
                            <option value="+41">Suisse</option>
                            <option value="+597">Suriname</option>
                            <option value="+268">Swaziland</option>
                            <option value="+963">Syrie</option>
                            <option value="+992">Tadjikistan</option>
                            <option value="+255">Tanzanie</option>
                            <option value="+886">Taïwan</option>
                            <option value="+235">Tchad</option>
                            <option value="+420">République tchèque</option>
                            <option value="+672">Territoires extérieurs - Australie</option>
                            <option value="+66">Thaïlande</option>
                            <option value="+670">Timor oriental</option>
                            <option value="+228">Togo</option>
                            <option value="+690">Tokelau</option>
                            <option value="+676">Tonga</option>
                            <option value="+1868">Trinité-et-Tobago</option>
                            <option value="+216">Tunisie</option>
                            <option value="+993">Turkménistan</option>
                            <option value="+1649">Turks et Caico (Îles)</option>
                            <option value="+90">Turquie</option>
                            <option value="+688">Tuvalu</option>
                            <option value="+380">Ukraine</option>
                            <option value="+598">Uruguay</option>
                            <option value="+678">Vanuatu</option>
                            <option value="+39">Vatican (Saint-Siège)</option>
                            <option value="+58">Venezuela</option>
                            <option value="+1340">Vierges Américaines (Îles)</option>
                            <option value="+1284">Vierges Britanniques (Îles)</option>
                            <option value="+84">Viêt Nam</option>
                            <option value="+681">Wallis-et-Futuna</option>
                            <option value="+967">Yémen</option>
                            <option value="+260">Zambie</option>
                            <option value="+26">Zimbabwe</option>
                        </select>
                        </div>

                        <input type="text" style="display: none;" class="form-control" name="option_reseach" id="option_reseach">
                        
                        <input type="date" id="by_date" style="display: none;" class="form-control mb-3"  name="date1" class="form-control">
                        <input type="date" id="by_date1" style="display: none;" class="form-control mb-3"  name="date2" class="form-control">

                        <button class="btn btn-sidebar" type="submit">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </form>

                <!-- @error('search')
                          <div class="" style="color: red;">{{ $message }}</div>
                      @enderror -->


            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


    <hr>



    <div class="content mt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 ">

                    <div class="card">
                        <div class="card-header">
                            <h1 class="font-weight-bold" style="font-size: 20px;">
                                @if($what) {{ $what }} @endif
                            </h1>
                        </div>
                        <div class="card-body ">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped"  id="dataTable" cellspacing="0" width="100%">
                                    <thead class=" text-info">
                                        <tr>
                                            <th>N°</th>
                                            <th>Nom & Prénoms</th>
                                            <th>Téléphone</th>
                                            <th>Email</th>
                                            <th>Profession</th>
                                            <th>Adresse</th>
                                            <th>Mode</th>
                                            <th>Date inscription</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($customers))
                                        @if(count($customers) > 0)
                                        @foreach( $customers as $proprio )
                                        <tr>
                                            <td>{{ $loop->iteration ?? '' }} </td>
                                            <td>{{ $proprio->nom ?? ''}} {{ $proprio->prenom ?? ''}}</td>
                                            <td> {{ $proprio->phone ?? ''}}</td>
                                            <td>{{ $proprio->email ?? ''}} </td>
                                            <td>{{ $proprio->profession ?? ''}}</td>
                                            <td>{{ $proprio->adresse ?? ''}}</td>
                                            <td>{{ $proprio->mode ?? ''}}</td>
                                            <td>{{ $proprio->created_at->format('d/m/Y H:m:s') ?? ''}}</td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <td colspan="8">PAS ENCORE DE DONNEE</td>
                                        @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
</div>




<script src="{{ asset('dist/vendor/datatables/jquery.dataTables.js') }}" defer></script>
  <script src="{{ asset('dist/vendor/datatables/dataTables.bootstrap4.js') }}" defer></script>
  <script src="{{ asset('dist/vendor/datatables-demo.js') }}" defer></script>
<script src="{{ asset('dist/select2/dist/js/select2.min.js') }}"></script>
        


<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
        displayRaison();

        // $('select.select-champ').select2({
        //     theme: "material"
        // });

    })


    function displayRaison() {
        var search = $("#search").val();

        if (search == 'pays') {

            $("#searchCountry").attr('required', true);
            $("#option_reseach").attr('required', false);
            $("#by_date").attr('required', false);
            $("#by_date1").attr('required', false);

             
            $("#searchCountryDiv").show();
            $("#option_reseach").hide();
            $("#by_date").hide();
            $("#by_date1").hide();
        } else if(search == 'profession' || search == 'ville' || search == 'nom') {

            $("#searchCountryDiv").hide();
            $("#by_date").hide();
            $("#by_date1").hide();
            $("#option_reseach").show();

            $("#searchCountry").attr('required', false);
            $("#option_reseach").attr('required', true);
            $("#by_date").attr('required', false);
            $("#by_date1").attr('required', false);
        }else if(search == 'interval'){

            $("#searchCountryDiv").hide();
            $("#option_reseach").hide();
            $("#by_date").show();
            $("#by_date1").show();

            $("#searchCountry").attr('required', false);
            $("#option_reseach").attr('required', false);
            $("#by_date").attr('required', true);
            $("#by_date1").attr('required', true);
        }
    }
</script>
@stop
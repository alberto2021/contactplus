@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  	@if(session()->has('excel'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('excel') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ajouter des contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire d'ajout de contact</h2>
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('admins_add_contact_with_excel_form') }}" method="post" enctype="multipart/form-data">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="file">Uploader le fichier de contact Excel</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="file" id="file" name="file" class="form-control @error('file') is-invalid @enderror" value="{{ old('file') }}" />

                                @error('file')

                                    <div class="alert alert-danger">{{ $message }}</div>

                                @enderror
                                
                            </div>
  
                        </div>
                        

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Enregistrer </button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Envoyer un message</h1>
          </div><!-- /.col -->
          <div class="col-sm-6" >
            <a href="{{ route('admins_send_email_to_all' ) }}" class="ui button primary">Envoyez un message par mail à tous les utilisateurs.</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <hr>
    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    <div class="card-header">
                        @if(session()->has('sendit'))
                          <div class="ui success message">
                              {{ session()->get('sendit') }}
                          </div>
                        @endif
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Identifiant
                                        </th>

                                        <th>
                                            Nom & Prénoms
                                        </th>

                                        <th>
                                            Téléphone
                                        </th>

                                        <th>
                                            Email
                                        </th>                               
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $users as $proprio )
                                        <tr >
                                            <td>
                                                 {{ $proprio->id ?? '' }}
                                            </td>
                                            <td>
                                                {{ $proprio->nom ?? ''}} {{ $proprio->prenom ?? ''}}  
                                                
                                            </td>

                                            <td>
                                                {{ $proprio->phone ?? ''}}  
                                                
                                            </td> 

                                            <td>
                                                {{ $proprio->email ?? ''}}
                                            </td>

                                            <td>
                                                <a href="{{ route('admins_write_message', $proprio->id ) }}" class="ui button primary">Envoyez un message par mail</a>
                                            </td>

                                            <td>
                                                <a href="https://wa.me/{{ $proprio->phone ?? ''}}" class="ui button green" target="_blank">Envoyez un message par Whatsapp</a>
                                            </td>

                                            <td>
                                                <a href="tel: 00229{{ $proprio->phone ?? ''}}" class="ui button teal" >Appelez directement</a>
                                            </td>
                                 
                                        </tr>
                                      
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>

@stop
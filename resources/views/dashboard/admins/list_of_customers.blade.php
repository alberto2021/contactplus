@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
    @if(session()->has('message'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('message') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Liste des utilisateurs</h1>
          </div><!-- /.col -->
          <div class="col-sm-6" >

            <form action="{{ route('admins_search_customers') }}" method="post">
                @csrf
                <div class="">
                    <div class="input-group" >
                      <select type="search" class="custom-select custom-select-md @error('search') is-invalid @enderror" id="search" name="search" value="{{ old('search') }}">
                        <option value="">--Veuillez choisir une catégorie--</option>
                        
                         <option value="gratuit">Gratuit</option>
                         <option value="basic">Module 2/ 3000f</option>
                         <option value="standard">Module 3/ 7000f</option>
                         <option value="premium">Module 4/ 12000f</option>
                         <option value="pro">Module 5/ 20000f</option>
                         <option value="all">Tous</option>
                          
                      </select>  

                      @error('search')

                          <div class="" style="color: red;">{{ $message }}</div>

                      @enderror
                      <div class="">
                        <button class="btn btn-sidebar" type="submit">
                          <i class="fas fa-search fa-fw"></i>
                        </button>
                      </div>
                    </div>
                </div>
            </form>

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <hr>

@if($number == 5)
    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    <div class="card-header" >
                        <h1 class="font-weight-bold" style="font-size: 20px;">Catégorie: Tous les utilisateurs</h1>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Identifiant
                                        </th>

                                        <th>
                                            Nom & Prénoms
                                        </th>

                                        <th>
                                            Téléphone
                                        </th>

                                        <th>
                                            Email
                                        </th>                               

                                        <th>
                                            Profession
                                        </th>

                                        <th>
                                            Adresse
                                        </th>
                                        
                                        <th>
                                            Description
                                        </th>

                                        <th>
                                            Mode
                                        </th>

                                        <th>
                                            Date de création du compte 
                                        </th>                                  
                                        
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $customers as $proprio )
                                        <tr >
                                            <td>
                                                 {{ $proprio->id ?? '' }}
                                            </td>
                                            <td>
                                                {{ $proprio->nom ?? ''}} {{ $proprio->prenom ?? ''}}  
                                                
                                            </td>

                                            <td>
                                                {{ $proprio->phone ?? ''}}  
                                                
                                            </td> 

                                            <td>
                                                {{ $proprio->email ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->profession ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->adresse ?? ''}}
                                            </td>
                                            
                                            <td>
                                                {{ $proprio->description_of_you ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->mode ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->created_at ?? ''}}
                                            </td>
                                            
                                            @if (auth()->guard('admin')->user()->is_super_admin)
                                                <td>
                                                    <a class="ui button red" href="{{ route('admins_delete_user', $proprio->id) }}">Supprimer</a>
                                                </td>
                                            @endif
                                 
                                        </tr>
                                      
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>


@else

<div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    <div class="card-header" >
                        <h1 class="font-weight-bold" style="font-size: 20px;">Catégorie: 
                        @if($what == "gratuit")
                            Gratuit
                        @elseif($what == "basic")
                            Module 1
                        @elseif($what == "standard")
                            Module 2
                        @elseif($what == "premium")
                            Module 3
                        @elseif($what == "pro")
                            Module 4
                        @endif 
                        </h1>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Identifiant
                                        </th>

                                        <th>
                                            Nom & Prénoms
                                        </th>

                                        <th>
                                            Téléphone
                                        </th>

                                        <th>
                                            Email
                                        </th>                               

                                        <th>
                                            Profession
                                        </th>

                                        <th>
                                            Adresse
                                        </th>
                                        
                                        <th>
                                            Description
                                        </th>

                                        <th>
                                            Mode
                                        </th>

                                        <th>
                                            Date de création du compte 
                                        </th>                                  
                                        
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $customers as $proprio )
                                        <tr >
                                            <td>
                                                 {{ $proprio->id ?? '' }}
                                            </td>
                                            <td>
                                                {{ $proprio->nom ?? ''}} {{ $proprio->prenom ?? ''}}  
                                                
                                            </td>

                                            <td>
                                                {{ $proprio->phone ?? ''}}  
                                                
                                            </td> 

                                            <td>
                                                {{ $proprio->email ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->profession ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->adresse ?? ''}}
                                            </td>
                                            
                                            <td>
                                                {{ $proprio->description_of_you ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->mode ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->created_at ?? ''}}
                                            </td>
                                            
                                            @if (auth()->guard('admin')->user()->is_super_admin)
                                                <td>
                                                    <a class="ui button red" href="{{ route('admins_delete_user', $proprio->id) }}">Supprimer</a>
                                                </td>
                                            @endif
                                 
                                        </tr>
                                      
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@endif
@stop
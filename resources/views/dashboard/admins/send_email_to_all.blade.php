@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#message').summernote({
      placeholder: 'Saisissez le contenu de la publication ici',
        tabsize: 2,
        height: 150,
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', ]],
          ['view', ['fullscreen', 'codeview', 'help']],
          ['fontname', ['fontname']],
        ],
        popover: {
          image: [
            ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
            ['float', ['floatLeft', 'floatRight', 'floatNone']],
            ['remove', ['removeMedia']]
          ],
          link: [
            ['link', ['linkDialogShow', 'unlink']]
          ],
          table: [
            ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
            ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
          ],
          air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']],
            ['para', ['ul', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture']]
          ]
        }


    });
  });
</script>
@stop

@section('content')
<div class="content-wrapper">
  	@if(session()->has('contact'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('contact') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Message par E-mail à tous les utilisateurs</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire d'envoi</h2>
                        @if(session()->has('mailok'))
                          <div class="ui success message">
                              {{ session()->get('mailok') }}
                          </div>
                        @endif
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-2 offset-lg-2 col-lg-8 col-md-8 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('admins_send_email_to_all_form') }}" method="post">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="objet">Saisissez l'objet de votre message</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="objet" name="objet" class="form-control @error('objet') is-invalid @enderror" value="{{ old('objet') }}" />

                                @error('objet')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>
                        <div class="row mb-4">

                          <div class="col" style="text-align: left;">
                              
                              <label class="form-label" for="message">Saisissez le contenu du mail</label>

                              <div style="margin-bottom: 10px;"></div>

                              <textarea name="message" id="message"  style="" rows="6" cols="30" class="form-control @error('message') is-invalid @enderror"></textarea>

                              @error('message')

                                  <div class="" style="color: red;">{{ $message }}</div>

                              @enderror
                              
                          </div>
  
                        </div>


                        <button type="submit" class="btn btn-outline-primary btn-rounded">Envoyer</button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
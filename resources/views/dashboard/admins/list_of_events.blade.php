@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Liste de tous les évènements</h1>
          </div><!-- /.col -->
          <div class="col-sm-6" >
            @if(session()->has('message'))
                  <div class="ui message success">
                      {{ session()->get('message') }}
                  </div> 
            @endif
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <hr>

    <!-- Main content -->
    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Title
                                        </th>

                                        <th>
                                            Contenu
                                        </th>

                                        <th>
                                           Durée de l'évènement
                                        </th>                               

                                        <th>
                                            Créé le 
                                        </th>                                  
                                        
                                        <th>
                                            Date de désactivation 
                                        </th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $events as $proprio )
                                      
                                        <tr>
                                            <td>
                                                 {{ $proprio->title ?? '' }}
                                            </td>
                                            <td>
                                                {!! $proprio->contenu ?? ''!!}  
                                                
                                            </td>
                                            <td>
                                                {{ $proprio->duree ?? ''}} jour(s)
                                            </td>
                                            <td>
                                                {{ $proprio->created_at ?? ''}}
                                            </td>

                                            <td>
                                                {{ $proprio->date_end ?? ''}}
                                            </td> 

                                            @if (auth()->guard('admin')->user()->is_super_admin) 
                                                <td>
                                                    <a href="{{ route('admins_delete_event', $proprio->id) }}" class="ui button red">Supprimer</a>
                                                </td>   
                                            @endif          
                                        </tr>
                                      
                                        
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@stop
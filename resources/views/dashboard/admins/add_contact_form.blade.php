@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
  	@if(session()->has('contact'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('contact') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Ajouter des contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire d'ajout de contact</h2>
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('admins_add_contact_with_form_form') }}" method="post">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="nom">Entrez le nom du contact</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="nom" name="nom" class="form-control @error('nom') is-invalid @enderror" value="{{ old('nom') }}" />

                                @error('nom')

                                    <div class="alert alert-danger">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="phone">Entrez le numéro de téléphone du contact</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}" />
                                <small>Ex:+22990000000</small>

                                @error('phone')

                                    <div class="alert alert-danger">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        <div class="col-md-12 col-sm-12 mb-4" style="text-align: left;">
                                        
                              <label class="form-label" for="wanted">Catégorie de contact</label>

                              <div style="margin-bottom: 10px;"></div>

                              <select class="custom-select custom-select-md @error('wanted') is-invalid @enderror" id="wanted" name="wanted" value="{{ old('wanted') }}">
                                <option value="">--Veuillez choisir une catégorie--</option>
                                
                                 <option value="gratuit">Gratuit</option>
                                 <option value="module 2">Module 2/ 3000f</option>
                                 <option value="module 3">Module 3/ 7000f</option>
                                 <option value="module 4">Module 4/ 12000f</option>
                                 <option value="module 5">Module 5/ 20000f</option>
                                  
                              </select>
                              

                              @error('wanted')

                                  <div class="" style="color: red;">{{ $message }}</div>

                              @enderror
                              
                        </div>

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Enregistrer </button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
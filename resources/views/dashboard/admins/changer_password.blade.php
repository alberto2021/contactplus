@extends('templates.dashboard.admins.master')

@section('title')
Tableau de bord | Administration
@stop

@section('content')
<div class="content-wrapper">
 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Changement du mot de passe</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <hr>
    <!-- Main content -->
    <div class="content mt-6">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('changer_Password') }}" method="post">
                        @csrf
                        <div class="row mb-4">
                            <div class="col" style="text-align: left;">
                                <label class="form-label" for="ancien_password">Ancien mot de passe</label>
                                <div style="margin-bottom: 10px;"></div>
                                <input type="password" id="ancien_password" name="ancien_password" 
                                class="form-control @error('ancien_password') is-invalid @enderror" required/>

                                @error('ancien_password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col" style="text-align: left;">
                                <label class="form-label" for="nouveau_password">Votre nouveau mot de passe</label>
                                <div style="margin-bottom: 10px;"></div>
                                <input type="password" id="nouveau_password" name="nouveau_password"
                                 class="form-control @error('nouveau_password') is-invalid @enderror" required/>
                                @error('nouveau_password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col" style="text-align: left;">
                                <label class="form-label" for="confirme_password">Confirmez votre nouveau mot de passe</label>
                                <div style="margin-bottom: 10px;"></div>
                                <input type="password" id="confirme_password" name="confirme_password" 
                                class="form-control @error('confirme_password') is-invalid @enderror" required />

                                @error('confirme_password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-primary btn-rounded">Enregistrer</button>
                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">Administration | CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>
@stop
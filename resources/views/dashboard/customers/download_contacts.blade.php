@extends('templates.dashboard.customers.master')

@section('title')
Télécharger des contacts
@stop

@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Télécharger des contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
          
          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 1: Télécharger 2000 contacts</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 2000 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_telecharger_contacts_option', 1) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>

            <div class="card card-danger card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 2: Télécharger 3000 contacts</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 3000 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_telecharger_contacts_option', 2) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card card-info card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 3: Télécharger 3500 contacts</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 3500 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_telecharger_contacts_option', 3) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>

            <div class="card card-warning card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 4: Télécharger 4000 contacts</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 4000 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_telecharger_contacts_option', 4) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>
          </div>
          
          
          
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
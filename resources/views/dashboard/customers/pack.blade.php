@extends('templates.dashboard.customers.master')

@section('title')
Activer un pack
@stop


@section('js')

  <script amount="{{$prix ?? ''}}" 
        callback="{{ env('APP_USER_TRANSACTION') }}/transaction_to_active_pack"
        data=""
        url="{{ route('welcome') }}"
        position="center" 
        theme="#9e825a"
        key="{{ env('KKIAPAY_PUBLIC_KEY') }}"
        src="https://cdn.kkiapay.me/k.js"></script>
@stop

@section('content')

@if(session()->has('type'))
  <div class="content-wrapper">

      @if( session()->get('type') == "basic")
      
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Pack 1</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>


        <div class="content mt-4">
          <div class="container-fluid">
            <div class="row">
              <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6">
                <div class="card">

                  <div class="card-header  py-3">
                    <p class="text-uppercase  mb-2"><strong>Détails sur le pack</strong> | 5000F CFA/mois</p>
                    
                  </div>

                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item" style="color: #9e825a;">2000 Contacts à l'inscription.</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster une annonce ( Durée :48 heures )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster une affiche ( Durée :48 heures )</li>
                    </ul>
                  </div>

                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>

                </div>

                
              </div>
            </div>
          </div>
        </div>

      @elseif ( session()->get('type') == "standard")

        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Pack 2</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>


        <div class="content mt-4">
          <div class="container-fluid">
            <div class="row">
              <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6">
                <div class="card">

                  <div class="card-header  py-3">
                    <p class="text-uppercase  mb-2"><strong>Détails sur le pack</strong> | 10000F CFA/mois</p>
                    
                  </div>

                  <div class="card-body">
                   <ul class="list-group list-group-flush">
                  <li class="list-group-item" style="color: #9e825a;">3000 Contacts à l'inscription.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster deux annonces ( Durée :4 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster deux affiches ( Durée :2 jours )</li>
                </ul>
                  </div>

                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>

                </div>

                
              </div>
            </div>
          </div>
        </div>
      @elseif ( session()->get('type') == "premium")

        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Pack 3</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>


        <div class="content mt-4">
          <div class="container-fluid">
            <div class="row">
              <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6">
                <div class="card">

                  <div class="card-header  py-3">
                    <p class="text-uppercase  mb-2"><strong>Détails sur le pack</strong> | 15000F CFA/mois</p>
                    
                  </div>

                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item" style="color: #9e825a;">3500 Contacts à l'inscription.</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster 5 annonces ( Durée :5 jours )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster 2 affiches ( Durée :5 jours )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par whatsapp (inbox)</li>
                       <li class="list-group-item" style="color: #9e825a;">Réalisation d'une vidéo présentative</li>
                    </ul>
                  </div>

                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>

                </div>

                
              </div>
            </div>
          </div>
        </div>

        @elseif ( session()->get('type') == "pro")

        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Pack 4</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>


        <div class="content mt-4">
          <div class="container-fluid">
            <div class="row">
              <div class="offset-md-3 offset-lg-3 col-lg-6 col-md-6">
                <div class="card">

                  <div class="card-header  py-3">
                    <p class="text-uppercase  mb-2"><strong>Détails sur le pack</strong> | 25000F CFA/mois</p>
                    
                  </div>

                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                  <li class="list-group-item" style="color: #9e825a;">4000 Contacts à l'inscription.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster 5 annonces ( Durée :7 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster 2 affiches ( Durée :7 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par whatsapp (inbox)</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par E-mai)</li>
                   <li class="list-group-item" style="color: #9e825a;">Réalisation d'une vidéo présentative</li>
                </ul>
                  </div>

                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>

                </div>

                
              </div>
            </div>
          </div>
        </div>
      @endif

  </div>


@else



<div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Activer le pack de votre choix</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content mt-2">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 mb-4">
            <div class="card">

              <div class="card-header  py-3">
                <p class="text-uppercase  mb-2"><strong>Pack 1</strong> | 5000F CFA/mois</p>
                
              </div>

              <div class="card-body">
                <ul class="list-group list-group-flush">
                      <li class="list-group-item" style="color: #9e825a;">2000 Contacts à l'inscription.</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster une annonce ( Durée :48 heures )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster une affiche ( Durée :48 heures )</li>
               </ul>
              </div>

              <div class="card-footer  py-3">
                <a href="{{ route('customers_active_pack_specific', 1) }}" class="btn btn-info btn-rounded">Activer un pack</a>
              </div>

            </div>

            
          </div>


          <div class="col-lg-6 mb-4">
            <div class="card">

              <div class="card-header  py-3">
                <p class="text-uppercase  mb-2"><strong>Pack 2</strong> | 10000F CFA/mois</p>
                
              </div>

              <div class="card-body">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item" style="color: #9e825a;">3000 Contacts à l'inscription.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster deux annonces ( Durée :4 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster deux affiches ( Durée :2 jours )</li>
                </ul>
              </div>

              <div class="card-footer  py-3">
                <a href="{{ route('customers_active_pack_specific', 2) }}" class="btn btn-success btn-rounded">Activer un pack</a>
              </div>

            </div>

            
          </div>


          <div class="col-lg-6">
            <div class="card">

              <div class="card-header  py-3">
                <p class="text-uppercase  mb-2"><strong>Pack 3</strong> | 15000F CFA/mois</p>
                
              </div>

              <div class="card-body">
                <ul class="list-group list-group-flush">
                      <li class="list-group-item" style="color: #9e825a;">3500 Contacts à l'inscription.</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster 5 annonces ( Durée :5 jours )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Poster 2 affiches ( Durée :5 jours )</li>
                      <hr>
                      <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par whatsapp (inbox)</li>
                       <li class="list-group-item" style="color: #9e825a;">Réalisation d'une vidéo présentative</li>
                    </ul>
              </div>

              <div class="card-footer  py-3">
                <a href="{{ route('customers_active_pack_specific', 3) }}" class="btn btn-success btn-rounded">Activer un pack</a>
              </div>

            </div>

            
          </div>

          <div class="col-lg-6">
            <div class="card">

              <div class="card-header  py-3">
                <p class="text-uppercase  mb-2"><strong>Pack 4</strong> | 25000F CFA/mois</p>
                
              </div>

              <div class="card-body">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item" style="color: #9e825a;">4000 Contacts à l'inscription.</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">25 Contacts tous les 07 jours</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster 5 annonces ( Durée :7 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Poster 2 affiches ( Durée :7 jours )</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par whatsapp (inbox)</li>
                  <hr>
                  <li class="list-group-item" style="color: #9e825a;">Publication d'une annonce suivie d'une affiche à tous les inscrits du site directement par E-mai)</li>
                   <li class="list-group-item" style="color: #9e825a;">Réalisation d'une vidéo présentative</li>
                </ul>
              </div>

              <div class="card-footer  py-3">
                <a href="{{ route('customers_active_pack_specific', 4) }}" class="btn btn-success btn-rounded">Activer un pack</a>
              </div>

            </div>

            
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->


      
    </div>
    <!-- /.content -->

    
</div>
@endif
@stop
@extends('templates.dashboard.customers.master')

@section('title')
Mon profil
@stop

@section('content')

<div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Mon profil</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Informations de l'utilisateur</h2>
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="col-lg-2 col-md-2 mb-4">
                    <img class="w-24 h-24 rounded-full mb-4" src="/assets/img/connection.jpg" alt="" width="384" height="512">
                    <a href="" class="pl-3 " style="color: #229ddc; text-decoration: underline;">Modifier</a>
                  </div>

                  <div class="col-lg-5 col-md-5 col-sm-6">
                    <p class="card-text" style="color: #9e825a;">Noms & Prénoms &nbsp;</p>{{ auth()->user()->nom }} {{ auth()->user()->prenom }}


                    <p class="card-text pt-2" style="color: #9e825a;">E-mail&nbsp;</p>{{ auth()->user()->email }} 

                    <p class="card-text pt-2" style="color: #9e825a;">Téléphone &nbsp;</p>{{ auth()->user()->phone }} 

                    <p class="card-text pt-2" style="color: #9e825a;">Profession&nbsp;</p>{{ auth()->user()->profession }} | <a href="{{ route('customers_modify_profil') }}" style="color: #229ddc; text-decoration: underline;">Modifier mes informations</a>

                  </div>

                  <div class="col-lg-5 col-md-5 mb-4">
                    @if (auth()->user()->mode == "gratuit")
                      <a class="nav-link" href="#">
                        <span class=" animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode gratuit</span>
                      </a>
                    @elseif (auth()->user()->mode == "basic")
                      <a class="nav-link " href="#">
                        <span class=" animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode basic</span>
                      </a>
                    @elseif (auth()->user()->mode == "standard")
                      <a class="nav-link " href="#" >
                        <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px">Mode standard</span>
                      </a>
                    @elseif (auth()->user()->mode == "premium")
                      <a class="nav-link " href="#">
                        <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode premium</span>
                      </a>
                    @endif

                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Informations sur l'abonnement actif</h2>
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="col-lg-2 col-md-2 mb-4">
                    
                  </div>

                  <div class="col-lg-5 col-md-5 col-sm-6">
                    <p class="card-text" style="color: #9e825a;">Date d'activation de l'abonnement &nbsp;</p>{{ auth()->user()->date_start }} 


                    <p class="card-text pt-2" style="color: #9e825a;">Date de fin de l'abonnement&nbsp;</p>{{ auth()->user()->date_end }}

                    <p class="card-text pt-2" style="color: #9e825a;">Mode actif&nbsp;</p>{{ auth()->user()->mode }} 

                    <p class="card-text pt-2" style="color: #9e825a;">&nbsp;</p> <a href="{{ route('customers_abonns') }}" style="color: #229ddc; text-decoration: underline;">Voir mes abonnements</a>

                  </div>

                  <div class="col-lg-5 col-md-5 mb-4">
                    @if (auth()->user()->mode == "gratuit")
                      <a class="nav-link" href="#">
                        <span class=" animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode gratuit</span>
                      </a>
                    @elseif (auth()->user()->mode == "basic")
                      <a class="nav-link " href="#">
                        <span class=" animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode basic</span>
                      </a>
                    @elseif (auth()->user()->mode == "standard")
                      <a class="nav-link " href="#" >
                        <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px">Mode standard</span>
                      </a>
                    @elseif (auth()->user()->mode == "premium")
                      <a class="nav-link " href="#">
                        <span class="animate-bounce badge badge-info navbar-badge pt-2 pb-4 pl-2 pr-2" style="height: 20px; font-size: 13px" >Mode premium</span>
                      </a>
                    @endif

                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a">CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@stop
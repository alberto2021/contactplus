@extends('templates.dashboard.customers.master')

@section('title')
Télécharger des contacts
@stop


@section('js')

  <script amount="1000" 
        callback="{{ route('customers_paiement_users_contacts_transaction') }}"
        data=""
        url="{{ route('welcome') }}"
        position="center" 
        theme="#9e825a"
        key="{{ env('KKIAPAY_PUBLIC_KEY') }}"
        src="https://cdn.kkiapay.me/k.js"></script>
@stop


@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Télécharger des contacts utilisateurs</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
          <div class="col-lg-3"></div>
          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Télécharger des contacts utilisateurs</h3>
              </div>
              <div class="card-body">
                  @if(auth()->user()->has_download != "yes")
                   
                    <p class="card-text">Chers utilisateurs, après téléchargement de ces contacts, l’enregistrement est automatique dans les répertoires et les vues sur statut augmentent instantanément.
Le nombre de contacts varies tout les 7 jours en fonction des nouveaux venus. Nombre de nouveaux = nombre de contacts à télécharger<br><br>
<h6>Prix: <strong>1000 FCFA</strong></h6><br>
<strong>NB:</strong>Ce paiement est unique et la fonctionalité est à vie   

</p>
                    <br>
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  @else
                   Vous êtes déjà un autorisé pour télécharger les contacts. Télécharger autant que vous pouvez. Merci!!!
                  @endif
              </div>
            </div>

            
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
@extends('templates.dashboard.customers.master')

@section('title')
Paiement pour option choisir
@stop

@section('js')

  <script amount="{{ $prix ?? '' }}" 
        callback="{{ route('customers_activate_service_publication_transaction') }}"
        data=""
        url="{{ route('welcome') }}"
        position="center" 
        theme="#9e825a"
        key="{{ env('KKIAPAY_PUBLIC_KEY') }}"
        src="https://cdn.kkiapay.me/k.js"></script>
@stop


@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Procédez au paiement pour cette option</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
            
          @if(session()->has('option'))
            @if(session()->get('option') == "5days")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 1: Faire des publications sur une durée de 05 jours</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Prix: 500 F CFA</h6>
    
                    
                    
                  </div>
                  
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                  
                  
                </div>
              </div>
              <div class="col-lg-3"></div>
            @elseif(session()->get('option') == "10days")
            
            <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-danger card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 2: Faire des publications sur une durée de 10 jours</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Prix: 800 F CFA</h6>
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3"></div>
            
            @elseif(session()->get('option') == "15days")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-info card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 3: Faire des publications sur une durée de 15 jours</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Prix: 1350 F CFA</h6>
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-3"></div>
              
            @elseif(session()->get('option') == "1month")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-warning card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 4: Faire des publications sur une durée de 01 mois</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Prix: 2500 F CFA</h6>
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-3"></div>
            
            
            @endif
            
        @endif
          
          
          
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
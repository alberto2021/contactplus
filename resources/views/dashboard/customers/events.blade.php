@extends('templates.dashboard.customers.master')

@section('title')
Evènements à venir
@stop

@section('css')
@stop

@section('js')
  @include('templates.components.js')
@stop

@section('content')
<div class="content-wrapper">
  @if(session()->has('abonnementsuccess'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('abonnementsuccess') }} 
      </div>
  @endif
	@if (auth()->user()->mode == "gratuit")
		<div class="alert alert-info mt-2 ml-4 mr-4 shadow-2xl">
			Cher contact, nous vous remercions pour l'intérêt porté à notre communauté. Nous vous informons qu'actuellement, vous êtes en mode gratuit. Nous serons davantage plus heureux de vous compter parmi nos partenaires d'affaires en activant un pack de votre choix. Veuillez cliquer <a href="{{ route('customers_active_pack') }}" style="color: #9e825a">ici </a>. Merci
		</div>
	@else
	@endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 " style="color: #138496; font-weight: bolder;">Mes évènements à venir</h1>
          </div><!-- /.col -->
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          @if($events->count() == 0)
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h2 class="card-title text-bold" style="color: steelblue;">Pas d'évènements</h2>
                  </div>
                  <div class="card-body text-center text-bold">
                    <p>Pas d'évènements à venir. Nous vous tiendrons informer... Merci!!!</p>
                    
                  </div>
                </div>

                
              </div>
            @endif
          @foreach($events as $publicite)
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header">
                  <h2 class="card-title text-bold" style="color: steelblue;">{{ $publicite->title }}</h2>
                </div>
                <div class="card-body">
                  @if($publicite->image != "")
                    <img src="/uploads/events/{{ $publicite->image }}" style="height: 100%; width: 100%;" class="mb-4">
                  @endif
                    {!! $publicite->contenu !!}
                  
                </div>
              </div>

              
            </div>

            
          @endforeach
          <div class="text-center  mb-6">
            <a href="" class="btn btn-info mt-2" >Nous écrire</a>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

      <hr>

      
    </div>
    <!-- /.content -->

    @if (auth()->user()->mode == "gratuit")
	   
  	@else
  	@endif
</div>
@stop
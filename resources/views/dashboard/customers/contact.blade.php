@extends('templates.dashboard.customers.master')

@section('title')
Mes contacts
@stop

@section('content')
<div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
          
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Mes contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <hr>


    <hr>

    <!-- Main content -->
    <div class="content mt-4">
      <div class="container-fluid">
          <div class="alert alert-info mt-2  shadow-2xl" style="color:white;font-weight:bold">
			<p>Après téléchargement de ces contacts, l’enregistrement n’est pas automatique dans les répertoires et les vues sur statut augmentent instantanément après avoir écrit aux numéros pour qu’il vous enregistre afin que l’enregistrement soit mutuel.
Le nombre de contact est fixe sur 25 tout les 7 jours.
			<br><strong>NB:</strong>C'est gratuit.
			</p>
		</div>
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable"  width="100%">
                                <thead class=" text-info">
                                    <tr>

                                       
                                        <th>
                                            Nom du fichier
                                        </th>

                                        <th>
                                            Nombre de contacts
                                        </th>                               

                                      
                                        
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $contacts as $proprio )
                                      
                                        <tr >
                                           
                                            <td>
                                                {{ $proprio->filename ?? ''}}  
                                                
                                            </td>
                                            <td>
                                                {{ $proprio->number_of_contacts ?? ''}}
                                            </td>
                                           

                                            <td>
                                                <a href="{{ route('customers_download_contacts', $proprio->id) }}" class="ui button success ">Télécharger le fichier</a>
                                            </td>
                                                                           
                                        </tr>
                                     
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@stop
@extends('templates.dashboard.customers.master')

@section('title')
Boîte de messagerie |
@stop

@section('css')
<link href="{{ asset('dist/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop



@section('content')
<div class="content-wrapper">
  @if(session()->has('contact'))
  <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
    {{ session()->get('contact') }}
  </div>
  @endif
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-8">
          <h1 class="m-0">Boîte de messagerie</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">

        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <hr>
  <!-- Main content -->
  <div class="content mt-6">
    <div class="container-fluid">
      <div class="row">

        @if($messages->count() == 0)
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title text-bold" style="color: steelblue;">Pas de messages</h2>
            </div>
            <div class="card-body text-center text-bold">
              <p>Pas de messages aujourd'hui, Nous sommes désolés...</p>

            </div>
          </div>


        </div>
        @endif
        @foreach($messages as $publicite)
        <div class="col-lg-12 mb-4">

          <div class="ui message green">
            {!! $publicite->message !!}
            <small>Envoyé le {{ $publicite->created_at->toDateString() }} à {{ $publicite->created_at->toTimeString() }}</small>
          </div>

        </div>


        @endforeach
      </div>
      <!-- /.row -->
    </div>
  </div>




  <div class="content mt-4">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 ">

          <div class="card">
            <div class="card-header">
              <h1 class="font-weight-bold text-center" style="font-size: 20px;">
                Les commentaires sur vos Annonces
              </h1>
            </div>
            <div class="card-body ">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" cellspacing="0" width="100%">
                  <thead class=" text-info">
                    <tr>
                      <th>N°</th>
                      <th>Titre annonce</th>
                      <th>Commentaire envoyé</th>
                      <th>Date du commentaire</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(isset($comments))
                    @foreach( $comments as $proprio)
                    <tr>
                      <td>{{ $loop->iteration ?? '' }} </td>
                      <td>{{ $proprio->title ?? ''}}</td>
                      <td> {{ $proprio->commenter ?? ''}}.
                        <br />
                        <strong> CONTACTEZ LE VIA </strong>
                        <a href="https://wa.me/{{ $proprio->phone ?? ''}}" class="btn btn-success social-button">
                          <span class="fa fa-whatsapp"></span>
                        </a>
                      </td>
                      <td>{{ $proprio->created_at->format('d/m/Y H:m:s') ?? ''}}</td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>




  <div class="content mt-4">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 ">

          <div class="card">
            <div class="card-header">
              <h1 class="font-weight-bold text-center" style="font-size: 20px;">
                Les commentaires sur vos publications
              </h1>
            </div>
            <div class="card-body ">
              <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" cellspacing="0" width="100%">
                  <thead class=" text-info">
                    <tr>
                      <th>N°</th>
                      <th>Titre publication</th>
                      <th>Commentaire envoyé</th>
                      <th>Date du commentaire</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if(isset($commentsPub))
                    @foreach( $commentsPub as $proprio )
                    <tr>
                      <td>{{ $loop->iteration ?? '' }} </td>
                      <td>{{ $proprio->title ?? ''}}</td>
                      <td> {{ $proprio->commenter ?? ''}}.
                        <strong> CONTACTEZ LE VIA </strong>
                        <a href="https://wa.me/{{ $proprio->phone ?? ''}}" class="btn btn-success social-button">
                          <span class="fa fa-whatsapp"></span>
                        </a>
                      </td>
                      <td>{{ $proprio->created_at->format('d/m/Y H:m:s') ?? ''}}</td>
                    </tr>
                    @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
</div>
@section('js')
<script src="{{ asset('dist/vendor/datatables/jquery.dataTables.js') }}" defer></script>
<script src="{{ asset('dist/vendor/datatables/dataTables.bootstrap4.js') }}" defer></script>
<script src="{{ asset('dist/vendor/datatables-demo.js') }}" defer></script>
@stop
@stop
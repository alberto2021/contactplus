@extends('templates.dashboard.customers.master')

@section('title')
Paiement pour option choisir
@stop

@section('js')

  <script amount="{{ $prix ??  '' }}" 
        callback="{{ route('customers_transaction_download_contacts') }}"
        data=""
        url="{{ route('welcome') }}"
        position="center" 
        theme="#9e825a"
        key="{{ env('KKIAPAY_PUBLIC_KEY') }}"
        src="https://cdn.kkiapay.me/k.js"></script>
@stop


@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Procédez au paiement pour cette option de télécharger {{ session()->get('option') }} contacts</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
            
          @if(session()->has('option'))
            @if(session()->get('option') == "2000")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 1: Télécharger 2000 contacts</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">2000 contacts à télécharger / 2000 F CFA</h6>
    
                    
                    
                  </div>
                  
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                  
                  
                </div>
              </div>
              <div class="col-lg-3"></div>
            @elseif(session()->get('option') == "3000")
            
            <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-danger card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 2: Télécharger 3000 contacts</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">3000 contacts à télécharger / 3000 F CFA</h6>
    
                    
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
            </div>
            <div class="col-lg-3"></div>
            
            @elseif(session()->get('option') == "3500")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-info card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 3: Télécharger 3500 contacts</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">3500 contacts à télécharger / 3500 F CFA</h6>
    
                    
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-3"></div>
              
            @elseif(session()->get('option') == "4000")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-warning card-outline">
                  <div class="card-header">
                    <h3 class="m-0 ui header">Option 4: Télécharger 4000 contacts</h3>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">4000 contacts à télécharger / 4000 F CFA</h6>
    
                    
                    
                  </div>
                  <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
                </div>
              </div>
              <div class="col-lg-3"></div>
            
            
            @endif
            
        @endif
          
          
          
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
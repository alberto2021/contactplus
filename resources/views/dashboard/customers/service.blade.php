@extends('templates.dashboard.customers.master')

@section('title')
Activer un service
@stop



@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Activer des services</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ui grid stackable">
        <div class="row">
            <div class="sixteen wide column">
                <div class="card">
                    <div class="card-header">
                      <h2 class="card-title" style="font-weight:bold; font-size:22px">Votre visibilité commence ici</h2>
                        
                    </div>
                    
                    
                    <div class="card-body table-responsive p-0">
                        <table class="table table-success">
                            <thead>
                                <tr>
                                    
                                    <th></th>
                                    <th></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                
                                <tr>
                                    <td>Poster une annonce, une vidéo, une affiche</td>
                                    <td> <a class="ui teal button" href="{{ route('customers_activate_service_publication') }}">Activer</a> </td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Télécharger des contacts</td>
                                    <td><a class="ui teal button" href="{{ route('customers_telecharger_contacts') }}">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Commander une affiche | 4000f/</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Commander un spot(vidéo) | 10.000f/20.000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                <tr>
                                    <td>Profiter des bars d'informations sur le site | 200f/jour</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Profiter des bars de notifications sur le site | 300f/jour</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                
                                <tr>
                                    <td>Envoie d’une affiche annonce à tous les adhérents par WhatsApp | 3000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Envoie d’une affiche annonce à tous les adhérents par WhatsApp et email | 4000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                
                                <tr>
                                    <td>Profiter d’une promotion d'un ou de  tous vos produits ou services pendant 1 mois</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          
          
        </div>
        
      </div>
    </div>


   
</div>
@stop
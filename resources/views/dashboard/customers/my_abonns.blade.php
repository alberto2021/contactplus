@extends('templates.dashboard.customers.master')

@section('title')
Mes abonnements
@stop

@section('content')
<div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Liste des abonnements</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <hr>


    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 ">
        
                <div class="card shadow-2xl">
                    
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="datatable" cellspacing="0" width="100%">
                                <thead class=" text-info">
                                    <tr>

                                        <th>
                                            Type d'abonnement
                                        </th>

                                        <th>
                                            Date d'activation 
                                        </th>
                                        <th>
                                            Date de fin
                                        </th>
                                        
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($abonns as $abonn)
                                        <tr>
                                            <td>
                                                 {{ $abonn->type ?? '' }}
                                            </td>
                                            <td>
                                                {{ $abonn->date_start ?? '' }} 
                                                
                                            </td>
                                            <td>
                                                {{ $abonn->date_end ?? '' }}
                                            </td>
                                 
                                        </tr>
                                    @endforeach
                                    
                                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
@stop
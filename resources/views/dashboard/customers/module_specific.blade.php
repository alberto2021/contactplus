@extends('templates.dashboard.customers.master')

@section('title')
Activer un module spécifique
@stop

@section('js')

  <script amount="{{ $prix ?? '' }}" 
        callback="{{ route('customers_activate_module_transaction') }}"
        data=""
        url="{{ route('welcome') }}"
        position="center" 
        theme="#9e825a"
        key="{{ env('KKIAPAY_PUBLIC_KEY') }}"
        src="https://cdn.kkiapay.me/k.js"></script>
@stop


@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Activer un module spécifique</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
            
          @if(session()->has('type'))
            @if(session()->get('type') == "promotion")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Module 1: Promotion  | 7 000<sup>F CFA</sup> </h3>
              </div>
              <div class="card-body">
                <h4 class="card-title ui header" style="text-decoration: underline">Avantages du module</h4>
                
                <p class="card-text">-Publication dans 200 groupes Facebook</p>
                <p class="card-text">-Publication dans 5000 groupes Whatsapp</p>
                <p class="card-text">-Publication dans Groupes Whatsapp du CCS+ (38 Groupes)</p>
                <p class="card-text">-Publication dans Groupes Whatsapp du CCS</p>
                <p class="card-text">-Publication dans Groupes Facebook du CCS</p>
                <p class="card-text">-Publication sur la Page CCS Production</p>
                <p class="card-text">-Publication sur la Page Cercle du Commerce Social</p>
                <p class="card-text">-Accès au Site CCS Contact+ pendant 5 Jours (Faites par nous et disponible à la demande)</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 05 Jours</p>
                
                
                
              </div>
              <div class="card-footer  text-center py-3">
                    <button  class="btn btn-primary btn-rounded kkiapay-button">Procédez au paiement</a>
              </div>
            </div>
              </div>
              <div class="col-lg-3"></div>
            @elseif(session()->get('type') == "communication")
            
            <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-danger card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Module 2: Communication | 12 000<sup>F CFA</sup></h3>
              </div>
              <div class="card-body">
                <h4 class="card-title ui header" style="text-decoration: underline">Avantages du module | (Module 1 au complet)</h4>
                
                <p class="card-text">-Logo (Facultatif)</p>
                <p class="card-text">-02 Affiches numériques</p>
                <p class="card-text">-Accès au Site CCS Contact+</p>
                <p class="card-text">-Création et maintient d'audience (Facebook)</p>
                <p class="card-text">-Résultat disponible à la demande</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 10 Jours</p>
                
              </div>
              <div class="card-footer  text-center py-3">
                    <button  class="btn btn-danger btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
            </div>
            </div>
            <div class="col-lg-3"></div>
            
            @elseif(session()->get('type') == "communicationplus")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-info card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Module 3: Communication+ | 25 000<sup>F CFA</sup></h3>
              </div>
              <div class="card-body">
                <h4 class="card-title ui header" style="text-decoration: underline">Avantages du module | (Module 1 au complet)</h4>
                
                <p class="card-text">-Une Consultation de votre structure (Stratégie communicationnelle)</p>
                <p class="card-text">-03 Affiches Numériques</p>
                <p class="card-text">-01 Spot Publicitaire (Facultatif)</p>
                <p class="card-text">-01 Vidéo réalisée par des acteurs comédiens</p>
                <p class="card-text">-Création et maintient d'audience (Facebook)</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Annonce en Inbox aux membres du Site</p>
                <p class="card-text">-Conception et Impression de 30 Cartes de visites.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 15 Jours</p>
                
              </div>
              <div class="card-footer  text-center py-3">
                    <button  class="btn btn-info btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
            </div>
              </div>
              <div class="col-lg-3"></div>
              
            @elseif(session()->get('type') == "entreprise")
              <div class="col-lg-3"></div>
              <div class="col-lg-6">
                <div class="card card-warning card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Module 4: Mon entreprise | 80 000<sup>F CFA</sup></h3>
              </div>
              <div class="card-body">
                <h4 class="card-title ui header" style="text-decoration: underline">Avantages du module</h4>
                
                <p class="card-text">-Une Consultation de votre structure (Stratégie communicationnelle)</p>
                <p class="card-text">-04 Affiches Numériques</p>
                <p class="card-text">-02 Spots Publicitaires/Une Vidéo réalisée par les acteurs Comédiens.</p>
                <p class="card-text">-01 Vidéo explicative de votre structure.</p>
                <p class="card-text">-Création et maintient d'audience (Facebook)</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Annonce en Inbox WhatsApp et Mail aux membres du Site.</p>
                <p class="card-text">-Conception de dépliant.</p>
                <p class="card-text">-Conception de Carte de Visite.</p>
                <p class="card-text">-Publication dans les canaux.</p>
                <p class="card-text">-Déploiement des agents.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 01 mois</p>
              </div>
              <div class="card-footer  text-center py-3">
                    <button  class="btn btn-warning btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
            </div>
              </div>
              <div class="col-lg-3"></div>
            
            @elseif(session()->get('type') == "partenariat")
          
            <div class="col-lg-3"></div>
              <div class="col-lg-12">
                <div class="card card-success card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header" >Module 5: Partenariat | 110 000<sup>F CFA</sup></h3>
              </div>
              <div class="card-body">
                <h4 class="card-title ui header" style="text-decoration: underline">Avantages du module</h4>
                
                <p class="card-text">-Stratégie communicationnelle</p>
                <p class="card-text">-Suivie communicationnelle</p>
                <p class="card-text">-Réalisation adéquate</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Publicité.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 03 mois</p>
              </div>
              <div class="card-footer  text-center py-3">
                    <button  class="btn btn-success btn-rounded kkiapay-button">Procédez au paiement</a>
                  </div>
            </div>
              </div>
            <div class="col-lg-3"></div>
            
            @endif
            
        @endif
          
          
          
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
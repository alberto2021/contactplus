@extends('templates.dashboard.customers.master')

@section('title')
Tableau de bord
@stop

@section('css')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@stop

@section('js')
@include('templates.components.js')
@stop

@section('content')
<div class="content-wrapper">
  @if(session()->has('abonnementsuccess'))
  <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
    {{ session()->get('abonnementsuccess') }}
  </div>
  @endif
  @if (auth()->user()->mode == "gratuit")
  <div class="alert alert-info mt-2 ml-4 mr-4 shadow-2xl">
    Cher contact, nous vous remercions pour l'intérêt porté à notre communauté.
    Nous serons davantage plus heureux de vous compter parmi nos partenaires
    d'affaires en activant un module de votre choix. Merci
  </div>
  @endif

  @if(Session::has('response'))
  <div class="mt-2 ml-5 mr-5">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>{{session('response')}}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
  @endif
  @if(Session::has('error'))
  <div class="mt-2 ml-5 mr-5">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>{{session('error')}}</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
  @endif


  @if (auth()->user()->has_module == "gratuit")
  @endif
 

  <!-- Content Header (Page header) -->
  @if(count($publicites) > 0)
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2 text-center">
        <div class="col-sm-12">
          <h1 class="m-0 " style="color: #138496; font-weight: bolder;">Annonces</h1>
        </div><!-- /.col -->
        <!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  @endif


  <div class="content">
    <div class="container-fluid">
      <div class="row">

        <!-- @if(count($publicites) == 0)
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title text-bold" style="color: steelblue;">Pas de publicités</h2>
            </div>
            <div class="card-body text-center text-bold">
              <p>Pas de publicités aujourd'hui, Nous sommes désolés...</p>
            </div>
          </div>
        </div>
        @endif -->

        @foreach($publicites ?? '' as $publicite)
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h2 class="card-title text-bold" style="color: steelblue;">{{ $publicite->title }}</h2>
            </div>
            <div class="card-body">
              {!! $publicite->contenu !!}

              <div>
                <div class="btn-group me-2" role="group" aria-label="First group">
                  <form method="POST" action="{{ route('LikePub') }}">
                    @csrf
                    <input type="hidden" id="id_pub" name="id_pub" value="{{ $publicite->id}}" />
                    <button type="submit" style="min-width: 65px; max-width: 65px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-primary shadow"><i class="fa fa-star"></i></button>
                    <!-- <a type="submit" class="btn btn-outline-primary shadow" id=""><span class="fa fa-star"></span></a> -->

                  </form>
                  @if($publicite->compteur_like == 0)
                  <button class="btn btn-outline-primary" id="nbr_likes">0</button>
                  @else
                  <button class="btn btn-outline-primary" id="nbr_likes">{!! $publicite->compteur_like !!}</button>
                  @endif
                </div>

                <div class="btn-group me-2" role="group" aria-label="First group">
                  <button type="button" style="min-width: 100px; max-width: 100px; min-height: 35px;  max-height: 35px;" data-bs-toggle="modal" data-bs-target="#staticBackdrop{{ $publicite->id}}" class="btn btn-outline-primary shadow"><i class="fa fa-comments"></i>
                  </button>
                  @if($publicite->compteur_commenter == 0)
                  <button type="button" class="btn btn-outline-primary">0</button>
                  @else
                  <button type="button" class="btn btn-outline-primary">{!! $publicite->compteur_commenter !!}</button>
                  @endif
                </div>


                <!-- <span class="fa-solid fa-share-nodes"></span>  -->

                <!-- LOCAL LINKS -->
                <!-- <a href="https://wa.me/?text=http://127.0.0.1:8000/customers/publie/{{ $publicite->id}}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=http://127.0.0.1:8000/customers/publie/{{ $publicite->id }}" class="social-button " id=""><span class="fa fa-facebook-official"></span></a>
                <a href="https://twitter.com/intent/tweet?text={{ $publicite->title }}&amp;url=http://127.0.0.1:8000/customers/publie/{{ $publicite->id }}" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://127.0.0.1:8000/customers/publie/{{ $publicite->id }}&amp;title={{ $publicite->title }}&amp;summary=publication sur linkedin" class="social-button " id=""><span class="fa fa-linkedin"></span></a> -->

                <!-- ONLINE  LINKS -->

                <a href="https://wa.me/?text=https://contactplus.cercle-cs.com/customers/publie/{{ $publicite->id}}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=https://contactplus.cercle-cs.com/customers/publie/{{ $publicite->id }}"  class="social-button " id=""><span class="fa fa-facebook-official"></span></a>
                <a href="https://twitter.com/intent/tweet?text={{ $publicite->title }}&amp;url=https://contactplus.cercle-cs.com/customers/publie/{{ $publicite->id }}" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://contactplus.cercle-cs.com/customers/publie/{{ $publicite->id }}&amp;title={{ $publicite->title }}&amp;summary=publication sur linkedin" class="social-button " id=""><span class="fa fa-linkedin"></span></a>






                <!-- <div id="social-links">
                <ul>
                  <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://jorenvanhocht.be" class="social-button " id=""><span class="fa fa-facebook-official"></span></a></li>
                  <li><a href="https://twitter.com/intent/tweet?text=my share text&amp;url=http://jorenvanhocht.be" class="social-button " id=""><span class="fa fa-twitter"></span></a></li>
                  <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://jorenvanhocht.be&amp;title=my share text&amp;summary=dit is de linkedin summary" class="social-button " id=""><span class="fa fa-linkedin"></span></a></li>
                  <li><a href="https://wa.me/?text=http://uploads/videos/dashboard/{{ $publicite->video }}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a></li>    
                </ul>
              </div> -->
              </div>
            </div>
          </div>



          <!-- DEBUT MODAL -->
          <div class="modal fade" id="staticBackdrop{{ $publicite->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header text-white" style="background: black;">
                  <h5 class="modal-title text-center" id="staticBackdropLabel">Votre commentaire</h5>
                  <button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                  <form method="POST" action="{{ route('addCommentaire') }}">
                    @csrf
                    <input type="hidden" id="commenter_id_pub" name="commenter_id_pub" value="{{ $publicite->id}}" />
                    <div class="mb-3 row">
                      <div class="col-sm-10">
                        <textarea required class="form-control" id="commentaire" name="commentaire" aria-label="With textarea"></textarea>
                      </div>
                      @error('commentaire')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="modal-footer">
                      <button type="button" style="min-width: 60px; max-width: 60px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-secondary shadow" data-bs-dismiss="modal">Fermer</button>
                      <button type="submit" style="min-width: 60px; max-width: 60px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-primary shadow">Valider</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- FIN MODAL -->

          @endforeach
          @if(count($publicites) > 0)
          <div class="text-center  mb-6">
            <a href="https://wa.me/22998736175" class="btn btn-info mt-2">Nous écrire</a>
          </div>
          @endif
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      <hr>


      @if(count($publications) > 0)
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2 text-center">
            <div class="col-sm-12">
              <h1 class="m-0 " style="color: #138496; font-weight: bolder;">Affiches publicitaires</h1>
            </div><!-- /.col -->
            <!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      @endif


      <div class="container-fluid mt-4">
        <div class="row">
          @foreach($publications as $publication)
          <div class="col-lg-12 mb-6">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title text-bold" style="color: steelblue;">{{ $publication->title }}</h2>
              </div>
              <div class="card-body">
                @if($publication->image != "")
                <img src="/uploads/images/{{ $publication->image }}" style="height: 100%; width: 100%;" class="mb-4">
                @endif

                @if($publication->video != "")

                <video controls width="100%">

                  <source src="/uploads/videos/{{ $publication->video }}">


                  Sorry, your browser doesn't support embedded videos.
                </video>
                @endif

                {!! $publication->description !!}



                <div class="btn-group me-2" role="group" aria-label="First group">
                  <form method="POST" action="{{ route('LikePublication') }}">
                    @csrf
                    <input type="hidden" id="id_publication" name="id_publication" value="{{ $publication->id}}" />
                    <button type="submit" style="min-width: 65px; max-width: 65px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-primary shadow"><i class="fa fa-star"></i></button>
                  </form>
                  @if($publication->compteur_like_publication == 0)
                  <button class="btn btn-outline-primary" id="nbr_likes">0</button>
                  @else
                  <button class="btn btn-outline-primary" id="nbr_likes">{!! $publication->compteur_like_publication !!}</button>
                  @endif
                </div>

                <div class="btn-group me-2" role="group" aria-label="First group">
                  <button type="button" style="min-width: 100px; max-width: 100px; min-height: 35px;  max-height: 35px;" data-bs-toggle="modal" data-bs-target="#staticBackdrop2{{ $publication->id}}" class="btn btn-outline-primary shadow"><i class="fa fa-comments"></i> </button>
                  @if($publication->compteur_commenter_publication == 0)
                  <button type="button" class="btn btn-outline-primary">0</button>
                  @else
                  <button type="button" class="btn btn-outline-primary">{!! $publication->compteur_commenter_publication !!}</button>
                  @endif
                </div>

                <!-- LOCAL LINKS -->

                <!-- <a href="https://wa.me/?text=http://127.0.0.1:8000/customers/publie/{{ $publication->id}}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=http://127.0.0.1:8000/customers/publie/{{ $publication->id }}" class="social-button " id=""><span class="fa fa-facebook-official"></span></a>
                <a href="https://twitter.com/intent/tweet?text={{ $publication->title }}&amp;url=http://127.0.0.1:8000/customers/publie/{{ $publication->id }}" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://127.0.0.1:8000/customers/publie/{{ $publication->id }}&amp;title={{ $publication->title }}&amp;summary=publication sur linkedin" class="social-button " id=""><span class="fa fa-linkedin"></span></a> -->

                <!-- ONLINE  LINKS -->

                <a href="https://wa.me/?text=https://contactplus.cercle-cs.com/customers/publie/{{ $publication->id}}" class="social-button " id=""><span class="fa fa-whatsapp"></span></a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=https://contactplus.cercle-cs.com/customers/publie/{{ $publication->id }}"  class="social-button " id=""><span class="fa fa-facebook-official"></span></a>
                <a href="https://twitter.com/intent/tweet?text={{ $publication->title }}&amp;url=https://contactplus.cercle-cs.com/customers/publie/{{ $publication->id }}" class="social-button " id=""><span class="fa fa-twitter"></span></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://contactplus.cercle-cs.com/customers/publie/{{ $publication->id }}&amp;title={{ $publication->title }}&amp;summary=publication sur linkedin" class="social-button " id=""><span class="fa fa-linkedin"></span></a>

              </div>
            </div>
          </div>



          <!-- DEBUT MODAL2 -->
          <div class="modal fade" id="staticBackdrop2{{ $publication->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header text-white" style="background: black;">
                  <h5 class="modal-title text-center" id="staticBackdropLabel">Votre commentaire</h5>
                  <button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>


                <div class="modal-body">
                  <form method="POST" action="{{ route('addCommentairePub') }}">
                    @csrf
                    <input type="hidden" id="commenter_id_pub2" name="commenter_id_pub2" value="{{ $publication->id}}" />
                    <div class="mb-3 row">
                      <div class="col-sm-10">
                        <textarea required class="form-control" id="commentaire2" name="commentaire2" aria-label="With textarea"></textarea>
                      </div>
                      @error('commentaire2')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="modal-footer">
                      <button type="button" style="min-width: 60px; max-width: 60px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-secondary shadow" data-bs-dismiss="modal">Fermer</button>
                      <button type="submit" style="min-width: 60px; max-width: 60px; min-height: 35px;  max-height: 35px;" class="btn btn-outline-primary shadow">Valider</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!-- FIN MODAL2 -->

          @endforeach
          @if(count($publications) > 0)
          <div class="text-center  mb-6">
            <a href="https://wa.me/22998736175" class="btn btn-info mt-2">Nous écrire</a>
          </div>
          @endif
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <div class="container mb-16">
      <div class="row border border-light">
        <div class="col-md-2"></div>
        <div class="col-md-4 col-8 border border-dark bg bg-success">
          <h1 style="text-align: center; font-weight: bold;font-size: 15px;padding-left: 150px; padding:15px">Nombre d'adhérents</h1>
        </div>
        <div class="col-md-4 col-4 border border-dark">
          <h1 style="text-align: center; font-weight: bold;font-size: 15px;padding-left: 150px; padding:15px"> {{ $admin->paid + $users->count() }} </h1>
        </div>
        <div class="col-md-2"></div>
      </div></br></br>
      @if(auth()->user()->has_module == "gratuit")
      @endif
      
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/share.js') }}"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('/js/jquery.min.js') }}"></script>

    <!-- <script src="{{ asset('assets/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script> -->



    <script>
      $(document).ready(function() {
        //$("#superviseurinfo").hide();
        // $("#notinfo").hide();
        // $("#nofo").hide();
        save_like();

      })


      function save_like() {

        var data = new FormData();


        var form_data = $('#formulaire').serializeArray();
        $.each(form_data, function(key, input) {
          data.append(input.name, input.value);
        });

        // var id_to_send = getElementById('id_pub_get');
        // alert('id_to_send');

        $.ajax({
          url: "{{ route('LikePub') }}",
          method: "POST",
          processData: false,
          contentType: false,
          data: data,
          beforeSend: function(data) {

            // $("#produit_achete").attr('required', true);
            // $("#ville_appel").attr('required', true);
            // $("#prix_achat").attr('required', true);
            // $("#raion_appel").attr('required', true);

            // $("#myLargeModalLabel button#close").prop("disabled", true);
            // $("#myLargeModalLabel button#valider").prop("disabled", true);
            // $("#ajouter").prop("disabled", true);
            // $("#myLargeModalLabel button#valider").html('<i class="text-center fa fa-spinner fa-pulse fa-1x fa-fw ml-2"></i>');
          },
          success: function(data) {
            if (!$.isEmptyObject(data.error)) {
              printErrorMsg(data.error);
            }

            if (data.status) {
              //alert(data.nombre);
              $("#message").removeClass('alert alert-danger').html('').hide();
              $("#message").addClass('alert alert-success').html(data.message).show();

              $("#nbr_like").html(data.nombre).show();

              $("#montant_valeur").html(data.valeur).show();
              $("#texte").html(data.information).show();
              $("#myLargeModalLabel button#close").prop("disabled", false);
              $("#myLargeModalLabel button#valider").prop("disabled", false);
              $("#myLargeModalLabel button#valider").html('Enregistrer');

              $("#ajouter").prop("disabled", true);
              $("#nouveau").prop("disabled", false);
              $("#appel_coupe").prop("disabled", true);

            } else {
              $("#ajouter").prop("disabled", false);

              $("#message").addClass('alert alert-danger').html(data.message).show();

            }


          },
          error: function(data) {
            $("#ajouter").prop("disabled", false);

            $("#message").removeClass('alert alert-success').html('').hide();
            $("#message").addClass('alert alert-danger').html(data.message).show();

          }
        });

      }



      // });
    </script>

    @stop
@extends('templates.dashboard.customers.master')

@section('title')
Les vidéos publicitaires
@stop

@section('content')
<div class="content-wrapper">
	
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Les vidéos publicitaires</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <hr>


    <hr>

    <!-- Main content -->
    <div class="content mt-4">
      <div class="container-fluid">
        <div class="row">
            @foreach($publications as $video)
            <div class="col-md-4">
               
               <div class="card" style="width: 24rem;"> 
               
                        <div class="card-body">
                         <video controls width="100%" height="315">
                        
                           <source src="/uploads/videos/{{$video->video}}" type="video/mp4">
                                   
                        
                        </video>
                    <h1 class="pb-8 mt-8" style="font-size:25px;font-weight:bold">{{$video->title}}</h1>
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <button type="button" class="btn btn-secondary">
                          <a class="nav-link btn btn-info" href="https://wa.me/22998736175" target="_blank"  style="color:white">COMMANDER UNE VIDEO PUBLICITAIRE</a>
                      </button>
                      <button type="button" class="btn btn-secondary">
                          <a class="nav-link btn btn-info" href="https://wa.me/{{ user_phone($video->from_user)}}" target="_blank"  style="color:white">CONTACTEZ L'ANNONCEUR</a>
                      </button>
                    </div>

                  </div>
                </div>  
             </div>
             @endforeach
        </div>
       
        <!-- /.row -->
      </div><!-- /.container-fluid -->
        {{ $publications->links() }}
    </div>
</div>
@stop
@extends('templates.dashboard.customers.master')

@section('title')
Modifier votre profil
@stop

@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Modifier votre profil</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ui stackable grid">
        <div class="row">
          <div class="ui three wide column aligned center aligned">
              
          </div>
          
          <div class="ui ten wide column aligned">
              
            <div class="ui horizontal divider">
              Modifier mon profil
            </div>
            
            <div class="ui message">
              <form method="post" action="{{ route('customers_modify_profil_form') }}" class="ui form">
						    	@csrf
							    <div class="fields">
								    <div class=" eight wide field">
								      <label for="nom">Nom</label>
								      <input type="text" placeholder="{{ auth()->user()->nom }}" id="nom" name="nom" value="{{ old('nom')}}">								 	
								    </div>
								    
								    
								    <div class=" eight wide field">
								      <label for="prenom">Prénoms</label>
								      <input type="text" placeholder="{{ auth()->user()->prenom }}" id="prenom" name="prenom" value="{{ old('prenom')}}">
								    </div>
								</div>


								<div class="fields">
								    <div class=" eight wide field">
								      <label for="email">Email</label>
								      <input type="email" placeholder="{{ auth()->user()->email }}" id="email" name="email" value="{{ old('email')}}">
								      	
								    </div>
								    
								    <div class=" eight wide field">
								      <label for="phone">Téléphone (Whatsapp)</label>
								      <input type="tel" placeholder="{{ auth()->user()->phone }}" id="phone" name="phone" value="{{ old('phone')}}">
								      <small>Ecrivez l'indicatif de votre pays.</small>
								    </div>
								</div>

								<div class="fields">
								    <div class=" eight wide field">
								      <label for="adresse">Adresse</label>
								      <input type="text" placeholder="{{ auth()->user()->adresse }}" id="adresse" name="adresse" value="{{ old('adresse')}}">
								      	
								    </div>
								    
								    <div class=" eight wide field">
								      <label for="profession">Profession</label>
								      <input type="text" placeholder="{{ auth()->user()->profession }}" id="profession" name="profession" value="{{ old('profession')}}">
								    </div>
								</div>


								<div class="two fields">
								    <div class=" field">
								      <label for="password">Modifier mon mot de passe</label>
								      <input type="password" name="password" id="password" value="{{ old('password')}}">
								    </div>

								    <div class=" field">
								      <label for="confirm_password">Confirmez le mot de passe entré</label>
								      <input type="password" name="confirm_password" id="confirm_password">
								    </div>
								</div>
								  <button class="ui submit button" type="submit">Modifier</button>
							</form>
            </div>
            
          </div>
          
          <div class="ui three wide column aligned center aligned">
              
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
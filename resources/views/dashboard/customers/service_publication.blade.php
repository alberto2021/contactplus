@extends('templates.dashboard.customers.master')

@section('title')
Choisir une option
@stop

@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Activer le service de publication | Choisir une option</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
        <div class="row">
          
          <div class="col-lg-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 1: Faire des publications sur une durée de 05 jours</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 500 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_activate_service_publication_option', 1) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>

            <div class="card card-danger card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 2: Faire des publications sur une durée de 10 jours</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 800 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_activate_service_publication_option', 2) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card card-info card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 3: Faire des publications sur une durée de 15 jours</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 1350 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_activate_service_publication_option', 3) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>

            <div class="card card-warning card-outline">
              <div class="card-header">
                <h3 class="m-0 ui header">Option 4: Faire des publications sur une durée de 01 mois</h3>
              </div>
              <div class="card-body">
                <h6 class="card-title">Prix: 2500 F CFA</h6>

                <p class="card-text"></p>
                <a href="{{ route('customers_activate_service_publication_option', 4) }}" class="btn btn-primary">Choisir cette option</a>
              </div>
            </div>
          </div>
          
          
          
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
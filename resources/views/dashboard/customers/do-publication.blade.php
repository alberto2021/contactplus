@extends('templates.dashboard.customers.master')

@section('title')
Poster une annonce ou une affiche
@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#description').summernote({
      placeholder: 'Saisissez le contenu de la publication ici',
        tabsize: 2,
        height: 150,
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', ]],
          ['view', ['fullscreen', 'codeview', 'help']],
          ['fontname', ['fontname']],
        ],
        popover: {
          image: [
            ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
            ['float', ['floatLeft', 'floatRight', 'floatNone']],
            ['remove', ['removeMedia']]
          ],
          link: [
            ['link', ['linkDialogShow', 'unlink']]
          ],
          table: [
            ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
            ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
          ],
          air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']],
            ['para', ['ul', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture']]
          ]
        }


    });
  });
</script>
@stop

@section('content')

<div class="content-wrapper">
	@if(session()->has('success'))
      <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
          {{ session()->get('success') }}
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Poster une annonce </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div>
        <div class="ui  divider">
              
            </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="content">
      <div class="container-fluid ">
          
          <div class="alert alert-warning mt-2  shadow-2xl" style="color:white;font-weight:bold">
			<p>Postez des annonces sérieuses,précieuses et responsables pour ne pas voir l'annonce supprimée.
			<br><strong>NB:</strong>Eviter la répétition d'une annonce.
			</p>
		</div>
          <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire de publication</h2>
                @if(session()->has('publication'))
                  <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
                      {{ session()->get('publication') }}
                  </div>
                @endif
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-2 offset-lg-2 col-lg-8 col-md-8 ">

                    <form class="bg-white rounded-5 shadow-5-strong " action="{{ route('customers_do_publication_form') }}" method="post" enctype="multipart/form-data">
                        @csrf
                  
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="nom">Saisissez le titre de la publication</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="nom" name="nom" class="form-control @error('nom') is-invalid @enderror" value="{{ old('nom') }}" />

                                @error('nom')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        <div class="row mb-4">

                          <div class="col" style="text-align: left;">
                              
                              <label class="form-label" for="description">Saisissez le contenu de la publication</label>

                              <div style="margin-bottom: 10px;"></div>

                              <textarea name="description" id="description"  style="" rows="6" cols="30" class="form-control"></textarea>

                              @error('description')

                                  <div class="" style="color: red;">{{ $message }}</div>

                              @enderror
                              
                          </div>
  
                        </div>

                        @if ( Request::path() != 'customers/do-publication' )
                            
                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="image">Uploader une image (facultatif)</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="file" id="image" name="image" class="form-control @error('image') is-invalid @enderror" value="{{ old('image') }}"  />

                                @error('image')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>
                        
                        @endif

                        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="duree">Durée de la publication (en jours)</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="number" id="duree" name="duree" class="form-control @error('duree') is-invalid @enderror" value="{{ old('duree') }}" min="1"  />

                                @error('duree')

                                    <div class="" style="color: red;">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

                        

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Publier</button>


                      </form>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a"> CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
          
          <div class="row" style="display: none;">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h2 class="card-title">Remplissez le formulaire de publication</h2>
                @if(session()->has('publication'))
                  <div class="alert alert-success mt-2 ml-4 mr-4 shadow-2xl">
                      {{ session()->get('publication') }}
                  </div>
                @endif
              </div>
              <div class="card-body">
                
                <div class="row">
                  <div class="offset-md-2 offset-lg-2 col-lg-8 col-md-8 ">

                    <div class="ui negative message">
                        <div class="ui header">
                            Abonnement non activé
                        </div>
                        <p>Vous devez activer un module ou activer un forfait pour faire les publications!!! Vous pouvez activer un module ou activer tout simplement un service de votre choix. Merci!!</p>
                        
                        <a class="btn btn-primary" href="{{ route('customers_activate_service') }}">Activer un service</a>
                        <a class="btn btn-success" href="{{ route('customers_activate_module') }}">Activer un module</a>
                    </div>
                    
                  </div>

                  
                </div>

                
              </div>
              <div class="card-footer text-center">
                <h5 style="color: #9e825a"> CCS+ | Cercle du Commerce Social</h5>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
          
          <!-- /.col-md-6 -->
        </div>
          
        
      </div><!-- /.container-fluid -->
    </div>


   
</div>
@stop
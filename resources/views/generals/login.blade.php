<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Se connecter | {{ env('APP_NAME') }}</title>
		
		@include('templates.components.css')
		@yield('stylesheets')

		
	</head>

	<body style="background-color: #ECEFF8;" >
		
		<div class="ui stackable grid" style="height: 100%;">

			<div class="row">
				
        		<div class="ui eight wide column aligned center " style="background-color: #ECEFF8; padding-top: 20px; padding-left: 50px;">
        			<img class="ui fluid image" src="/assets/img/contact2.jpg" width="100%" height="100%" >
		        </div>

		        <div class="eight wide column " style="margin-top: 30px;">

		        	<div class="ui  basic  ">
					  
					  <div class="ui horizontal divider" style="color: #7E623D;">
					    Connection | CCS+	
					  </div>


					  	<div class="ui stripe" >
						    <div class="ui middle aligned stackable grid container">
						      <div class="row">
						        <div class="wide column">
						          
						          <div class="ui container fluid" style=" margin-top: 20px;">
									<div >
									  <div class="ui  message">

						    @if ($errors->any())
							    <div class="ui error message">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							@if(session()->has('error'))
				                  <div class="ui message error">
				                      {{ session()->get('error') }}
				                  </div> 
				            @endif
				            @if(session()->has('okay'))
				                  <div class="ui message positive">
				                      {{ session()->get('okay') }}
				                  </div> 
				            @endif
									    
									    <form method="post" action="{{ route('login_form') }}" class="ui form">
						    	@csrf
							    
                                    
                                    @if(session()->has('deconnexion'))
                      <div class="alert alert-info shadow-2xl">
                          {{ session()->get('deconnexion') }}
                      </div>
                  @endif

                  @if(session()->has('noenter'))
                      <div class="alert alert-danger shadow-2xl">
                          {{ session()->get('noenter') }}
                      </div>
                  @endif

                    @if(session()->has('error'))
                      <div class="alert alert-danger shadow-2xl">
                          {{ session()->get('error') }}
                      </div>
                  @endif
                  

                    @error('email_phone')
                            <div class="alert alert-danger">Ce champ est requis.</div>
                        @enderror


								    <div class="required wide field">
								      <label for="email_phone">Email ou Téléphone</label>
								      <input type="text" placeholder="" id="email_phone" name="email_phone" value="{{ old('email_phone')}}" class=" @error('email_phone') is-invalid @enderror">
								      	
								    </div>
								    
								
								    <div class="required field">
								      <label for="password">Entrez votre mot de passe</label>
								      <input type="password" name="password" id="password" value="{{ old('password')}}" class="@error('password') is-invalid @enderror">
								    </div>

								   
								
								  <button class="ui submit button" type="submit">Se connecter</button>
								  
								  <p >Mot de passe oublié? <a href="{{ route('reset_step_one') }}" class="ui teal" style="color: teal">Cliquez ici</a></p>
							</form>
									  </div>
									  
									</div>
								</div>
						        </div>

						        
						        
						      </div>
						      <div class="row" style="margin-top: 15px;">
						        
						      </div>
						    </div>
						  </div>

						



						<div class="ui horizontal divider" style="margin-top: 10px;">
						    Pas inscrit?
						</div>

						<div class="ui center aligned middle segment">
							<a class="ui teal labeled icon button" href="{{ route('join') }}">
							    S'inscrire
							    <i class="sign-in icon"></i>
							  </a>
						</div>

					</div>

		        </div>
		    </div>
			
		</div>

		


		@include('templates.components.js')		
	</body>

</html>
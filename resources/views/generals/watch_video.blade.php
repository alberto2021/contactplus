
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Vidéos | {{ env('APP_NAME') }}</title>
    
    @include('templates.components.css')
    @yield('stylesheets')

  <style type="text/css">
    body {
      background-color: #ECEFF8;
    }
    body > .grid {
      height: 100%;
    }
    
    .column {
      max-width: 500px;
    }

    
  </style>
</head>
<body>

<style type="text/css">
  @media only screen and (max-width: 1000px) {
    .adevoi2{
      display: block;
    }

  }

  @media only screen and (max-width: 800px) {
    .adevoi {
      display: block;
    }

  }
</style>
    <nav class="navbar navbar-expand-lg">
      <div class="container">

            <!-- Logo -->
            <a class="" href="{{ route('welcome') }}">
               <h1><span style="color: #9e8359">CCS</span> <span style="color: #2ba9e1;font-weight: bold">CONTACT+</span></h1>     
            </a>

            <a class="nav-link btn btn-outline-success adevoi" href="#" data-scroll-nav="6" style="display: none;">Postez une annonce</a>
        <button class="navbar-toggler adevoi2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"><i class="fas fa-bars" style="color: black;"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent"">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link btn btn-primary" href="{{route('watch_video')}}" style="color:white">Voir une vidéo publicitaire</a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn btn-primary" href="https://wa.me/22998736175" target="_blank"  style="color:white">Postez une annonce</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
      <!-- End Navbar ====
      ======================================= -->

 <div class="container mt-32">
        <div class="row">
            @foreach($videos as $video)
            <div class="col-md-4">
               
               <div class="card" style="width: 24rem;"> 
               
                    <div class="card-body">
                         <video width="560" height="315" controls>
                              <source src="/uploads/videos/{{$video->video}}" type="video/mp4">
                               
                              Your browser does not support the video tag.
                        </video>
                        <h1 class="pb-8 mt-8" style="font-size:25px;font-weight:bold">{{$video->title}}</h1>
                            
                        <a class="nav-link btn btn-info" href="https://wa.me/22998736175" target="_blank"  style="color:white">Commander une vidéo publicitaire</a>
                     </div>
                    
    
              </div>  
            </div>
             @endforeach
        </div>
 </div>


@include('templates.components.js')
</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Mot de passe oublié | {{ env('APP_NAME') }}</title>
		
		@include('templates.components.css')
		@yield('stylesheets')

		
	</head>

	<body style="background-color: #ECEFF8;" >
		
		<div class="ui stackable grid" style="height: 100%;">

			<div class="row">
				
        		<div class="ui eight wide column aligned center " style="background-color: #ECEFF8; padding-top: 20px; padding-left: 50px;">
        			<img class="ui fluid image" src="https://img.freepik.com/vecteurs-libre/illustration-concept-mot-passe-oublie_114360-1010.jpg?w=2000" width="100%" height="100%" >
		        </div>

		        <div class="eight wide column " style="margin-top: 30px;">

		        	<div class="ui  basic  ">
					  
					  <div class="ui horizontal divider" style="color: #7E623D;">
					    Mot de passe oublié | CCS+	
					  </div>


					  	<div class="ui stripe" >
						    <div class="ui middle aligned stackable grid container">
						      <div class="row">
						        <div class="wide column">
						          
						          <div class="ui container fluid" style=" margin-top: 20px;">
									<div >
									  <div class="ui  message">

						    
							
									    
									    <form method="post" action="{{ route('reset_step_one_form') }}" class="ui form">
						    	@csrf

                    @if(session()->has('errormail'))
                      <div class="alert alert-danger shadow-2xl">
                          {{ session()->get('errormail') }}
                      </div>
                  @endif
                  

                    @error('email_phone')
                            <div class="alert alert-danger">Ce champ est requis.</div>
                        @enderror


								    <div class="required wide field">
								      <label for="email_phone">Entrez l'email de votre compte</label>
								      <input type="text" placeholder="" id="email_phone" name="email_phone" value="{{ old('email_phone')}}" class=" @error('email_phone') is-invalid @enderror">
								      	
								    </div>
								
								  <button class="ui submit button" type="submit">Confirmez</button>
								  
								  
							</form>
									  </div>
									  
									</div>
								</div>
						        </div>

						        
						        
						      </div>
						      <div class="row" style="margin-top: 15px;">
						        
						      </div>
						    </div>
						  </div>

						



						<div class="ui horizontal divider" style="margin-top: 10px;">
						    Vous vous rappelez du mot de passe?
						</div>

						<div class="ui center aligned middle segment">
							<a class="ui teal labeled icon button" href="{{ route('login') }}">
							    Se connecter
							    <i class="sign-in icon"></i>
							  </a>
						</div>

					</div>

		        </div>
		    </div>
			
		</div>

		


		@include('templates.components.js')		
	</body>

</html>
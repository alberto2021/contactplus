<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Inscription - Administration - {{ env('APP_NAME') }}</title>
		<link
	     rel="stylesheet"
	     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
	   />
	   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
		@include('templates.components.css')
		
	</head>

	<body style="background-color: gray; background-attachment: fixed;">


		@if (session()->get('one') == "oui")
			<div class="container-fluid mt-40">
				<div class="row">
					<div class="offset-md-2 col-md-8 offset-lg-2 col-lg-8 text-center">
						<h1 class="font-weight-bold mb-6" style="font-size: 30px; color: #1a237e;">Inscription - Administration - CCS+ </h1>
						<form class="bg-white  rounded-5 shadow-5-strong p-5" action="{{ route('admins_join_form_confirm') }}" method="post">
					              	@csrf
					          
					          <div class="alert alert-info shadow-2xl">
					          	Veuillez donc confirmez l'insertion de ce nouveau administrateur. Merci!
					          </div>

					          @if(session()->has('error'))
							    <div class="alert alert-danger shadow-2xl">
							        {{ session()->get('error') }}
							    </div>
							@endif
			                <div class="row mb-4">

	                            <div class="col" style="text-align: left;">
	                                
	                                <label class="form-label" for="code_conf">Entrez le code de confirmation</label>

	                                <div style="margin-bottom: 10px;"></div>

	                                <input type="text" id="code_conf" name="code_conf" class="form-control @error('code_conf') is-invalid @enderror" value="{{ old('code_conf') }}" />

	                                @error('code_conf')

	                                    <div class="alert alert-danger">{{ $message }}</div>

	                                @enderror
	                                
	                            </div>
	      
	                        </div>

	                        <button type="submit" class="btn btn-outline-success btn-rounded">Confirmez </button>


					    </form>
					</div>
				</div>
			</div>
		@else
			<div class="container-fluid mt-40">
				<div class="row">
					<div class="offset-md-2 col-md-8 offset-lg-2 col-lg-8 text-center">
						<h1 class="font-weight-bold mb-6" style="font-size: 30px; color: #1a237e;">Inscription - Administration - CCS+ </h1>
						<form class="bg-white  rounded-5 shadow-5-strong p-5" action="{{ route('admins_join_form') }}" method="post">
					              	@csrf
					          
					          <div class="alert alert-danger">
					          	<ul>
					          		<li>1. De grands pouvoirs confèrent de grandes responsabilités.</li>
					          		<li>2. Réfléchissez avant d'utiliser le clavier.</li>
					          		<li>3. Cette action est irreversible.</li>
					          	</ul>
					          </div>
			                <div class="row mb-4">

	                            <div class="col" style="text-align: left;">
	                                
	                                <label class="form-label" for="username">Entrez votre nom d'utilisateur</label>

	                                <div style="margin-bottom: 10px;"></div>

	                                <input type="text" id="username" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" />

	                                @error('username')

	                                    <div class="" style="color: red;">{{ $message }}</div>

	                                @enderror
	                                
	                            </div>
	      
	                        </div>


	                        <div class="row mb-4">

	                            <div class="col" style="text-align: left;">
	                                
	                                <label class="form-label" for="email">Entrez votre adresse électronique</label>

	                                <div style="margin-bottom: 10px;"></div>

	                                <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" />

	                                @error('email')

	                                    <div class="" style="color: red;">{{ $message }}</div>

	                                @enderror
	                                
	                            </div>
	      
	                        </div>

					        <div class="row mb-4">

	                            <div class="col" style="text-align: left;">
	                                
	                                <label class="form-label" for="password">Entrez votre mot de passe</label>

	                                <div style="margin-bottom: 10px;"></div>

	                                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" />

	                                @error('password')

	                                    <div class="" style="color: red;">{{ $message }}</div>

	                                @enderror
	                                
	                            </div>

	                            <div class="col" style="text-align: left;">
	                                
	                                <label class="form-label" for="confirm_password">Confirmez le mot de passe</label>

	                                <div style="margin-bottom: 10px;"></div>

	                                <input type="password" id="confirm_password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror" value="" />

	                                @error('confirm_password')

	                                    <div class="" style="color: red;">{{ $message }}</div>

	                                @enderror
	                                
	                            </div>
	      
	                        </div>

					        <div class="col-md-12 col-sm-12 mb-4" style="text-align: left;">
		                                    
	                            <label class="form-label" for="wanted">Niveau d'administration de l'administrateur</label>

	                            <div style="margin-bottom: 10px;"></div>

	                            <select class="custom-select custom-select-md @error('wanted') is-invalid @enderror" id="wanted" name="wanted" value="{{ old('wanted') }}">
	                              <option value="">--Veuillez choisir votre réponse--</option>
	                              
	                               <option value="simple">Administrateur simple</option>
	                               <option value="super">Super administrateur</option>

	                              
	                            </select>
	                            

	                            @error('wanted')

	                                <div class="" style="color: red;">{{ $message }}</div>

	                            @enderror
	                            
	                        </div>

	                        <button type="submit" class="btn btn-outline-primary btn-rounded">Valider</button>


					    </form>
					</div>
				</div>
			</div>
		@endif
			
		@include('templates.components.js')
	</body>

</html>
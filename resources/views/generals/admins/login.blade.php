<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Connexion - Administration - {{ env('APP_NAME') }}</title>
		<link
	     rel="stylesheet"
	     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
	   />
	   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
		@include('templates.components.css')
		
	</head>

	<body style="background-color: gray;  background-attachment: fixed;">

		<div class="container-fluid mt-40 mb-44">
			<div class="row">
				<div class="offset-md-2 col-md-8 offset-lg-2 col-lg-8 text-center">
					<h1 class="font-weight-bold mb-6" style="font-size: 30px; color: #1a237e; z-index: -2;">Connexion - Administration - CCS+ </h1>
					<form class="bg-white  rounded-5 shadow-5-strong p-5" action="{{ route('admins_login_form') }}" method="post">
				              	@csrf
				          
				          

							@if(session()->has('deconnexion'))
							    <div class="alert alert-success shadow-2xl">
							        {{ session()->get('deconnexion') }}
							    </div>
							@endif
							@if(session()->has('errore'))
							    <div class="alert alert-danger shadow-2xl">
							        {{ session()->get('errore') }}
							    </div>
							@endif
							@if(session()->has('noenter'))
							    <div class="alert alert-danger shadow-2xl">
							        {{ session()->get('noenter') }}
							    </div>
							@endif
		                <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="username">Entrez votre nom d'utilisateur</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="text" id="username" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" />

                                @error('username')

                                    <div class="alert alert-danger">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

				        <div class="row mb-4">

                            <div class="col" style="text-align: left;">
                                
                                <label class="form-label" for="password">Entrez votre mot de passe</label>

                                <div style="margin-bottom: 10px;"></div>

                                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" />

                                @error('password')

                                    <div class="alert alert-danger">{{ $message }}</div>

                                @enderror
                                
                            </div>
      
                        </div>

				        <div class="row mb-6">
		                  <div class="col d-flex justify-content-center">
		                    <!-- Checkbox -->
		                    <div class="form-check">
		                      <input class="form-check-input" type="checkbox" name="remember" id="remember" checked />
		                      <label class="form-check-label" for="remember">
		                        Se souvenir de moi
		                      </label>
		                    </div>
		                  </div>

		                  <div class="col text-center">
		                    <!-- Simple link -->
		                    <a href="#!" style="color: #9e825a">Mot de passe oublié?</a>
		                  </div>
		                </div>

                        <button type="submit" class="btn btn-outline-primary btn-rounded">Se connecter</button>


				    </form>
				</div>
			</div>
		</div>
			
		@include('templates.components.js')
	</body>

</html>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Inscription - {{ env('APP_NAME') }}</title>
		<link
	     rel="stylesheet"
	     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
	   />
	   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
		@include('templates.components.css')
		
	</head>

	<body>


      

      
		
@if(session()->has('etape'))


		        @if( session()->get('etape') == "first step")

		        	@if( session()->has('inscription'))
					    <header class="header pos-re slider-fade" data-scroll-index="0">

        <div class="owl-carousel owl-theme">
                
                
                <div class="item bg-img" data-overlay-dark="3" data-background="/assets/img/inscription.jpg">
                    <div class="text-center v-middle caption mt-30">
                        <h1>Boostez vos activités</h1>
                        <div class="row">
                            <div class="offset-md-4 col-md-4 offset-lg-4 col-lg-4">
                                <div class="card">
					                <div class="card-header">
					                  <h1 style="font-weight: bolder; color: #9e825a; font-size: 20px;">FÉLICITATIONS</h1>
					                </div>
					                <div class="card-body">
					                  <div class="alert alert-success">
									        {{ session()->get('inscription') }}
									  </div>
					                </div>
					              </div>
                            </div>
                        </div>
                        <a href="{{route('join')}}" class="butn butn-light mt-30">
                            <span>Inscrivez-vous  </span>
                        </a>

                        <a href="{{route('login')}}" class="butn butn-bg">
                            <span>Se connecter</span>
                        </a>
                    </div>
                </div>
        </div>
      </header>
				    @else
				    	<header class="header pos-re slider-fade" data-scroll-index="0">

					        <div class="owl-carousel owl-theme">
					                
					                
					                <div class="item bg-img" data-overlay-dark="3" data-background="/assets/img/inscription.jpg">
					                    <div class="text-center v-middle caption mt-30">
					                        
					                        <div class="row">
					                            <div class="offset-md-2 col-md-8 offset-lg-2 col-lg-8">
					                                <h1 class="font-weight-bold text-white mb-6" style="font-size: 30px;">Inscription | CCS+ | Informations Générales</h1>
				              <form class="bg-white  rounded-5 shadow-5-strong p-5" id="join" onsubmit="process(event)" action="{{ route('join_form') }}" method="post">
				              	@csrf
				              	<p class="mb-4 text-center " style="color: red;">* Tous les champs sont obligatoires</p>
				                <div class="alert alert-info" style="display: none;"></div>
				                

				                <div class="row">
				                	
					                <div class="form-outline mb-4 col-md-6 col-sm-12">

					                  <input type="text" id="nom" name="nom" class="form-control @error('nom') is-invalid @enderror" />
					                  <label class="form-label" for="nom" style="font-size: 20px;">Noms</label>
					                  	@error('nom')
				                            <div class="alert alert-danger">
				                            	<ul>
				                            		<li>Ce champ est requis.</li>
				                            		<li>Veuillez saisir au minimum 3 caractères.</li>
				                            	</ul></div>
				                        @enderror
					                </div>

					                <div class="col-md-1"></div>
					                <div class="form-outline mb-4 col-md-5 col-sm-12">
					                  <input type="text" name="prenom" id="prenom" class="form-control @error('prenom') is-invalid @enderror" />
					                  <label class="form-label" for="prenom" style="font-size: 20px;">Prénoms</label>
					                  	@error('prenom')
				                            <div class="alert alert-danger">
				                            	<ul>
				                            		<li>Ce champ est requis.</li>
				                            		<li>Veuillez saisir au minimum 3 caractères.</li>
				                            	</ul></div>
				                        @enderror
					                </div>
				                </div>

				                <div class="row">
					                <div class="form-outline mb-4 col-md-6 col-sm-12">
					                  <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" />
					                  <label class="form-label" for="email" style="font-size: 20px;">Email</label>
					                  @error('email')
				                            <div class="alert alert-danger">
				                            	{{ $message }}</div>
				                        @enderror
					                </div>
					                <div class="col-md-1"></div>
					                <div class=" mb-4 col-md-5 col-sm-12">
					                	<label class="form-label" for="phone" style="font-size: 20px;">Téléphone (Whatsapp)</label>
					                  <input type="tel" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" />
					                  @error('phone')
				                            <div class="alert alert-danger">
				                            	{{ $message }}</div>
				                        @enderror
					                </div>
				                </div>

				                <div class="row">
					                <div class="form-outline mb-4 col-md-6 col-sm-12">
					                  <input type="text" name="adresse" id="adresse" class="form-control @error('adresse') is-invalid @enderror" />
					                  <label class="form-label" for="adresse" style="font-size: 20px;">Votre adresse</label>
					                  @error('adresse')
				                            <div class="alert alert-danger">
				                            	{{ $message }}</div>
				                        @enderror
					                </div>
					                <div class="col-md-1"></div>
					                <div class="form-outline mb-4 col-md-5 col-sm-12">
					                  <input type="text" name="profession" id="profession" class="form-control @error('profession') is-invalid @enderror" />
					                  <label class="form-label" for="profession" style="font-size: 20px;">Votre profession</label>
					                  	@error('profession')
				                            <div class="alert alert-danger">
				                            	{{ $message }}</div>
				                        @enderror
					                </div>
				                </div>


				                

				                <!-- 2 column grid layout for inline styling -->
				                

				                <div class="flex justify-end">
				                	<button type="submit" class="btn btn-outline-primary btn-rounded ">Suivant	</button>
				                </div>
				              </form>
					                            </div>
					                        </div>
					                        <a href="{{route('join')}}" class="butn butn-light mt-30">
					                            <span>Inscrivez-vous  </span>
					                        </a>

					                        <a href="{{route('login')}}" class="butn butn-bg">
					                            <span>Se connecter</span>
					                        </a>
					                    </div>
					                </div>
					        </div>
					      </header>
				    @endif
				    <!-- Background image -->
				 

				@elseif ( session()->get('etape') == "second step" )

					<header class="header pos-re slider-fade" data-scroll-index="0">

        <div class="owl-carousel owl-theme">
                
                
                <div class="item bg-img" data-overlay-dark="3" data-background="/assets/img/inscription.jpg">
                    <div class="text-center v-middle caption mt-30">
                        
                        <div class="row">
                            <div class="offset-md-2 col-md-8 offset-lg-2 col-lg-8">
                                <h1 class="font-weight-bold text-white mb-6" style="font-size: 30px;">Inscription | CCS+ | Informations Supplémentaires</h1>
				              <form class="bg-white  rounded-5 shadow-5-strong p-5" id="join" onsubmit="process(event)" action="{{ route('join_form') }}" method="post">
				              	@csrf
				              	<p class="mb-4 text-center " style="color: red;">* Tous les champs sont obligatoires</p>
				                <div class="alert alert-info" style="display: none;"></div>

				                <div class="row mb-4">		                              

	                                <div class="col-md-12 col-sm-12" style="text-align: left;">
	                                    
	                                    <label class="form-label" for="wanted">Voulez-vous appartenir au cercle du CCS?</label>

	                                    <div style="margin-bottom: 10px;"></div>

	                                    <select class="custom-select custom-select-md @error('wanted') is-invalid @enderror" id="wanted" name="wanted" value="{{ old('wanted') }}">
	                                      <option value="">--Veuillez choisir votre réponse--</option>
	                                      
	                                       <option value="oui">Oui</option>
	                                       <option value="non">Non</option>

	                                      
	                                    </select>
	                                    

	                                    @error('wanted')

	                                        <div class="" style="color: red;">{{ $message }}</div>

	                                    @enderror
	                                    
	                                </div>

		                        </div>

				                <div class="row mb-4">		                              

	                                <div class="col-md-12 col-sm-12" style="text-align: left;">
	                                    
	                                    <label class="form-label" for="read">Avez-vous lu le but du CCS?</label>

	                                    <div style="margin-bottom: 10px;"></div>

	                                    <select class="custom-select custom-select-md @error('read') is-invalid @enderror" id="read" name="read" value="{{ old('read') }}">
	                                      <option value="">--Veuillez choisir votre réponse--</option>
	                                      	<option value="oui">Oui</option>
	                                       <option value="non">Non</option>
	                                      
	                                    </select>
	                                    

	                                    @error('read')

	                                        <div class="" style="color: red;">{{ $message }}</div>

	                                    @enderror
	                                    
	                                </div>

		                        </div>

		                        <div class="row mb-4">

		                        	<div class="col-md-12 col-sm-12" style="text-align: left;">
		                        		<label class="form-label" for="description">Décrivez-nous un peu ce que vous faîtes</label>

	                                    <div style="margin-bottom: 10px;"></div>
								  		<textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="4" placeholder="Ex: Je suis enseignant des sciences mathématiques..."></textarea>

								  		@error('description')

	                                        <div class="" style="color: red;">{{ $message }}</div>

	                                    @enderror
								  	</div>
								  
								</div>


								<div class="row mb-5">

		                                <div class="col" style="text-align: left;">
		                                    
		                                    <label class="form-label" for="password">Définissez un mot  de passe</label>

		                                    <div style="margin-bottom: 10px;"></div>

		                                    <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" />

		                                    @error('password')

		                                        <div class="" style="color: red;">{{ $message }}</div>

		                                    @enderror
		                                    
		                                </div>

		                                <div class="col" style="text-align: left;">
		                                    
		                                    <label class="form-label" for="confirm_password">Confirmer le mot de passe</label>

		                                    <div style="margin-bottom: 10px;"></div>

		                                    <input type="password" id="confirm_password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror"  />

		                                    @error('confirm_password')

		                                        <div class="" style="color: red;">{{ $message }}</div>

		                                    @enderror
		                                    
		                                </div>

		                        </div>
				                

				            <div class=" flex justify-between">

				                <div class="">
				                	
				                </div>

				                <div class="">
				                	<button type="submit" class="btn btn-outline-success btn-rounded " name="continue" value="continue">S'inscrire</button>
				                </div>
				            </div>
				              </form>
                            </div>
                        </div>
                        <a href="{{route('join')}}" class="butn butn-light mt-30">
                            <span>Inscrivez-vous  </span>
                        </a>

                        <a href="{{route('login')}}" class="butn butn-bg">
                            <span>Se connecter</span>
                        </a>
                    </div>
                </div>
        </div>
      </header>
				@endif
			@endif
		</header>
		


		@include('templates.components.js')
		<script>
		   const phoneInputField = document.querySelector("#phone");
		   const phoneInput = window.intlTelInput(phoneInputField, {
		     utilsScript:
		       "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
		   });

		   var input = document.querySelector("#phone");
	        window.intlTelInput(input, {
	            formatOnDisplay: true,
	            initialCountry: "bj",
	            separateDialCode: true,
	            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
	        });
		   
		   $("#phone").countrySelect({
            defaultCountry:"bj"
        });
		</script>
	</body>

</html>
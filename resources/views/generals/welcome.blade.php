@extends('templates.generals.master')

@section('title')
Accueil
@stop

@section('content')


<section class="testimonials  bg-img bg-fixed pos-re" data-overlay-dark="7" data-background="/assets/img/etoile_rouge_haut.jpg" style="padding-top:50px">
            <div class="container">
                <div class="row">

                    <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                        <h1 style="font-size: 45px; font-weight:bold;padding-top:40px">CCS CONTACT+ C'EST QUOI?</h1>
                        <p></p>
                    </div>

                    <div class=" owl-theme text-center col-lg-10 offset-lg-1">
                        
                        
                        <div class="item-box" style="background:#2ba9e1">
                            <p style="color: white;">CCS vient résoudre le problème de visibilité sur whatsapp.
                            Il suffit de vous inscrire et vous aurez des contacts whatsapp chaque semaine que vous pouvez télécharger directement à partir de l'application. Cela vous permettra aussi de faire des publicités sur notre site, grâce à nos 6673 membres <span style="font-weight:bold"> et qui augmente au fur et à mésure </span>, plus de 11000 contacts disponibles y compris ceux des membres et à nos" nombreux visiteurs. Celà vous permettra d'envoyer des publications directement dans le Inbox des membres du cercle, et bien d'autres choses encore.
                            C'est un système mis en place dans le but de créer un grand et vaste marché social ouvert à tous et pour booster vos business et vos différentes activités.</p>
                            
                        </div>
                    </div>


                    

                </div>
            </div>

            <div class="curve curve-top"></div>
        </section>



        <section class="hero" data-scroll-index="1">
            <div class=" pos-re" >
                <div class="container">
                    <div class="row">
                        <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10 text-center ">
                            
                        </div>

                        <div class="feat-item mb-md50 col-lg-3 shadow-2xl">
                            <div class="text-center">
                            
                                <p  style="color:black;font_weight:bold">Elargissez votre clientèle en un temps records</p>
                            </div>
                        </div>

                        <div class="feat-item active mb-md50 col-lg-3 shadow-2xl" style="margin-top: 10px">
                            <div class="text-center">
                                
                               
                                <p style="color:black;font_weight:bold">Faîtes promouvoir votre entreprise et vos différents produits à une clientèle déjà disponible.</p>
                            </div>
                        </div>

                        <div class="feat-item col-lg-3 shadow-2xl">
                            <div class="text-center">
                            
                                
                                <p  style="color:black;font_weight:bold">Nous proposons vos services aux membres du cercle.</p>
                            </div>
                        </div>


                        <div class="feat-item col-lg-3 shadow-2xl">
                            <div class="text-center">
                           
                                <p  style="color:black;font_weight:bold">Nous vous offrons beaucoup d'autres opportunités et services comme le service publicitaire.</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="curve curve-gray-b curve-bottom"></div>
            </div>

            <div class=" pos-re slider-fade" data-scroll-index="0">

        <div class="row">
                
              
                            <div class="col-md-12 col-lg-12">
                                <img src="/uploads/images/{{$affiche->image ?? '' }}" style="height: 100%; width: 100%;" class="mb-4">
                            </div>
                        
                
        </div>
        
        </section>

        <!-- End Hero ====
        ======================================= -->


        <!-- =====================================
        ==== Start Process -->

        <div class="process section-padding bg-img bg-fixed pos-re text-center" data-overlay-dark="7" data-background="/assets/img/etoile_rouge_haut2.jpg">
            <div class="container">
                <div class="row">

                    <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                        <h1 style="font-size:45px"><span>AVANTAGES</span></h1>
                        <p style="font-size: 20px">CCS CONTACT+ vous offre plusieurs avantages quand vous intégrez le cercle des grands professionnels.</p>
                    </div>
                    <div class="full-width clearfix"></div>
                    
                    <div class="col-lg-4 col-md-4">
                        <div class="item first mb-md50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-level-skill" style="display:none"></span>
                            <div class="cont">
                                <h3>01</h3>
                                <h6>Carnet d'adresse</h6>
                                <p>1. 25 contacts gratuits à l'inscription. <br>
                                2. Obtenez jusqu'à 10.000 contacts d'adhérents.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="item odd mb-md50">
                            <img src="img/arrow.png" alt="">
                            <span class="icon icon-basic-book-pencil" style="display:none"></span>
                            <div class="cont">
                                <h3>02</h3>
                                <h6>Un réseau très large</h6>
                                <p>3. 25 contacts tous les 7   jours à tous les membres. <br>
                                4. Publication à moindre coût sur le site.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="item mb-sm50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-pencil-ruler" style="display:none"></span>
                            <div class="cont">
                                <h3>03</h3>
                                <h6>Visibilité accrue</h6>
                                <p>5. Publication diverses à plus de 10.000 personnes directement en Inbox. <br>
                                6. Visibilité accrue entrainant une vente accelérée</p>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>

            <div class="curve curve-gray-t curve-top"></div>
            <div class="curve curve-bottom"></div>
        </div>

        <!-- End Process ====
        ======================================= -->


        <!-- =====================================
        ==== Start Works -->

        

        <div class="why-us section-padding bg-gray pos-re" style="display: ;">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                            <h2 class="card-title" style="font-weight:bold; font-size:22px">Votre visibilité commence ici</h2>
                        </div>
                        <div class="content">
                            <table class="table table-success">
                            <thead>
                                <tr>
                                    
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                
                                <tr>
                                    <td>Poster une annonce, une vidéo, une affiche</td>
                                    <td> <a class="ui teal button" href="{{ route('customers_activate_service_publication') }}">Activer</a> </td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Télécharger des contacts</td>
                                    <td><a class="ui teal button" href="{{ route('customers_telecharger_contacts') }}">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Commander une affiche | 4000f/</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Commander un spot(vidéo) | 10.000f/20.000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                <tr>
                                    <td>Profiter des bars d'informations sur le site | 200f/jour</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Profiter des bars de notifications sur le site | 300f/jour</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                
                                <tr>
                                    <td>Envoie d’une affiche annonce à tous les adhérents par WhatsApp | 3000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Envoie d’une affiche annonce à tous les adhérents par WhatsApp et email | 4000f</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
                                
                                
                                <tr>
                                    <td>Profiter d’une promotion d'un ou tous vos produits pendant 1 mois</td>
                                    <td><a class="ui teal button" href="https://wa.me/22998736175" target="_blank">Activer</a></td>
                                </tr>
                                
        
                            </tbody>
                        </table>
                            
                        </div>
                    </div>

                </div>
            </div>

            <div class="curve curve-top"></div>
            <div class="curve curve-bottom"></div>
        </div>

        

        <section class="team section-padding" data-scroll-index="3" style="display: none;">
            <div class="container">
                <div class="row">

                    <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                        <h4><span>Talent</span> Team</h4>
                        <p>We are a passionate digital design agency that specializes in beautiful and easy-to-use digital design & web development services.</p>
                    </div>
                    
                    <div class="owl-carousel owl-theme">

                        <div class="item">
                            <div class="team-img">
                                <img src="img/team/3.jpg" alt="">
                                <div class="social">
                                    <a href="#0" class="icon">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#0" class="icon">
                                        <i class="fab fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h6>Alex Smith</h6>
                                <span>Project Manager</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        

        <section class="testimonials section-padding bg-img bg-fixed pos-re" data-overlay-dark="1" data-background="/uploads/images/{{$affiche->image ?? '' }}" style="display: none;" >
            <div class="container" style="height: 500px;">

                <div class="row">

                    <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                        
                        
                    </div>

                    

                </div>



            </div>

            <div class="curve curve-top"></div>
        </section>
      
        <div class="process section-padding bg-img bg-fixed pos-re text-center" data-overlay-dark="7" data-background="/assets/img/etoile_rouge_haut2.jpg">
            <div class="container">
                <div class="row">

                    <div class="section-head offset-md-2 col-md-8 offset-lg-3 col-lg-6">
                        <h1 style="font-size:30px"><span>NOS MODULES EXCEPTIONNELS</span></h1>
                        <p style="font-size: 20px">CCS CONTACT+ vous offre plusieurs avantages quand vous intégrez le cercle des grands professionnels.</p>
                    </div>
                    <div class="full-width clearfix"></div>
                    
                    <div class="col-lg-4 col-md-4">
                        <div class="item first mb-md50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-level-skill" style="display:none"></span>
                            <div class="cont">
                                <h3>01</h3>
                                <h6>Module1: Promotion</h6>
                                <p class="card-text">-Publication dans 200 groupes Facebook</p>
                                <p class="card-text">-Publication dans 5000 groupes Whatsapp</p>
                                <p class="card-text">-Publication dans Groupes Whatsapp du CCS+ (38 Groupes)</p>
                                <p class="card-text">-Publication dans Groupes Whatsapp du CCS</p>
                                <p class="card-text">-Publication dans Groupes Facebook du CCS</p>
                                <p class="card-text">-Publication sur la Page CCS Production</p>
                                <p class="card-text">-Publication sur la Page CCS</p>
                                <p class="card-text">-Accès au Site CCS Contact+ pendant 5 Jours (Faites par nous et disponible à la demande)</p>
                                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 05 Jours</p>
                                <a href="{{ route('customers_activate_module_specific', 1) }}" class="btn btn-primary">Activer ce module</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="item odd mb-md50">
                            <img src="img/arrow.png" alt="">
                            <span class="icon icon-basic-book-pencil" style="display:none"></span>
                            <div class="cont">
                                <h3>02</h3>
                                <h6>Module2: Communication</h6>
                                <p class="card-text">-Logo (Facultatif)</p>
                <p class="card-text">-02 Affiches numériques</p>
                <p class="card-text">-Accès au Site CCS Contacte+</p>
                <p class="card-text">-Création et maintient d'audience (FaceBook)</p>
                <p class="card-text">-Résultat disponible à la demande</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 10 Jours</p>
                <a href="{{ route('customers_activate_module_specific', 2) }}" class="btn btn-danger">Activer ce module</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="item mb-sm50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-pencil-ruler" style="display:none"></span>
                            <div class="cont">
                                <h3>03</h3>
                                <h6>Module3: Communication+</h6>
                                <p class="card-text">-Une Consultation de votre structure (Stratégie communicationnelle)</p>
                <p class="card-text">-03 Affiches Numériques</p>
                <p class="card-text">-01 Spot Publicitaire (Facultatif)</p>
                <p class="card-text">-01 Vidéo réalisée par des acteurs comédiens</p>
                <p class="card-text">-Création et maintient d'audience (Facebook)</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Annonce en Inbox aux membres du Site</p>
                <p class="card-text">-Conception et Impression de 30 Cartes de visites.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 15 Jours</p>
                <a href="{{ route('customers_activate_module_specific', 3) }}" class="btn btn-info">Activer ce module</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2"></div>
                    <div class="col-lg-4 col-md-4 mt-6">
                        <div class="item mb-sm50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-pencil-ruler" style="display:none"></span>
                            <div class="cont">
                                <h3>04</h3>
                                <h6>Module4: Mon entreprise</h6>
                                <p class="card-text">-Une Consultation de votre structure (Stratégie communicationnelle)</p>
                <p class="card-text">-04 Affiches Numériques</p>
                <p class="card-text">-02 Spots Publicitaires/Une Vidéo réalisée par les acteurs Comédiens.</p>
                <p class="card-text">-01 Vidéo explicative de votre structure.</p>
                <p class="card-text">-Création et maintient d'audience (Facebook)</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Annonce en Inbox WhatsApp et Mail aux membres du Site.</p>
                <p class="card-text">-Conception de dépliant.</p>
                <p class="card-text">-Conception de Carte de Visite.</p>
                <p class="card-text">-Publication dans les canaux.</p>
                <p class="card-text">-Déploiement des agents.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 01 mois</p>
                <a href="{{ route('customers_activate_module_specific', 4) }}" class="btn btn-warning">Activer ce module</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 mt-6">
                        <div class="item mb-sm50">
                            <img src="img/arrow.png" class="tobotm" alt="">
                            <span class="icon icon-basic-pencil-ruler" style="display:none"></span>
                            <div class="cont">
                                <h3>05</h3>
                                <h6>Module5 : Partenariat</h6>
                                <p class="card-text">-Stratégie communicationnelle</p>
                <p class="card-text">-Suivie communicationnelle</p>
                <p class="card-text">-Réalisation adéquate</p>
                <p class="card-text">-Accès au Site CCS Contact+ </p>
                <p class="card-text">-Publicité.</p>
                <p class="card-text" style="font-weight: bolder;">-Durée de Promotion: 03 mois</p>
                <a href="{{ route('customers_activate_module_specific', 5) }}" class="btn btn-success">Activer ce module</a>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>

            <div class="curve curve-gray-t curve-top"></div>
            <div class="curve curve-bottom"></div>
        </div>
    <section style="margin:20px">
          <div class="container"> 
<div class="row border border-light">   
<div class="col-md-2"></div>
<div class="col-md-4 col-8 border border-dark bg bg-success">  
<a href="   "  style="text-align: center; color: white; font-weight: bold;padding-left:50px; padding:15px">Nombre d'Adhérents</a> 
</div>
<div class="col-md-4 col-4 border border-dark"  > <h1 style="text-align: center; font-weight: bold;font-size: 15px;padding-left: 150px; padding:15px">  {{ $admin->paid + $users->count() }}   </h1>   </div>       
<div class="col-md-2"></div>
</div>
  </div>
        <div class="row mt-12" style="display:none">
                       <div class="col-md-2 ">  </div>
                    <div class="order col-md-4 col-9" >
                                        <a href="#0" class="butn butn-bg" style="background:#2ba9e1">
                                            <span>Nombres d'adhérents</span>
                                        </a>
                                    </div>
                    <div class=" text-center col-lg-4 col-3" style="font-size: 40px; color: black;">
                        
                        {{ $admin->paid + $users->count() }}
                            
                    </div>
                     <div class="col-md-2 ">  </div>

        </div>
  </section>
        

        <section class="call-action section-padding bg-img bg-fixed" data-overlay-dark="5" data-background="/assets/img/banane_igname.jpg">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="text-center">
                            <h2>Elargissez votre clientèle avec CONTACT+</h2>
                            <h5>Inscrivez-vous pour entrer dans l'univers des grands commerçants.</h5>
                            <a href="{{route('join')}}" class="butn butn-bg" style="bacckground">
                                <span>S'inscrire</span>
                            </a>

                            <a href="{{route('login')}}" class="butn butn-light mt-30" style="background:#2ba9e1;color:white;font-weight:bold">
                                <span style="color:white">Se connecter</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

      <script src="https://my.hellobar.com/aca4878ae3745803333fe2bceeb43f6dbfb0f8a6.js" type="text/javascript" charset="utf-8" async="async"> </script> 
      <script type="text/javascript">var subscribersSiteId='d8a94f22-1c18-45e7-9ba0-5f588f10922e';</script><script type="text/javascript" src="https://cdn.subscribers.com/assets/subscribers.js"></script>
@stop
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Inscription - {{ env('APP_NAME') }}</title>
		<link
	     rel="stylesheet"
	     href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"
	   />
	   <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		
	</head>

	<body style="padding-top: 60px; background-color: #ECEFF8">


		<div class="ui vertical stripe  "  >
		    <div class="ui middle aligned stackable grid container">
		      <div class="row">
		      	<div class="six wide column">
		          
		        </div>
		        <div class="five wide column">
		          <div class="ui success message">
		          	<h2 class="ui header">Inscription - CCS+</h2>
				        Félicitations! Vous venez de vous inscrire avec succès sur CCS+. Nous vous souhaitons une très belle aventure. <a href="{{ route('login') }}">Cliquez ici pour continuer.</a>
				    </div>
        		</div>
        
      			</div>
		        </div>
		        
		      </div>
		      
		    </div>
		</div>

	

      <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js" integrity="sha512-dqw6X88iGgZlTsONxZK9ePmJEFrmHwpuMrsUChjAw1mRUhUITE5QU9pkcSox+ynfLhL15Sv2al5A0LVyDCmtUw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      
	</body>



		
	

</html>
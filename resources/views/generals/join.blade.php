<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>S'inscrire | {{ env('APP_NAME') }}</title>
		
		@include('templates.components.css')
		@yield('stylesheets')

		
	</head>

	<body style="background-color: #ECEFF8;" >
		
		<div class="ui stackable grid" style="height: 100%;">

			<div class="row">
				
        		<div class="ui eight wide column aligned center " style="background-color: #ECEFF8; padding-top: 20px; padding-left: 50px;">
        			<img class="ui fluid image" src="/assets/img/inscription.jpg" width="100%" height="100%" >
		        </div>

		        <div class="eight wide column " style="margin-top: 30px;">

		        	<div class="ui  basic  ">
					  
					  <div class="ui horizontal divider" style="color: #7E623D;">
					    Inscription | CCS+	
					  </div>


					  	<div class="ui stripe" >
						    <div class="ui middle aligned stackable grid container">
						      <div class="row">
						        <div class="wide column">
						          
						          <div class="ui container fluid" style=" margin-top: 20px;">
									<div >
									  <div class="ui  message">
									  	<p>Inscrivez-vous pour bénéficier des avantages de notre cercle.</p>
						    @if ($errors->any())
							    <div class="ui error message">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							@if(session()->has('error'))
				                  <div class="ui message error">
				                      {{ session()->get('error') }}
				                  </div> 
				            @endif
									    
									    <form method="post" action="{{ route('join_form') }}" class="ui form">
						    	@csrf
							    <div class="fields">
								    <div class="required eight wide field">
								      <label for="nom">Nom</label>
								      <input type="text" placeholder="DUPONT" id="nom" name="nom" value="{{ old('nom')}}">								 	
								    </div>
								    
								    
								    <div class="required eight wide field">
								      <label for="prenom">Prénoms</label>
								      <input type="text" placeholder="Jean" id="prenom" name="prenom" value="{{ old('prenom')}}">
								    </div>
								</div>


								<div class="fields">
								    <div class="required eight wide field">
								      <label for="email">Email</label>
								      <input type="email" placeholder="info@ccs.com" id="email" name="email" value="{{ old('email')}}">
								      	
								    </div>
								    

								    <div class="required four wide field">
								      <label for="indicatif">Code indicatif</label>
								      <input type="text" placeholder="229" id="indicatif" name="indicatif"  value="{{ old('indicatif')}}">
								      <small>Sans le signe +</small>
								    </div>
								    <div class="required eight wide field">
								      <label for="phone">Téléphone (Whatsapp)</label>
								      <input type="tel" placeholder="97000000" id="phone" name="phone" value="{{ old('phone')}}">
								      <small>N'écrivez pas avec l'indicatif du pays</small>
								    </div>
								</div>

								<div class="fields">
								    <div class="required eight wide field">
								      <label for="adresse">Adresse</label>
								      <input type="text" placeholder="Abomey-Calavi" id="adresse" name="adresse" value="{{ old('adresse')}}">
								      	
								    </div>
								    
								    <div class="required eight wide field">
								      <label for="profession">Profession</label>
								      <input type="text" placeholder="" id="profession" name="profession" value="{{ old('profession')}}">
								    </div>
								</div>

							  	<div class="two fields">
								    <div class="field">
								      <label for="wanted">Voulez-vous appartenir au cercle du CSS+ ?</label>
								      <select class="ui fluid dropdown" id="wanted" name="wanted">

								        <option value="oui">Oui</option>
								    	<option value="non">Non</option>
								    	</select>
								    </div>

								    <div class="field">
								      <label for="read">Avez-vous lu le but du CSS+ ?</label>
								      <select class="ui fluid dropdown" id="read" name="read">
								      	
								        <option value="oui">Oui</option>
								    	<option value="non">Non</option>
								    	</select>
								    </div>
								</div>
							    

							    <div class=" field">
								    <label for="description">Décrivez-nous un peu ce que vous faîtes</label>
								    <textarea id="description" name="description" placeholder="Je suis un enseignant des mathématiques...."></textarea>
								</div>

								<div class="two fields">
								    <div class="required field">
								      <label for="password">Définissez un mot de passe</label>
								      <input type="password" name="password" id="password" value="{{ old('password')}}">
								    </div>

								    <div class="required field">
								      <label for="confirm_password">Confirmez le mot de passe</label>
								      <input type="password" name="confirm_password" id="confirm_password">
								    </div>
								</div>
								  <button class="ui submit button" type="submit">S'inscrire</button>
							</form>
									  </div>
									  
									</div>
								</div>
						        </div>

						        
						        
						      </div>
						      <div class="row" style="margin-top: 15px;">
						        
						      </div>
						    </div>
						  </div>

						



						<div class="ui horizontal divider" style="margin-top: 10px;">
						    Déjà inscrit?
						</div>

						<div class="ui center aligned middle segment">
							<a class="ui teal labeled icon button" href="{{ route('login') }}">
							    Se connecter
							    <i class="sign-in icon"></i>
							  </a>
						</div>

					</div>

		        </div>
		    </div>
			
		</div>

		


		@include('templates.components.js')		
	</body>

</html>
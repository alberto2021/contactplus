<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link async rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2/dist/semantic.min.css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512-8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body style="background: #e5e5e5; padding: 10px;" >

<div style="max-width: 700px; margin: 0 auto; padding: 20px; background: #fff;">
	<h1 class="ui header">CCS+ Notification activation de module {{ $module }} par {{ $user->nom }} {{ $user->prenom }} | phone : {{ $user->phone }} | Email : {{ $user->email }}</h1>
	
	<div class="ui container center aligned">Ceci est une notification.</div>
</div>

</body>

</body>
</html> 
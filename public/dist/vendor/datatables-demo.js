$(function () {
    $('#dataTable').DataTable({
       "language": { 
        "search": "Rechercher: ",
        "infoFiltered": "(Pour _MAX_ totale entrée(s) filtré)",
        "infoEmpty": "Affichage 0 à 0 pour 0 entrer",
        "emptyTable": "Pas de donnée valabre à afficher dans la table",
        "info": "Affichage _START_ à _END_ pour _TOTAL_ entrée(s)",
        "lengthMenu": "Afficher _MENU_ entrée(s)",
        "zeroRecords": "Rien à afficher",
        "paginate": {
            "next": "Suivant",
            "previous": "Arrière",
            "first": "Première page",
            "last": "Dernière page"
         }
      }
     });
  })

